import React from 'react'
import Navigation from './app/navigation/Navigation'
import { LogBox } from 'react-native';
import {encode, decode} from 'base-64';
import { useEffect, useState, useRef } from 'react';
import * as Device from 'expo-device';
import * as Notifications from 'expo-notifications';
import { Platform } from 'react-native';
import { saveValue, getValueFor } from './app/utils/localStorage';

LogBox.ignoreLogs(["Can't perform a React state update on an unmounted component"]);
LogBox.ignoreLogs(["An effect function must not return anything besides a function, which is used for clean-up"]);
LogBox.ignoreLogs(["React Native version mismatch"]);
LogBox.ignoreLogs(["RCTView generated view config for validAttributes does not match native"]);
LogBox.ignoreLogs(["Each child in a list should have a unique key prop"]);
if (!global.btoa) global.btoa = encode;
if (!global.atob) global.atob = decode;

Notifications.setNotificationHandler({
  handleNotification: async () => ({
    shouldShowAlert: true,
    shouldPlaySound: true,
    shouldSetBadge: true,
  }),
});

export default function App() {

  const [notification, setNotification] = useState(false);
  const notificationListener = useRef();
  const responseListener = useRef();

  useEffect(() => {
      registerForPushNotificationsAsync().then(token => {
          token && saveValue('tokenPush', token)
      });
  
      // This listener is fired whenever a notification is received while the app is foregrounded
      notificationListener.current = Notifications.addNotificationReceivedListener(notification => {
        setNotification(notification);
      });
    
      // This listener is fired whenever a user taps on or interacts with a notification (works when app is foregrounded, backgrounded, or killed)
      responseListener.current = Notifications.addNotificationResponseReceivedListener(response => {
      });
    
      return () => {
        Notifications.removeNotificationSubscription(notificationListener.current);
        Notifications.removeNotificationSubscription(responseListener.current);
      };
  }, []);

  return (
    <Navigation />
  )

  //funcion para obtener el token
  async function registerForPushNotificationsAsync () {

    let token;
    if (Device.isDevice) {
      const { status: existingStatus } = await Notifications.getPermissionsAsync();
      let finalStatus = existingStatus;
      if (existingStatus !== 'granted') {
        const { status } = await Notifications.requestPermissionsAsync();
        finalStatus = status;
      }
      if (finalStatus !== 'granted') {
        alert('Failed to get push token for push notification!');
        return;
      }
    } else {
      alert('Must use physical device for Push Notifications');
    }
    if(Device.isDevice && Platform.OS !== 'web' || Platform.OS === 'android'){
      // token = (await Notifications.getDevicePushTokenAsync()).data;
      token = (await Notifications.getExpoPushTokenAsync()).data;
    }
    if (Platform.OS === 'android') {
      Notifications.setNotificationChannelAsync('default', {
        name: 'default',
        importance: Notifications.AndroidImportance.MAX,
        vibrationPattern: [0, 250, 250, 250],
        lightColor: '#FF231F7C',
      });
    }

    return token;
  };
}
