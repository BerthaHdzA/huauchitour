import React from 'react';
import { createNativeStackNavigator } from '@react-navigation/native-stack';

import CuentaEspera from '../screens/cuenta/CuentaEspera';
import Login from '../screens/cuenta/Login';
import Registro from '../screens/cuenta/registro/Registro';
import IngresaCorreo from '../screens/cuenta/registro/IngresaCorreo';
import RecuperarPassword from '../screens/cuenta/recuperarPassword/RecuperarPassword';
import CodigoRecuperacion from '../screens/cuenta/recuperarPassword/CodigoRecuperacion';
import EscribirPass from '../screens/cuenta/recuperarPassword/EscribirPass';
import TerminosCondiciones from '../screens/cuenta/userOptions/TerminosCondiciones';
import AvisoPrivacidad from '../screens/cuenta/userOptions/AvisoPrivacidad';
import ValidarCodigo from '../screens/cuenta/registro/ValidarCodigo';
import Cuenta from '../screens/cuenta/Cuenta';
import Perfiluser from '../screens/cuenta/userOptions/Perfiluser';
import MembershipPass from '../screens/cuenta/userOptions/MembershipPass';
import CentrosAutorizados from '../screens/cuenta/userOptions/CentrosAutorizados';
import Contactanos from '../screens/cuenta/userOptions/Contactanos';


const Stack = createNativeStackNavigator();

export default function CuentaStack() {
    return (
        <Stack.Navigator>
            <Stack.Screen
                name='cuentaEspera'
                component={CuentaEspera}
                options={{
                    title: 'Cuenta',
                    headerStyle: {
                        backgroundColor: "white",
                        height: 75
                    },
                    headerTintColor: '#fff',
                    headerShown: false
                }}
            />
            <Stack.Screen
                name='login'
                component={Login}
                options={{
                    title: 'Atrás',
                    headerTitleStyle: { fontWeight: 'bold', },
                    headerStyle: {
                        backgroundColor: '#fff',
                        height: 75
                    },
                    headerTintColor: "rgba(0,0,0,1)",
                    headerShown: false,
                    animation: 'slide_from_right'
                }}
            />
            <Stack.Screen
                name='Cuenta'
                component={Cuenta}
                options={{
                    title: 'Cuenta',
                    headerStyle: {
                        backgroundColor: "white",
                        height: 75
                    },
                    headerTintColor: '#fff',
                    headerShown: false,
                    animation: 'slide_from_left'
                }}
            />
            <Stack.Screen
                name='Perfil'
                component={Perfiluser}
                options={{
                    title: 'Cuenta',
                    headerStyle: {
                        backgroundColor: "white",
                        height: 75
                    },
                    headerTintColor: '#fff',
                    headerShown: false,
                    animation: 'slide_from_right'
                }}
            />
            <Stack.Screen
                name='membershipPass'
                component={MembershipPass}
                options={{
                    title: 'Huauchi Pass',
                    headerStyle: {
                        backgroundColor: "white",
                        height: 75
                    },
                    headerTintColor: '#fff',
                    headerShown: false,
                    animation: 'slide_from_right'
                }}
            />
            <Stack.Screen
                name='centrosAutorizados'
                component={CentrosAutorizados}
                options={{
                    title: 'Centros Autorizados',
                    headerStyle: {
                        backgroundColor: "white",
                        height: 75
                    },
                    headerTintColor: '#fff',
                    headerShown: false,
                    animation: 'slide_from_right',
                    
                }}
            />
            <Stack.Screen
                name='contactanos'
                component={Contactanos}
                options={{
                    title: 'Contáctanos',
                    headerStyle: {
                        backgroundColor: "white",
                        height: 75
                    },
                    headerTintColor: '#fff',
                    headerShown: false,
                    animation: 'slide_from_right'
                }}
            />
            <Stack.Screen
                name="ingresaCorreo"
                component={IngresaCorreo}
                options={{
                    title: "Atrás",
                    headerTitleStyle: { fontWeight: 'bold', },
                    headerStyle: {
                        backgroundColor: "#fff",
                        height: 75
                    },
                    headerTintColor: 'rgba(0,0,0,1)',
                    headerShown: false,
                    animation: 'slide_from_right'
                }}
            />
            <Stack.Screen
                name='registro'
                component={Registro}
                options={{
                    title: 'Atrás',
                    headerTitleStyle: { fontWeight: 'bold', },
                    headerShown: false,
                    animation: 'slide_from_right'
                }}
            />
            <Stack.Screen
                name="recuperarPassword"
                component={RecuperarPassword}
                options={{
                    title: "Recuperar Password",
                    headerTitleStyle: { fontWeight: 'bold', },
                    headerShown: false,
                    animation: 'slide_from_right'
                }}
            />
            <Stack.Screen
                name="CodigoRecuperacion"
                component={CodigoRecuperacion}
                options={{
                    title: "Recuperar Password",
                    headerTitleStyle: { fontWeight: 'bold', },
                    headerShown: false,
                    animation: 'slide_from_right'
                }}
            />
            <Stack.Screen
                name="EscribirPass"
                component={EscribirPass}
                options={{
                    title: "Escribir Contraseña",
                    headerTitleStyle: { fontWeight: 'bold', },
                    headerShown: false,
                    animation: 'slide_from_right'
                }}
            />
            <Stack.Screen
                name="terminosCondiciones"
                component={TerminosCondiciones}
                options={{
                    title: "Atrás",
                    headerTitleStyle: { fontWeight: 'bold', },
                    headerStyle: {
                        backgroundColor: "#fff",
                        height: 75
                    },
                    headerTintColor: 'rgba(0,0,0,1)',
                    headerShown: false,
                    animation: 'slide_from_right'

                }}
            />
            <Stack.Screen
                name="avisoPrivacidad"
                component={AvisoPrivacidad}
                options={{
                    title: "Atrás",
                    headerTitleStyle: { fontWeight: 'bold', },
                    headerStyle: {
                        backgroundColor: "#fff",
                        height: 75
                    },
                    headerTintColor: 'rgba(0,0,0,1)',
                    headerShown: false,
                    animation: 'slide_from_right'

                }}
            />
            <Stack.Screen 
                name="validarCodigo" 
                component={ValidarCodigo} 
                options={{ 
                    title: "Atrás", 
                    headerShown: false,
                    animation: 'slide_from_right'
                }} 
            />
        </Stack.Navigator>
    );
}