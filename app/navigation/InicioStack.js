import { createNativeStackNavigator } from "@react-navigation/native-stack";
import React from "react";
import Inicio from "../screens/inicio/Inicio";
import EstablishmentInformation from "../screens/cuenta/userOptions/EstablishmentInformation";
import ExperiencesInformation from "../screens/cuenta/userOptions/ExperiencesInformation";
import SeeAll from "../screens/inicio/SeeAll";
import SeeAllExperiences from "../screens/inicio/SeeAllExperiences";
import MapEvents from "../screens/inicio/seasonEvents/MapEvents"
import ProgressScreen from "../screens/inicio/seasonEvents/ProgressScreen";
import CheemsScreen from "../screens/inicio/seasonEvents/CheemsScreen";
const Stack = createNativeStackNavigator();

export default function InicioStack() {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name="inicio"
        component={Inicio}
        options={{
          title: "Inicio",
          headerStyle: {
            backgroundColor: "gray",
          },
          headerTintColor: "#fff",
          headerTitleStyle: {
            fontWeight: "bold",
          },
          headerTitleAlign: "center",
          headerShown: false,
        }}
      />
      <Stack.Screen
        name="establishmentInformation"
        component={EstablishmentInformation}
        options={{
          title: "Información del establecimiento",
          headerStyle: {
            backgroundColor: "white",
            height: 75,
          },
          headerTintColor: "#fff",
          headerShown: false,
          animation: 'slide_from_right'
        }}
      />
      <Stack.Screen
        name="mapEvents"
        component={MapEvents}
        options={{
          title: "Mapa de eventos de temporada",
          headerStyle: {
            backgroundColor: "white",
            height: 75,
          },
          headerTintColor: "#fff",
          headerShown: false,
          animation: 'slide_from_right'
        }}
      />
      <Stack.Screen
        name="progressScreen"
        component={ProgressScreen}
        options={{
          title: "Pantalla de progresos",
          headerStyle: {
            backgroundColor: "white",
            height: 75,
          },
          headerTintColor: "#fff",
          headerShown: false,
          animation: 'slide_from_right'
        }}
      />
      <Stack.Screen
        name="cheemsScreen"
        component={CheemsScreen}
        options={{
          title: "Pantalla Cheems",
          headerStyle: {
            backgroundColor: "white",
            height: 75,
          },
          headerTintColor: "#fff",
          headerShown: false,
          animation: 'slide_from_right'
        }}
      />
      <Stack.Screen
        name="SeeAll"
        component={SeeAll}
        options={{
          title: "Atrás",
          headerTitleStyle: { fontWeight: "bold" },
          headerStyle: {
            backgroundColor: "#fff",
            height: 75,
          },
          headerTintColor: "rgba(0,0,0,1)",
          headerShown: false,
          animation: 'slide_from_right'
        }}
      />
      <Stack.Screen
        name="experiencesInformation"
        component={ExperiencesInformation}
        options={{
          title: "Información de las experiencias",
          headerStyle: {
            backgroundColor: "white",
            height: 75,
          },
          headerTintColor: "#fff",
          headerShown: false,
          animation: 'slide_from_right'
        }}
      />
      <Stack.Screen
        name="SeeAllExperiences"
        component={SeeAllExperiences}
        options={{
          title: "Atrás",
          headerTitleStyle: { fontWeight: "bold" },
          headerStyle: {
            backgroundColor: "#fff",
            height: 75,
          },
          headerTintColor: "rgba(0,0,0,1)",
          headerShown: false,
          animation: 'slide_from_right'
        }}
      />
    </Stack.Navigator>
  );
}
