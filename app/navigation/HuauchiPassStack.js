import { createNativeStackNavigator } from '@react-navigation/native-stack';
import React from 'react';
import HuauchiPass from '../screens/huauchiPass/HuauchiPass'
import CrearHuauchiPass from '../screens/huauchiPass/CrearHuauchiPass'
import HuauchiPassCreado from '../screens/huauchiPass/HuauchiPassCreado';
import CardHuauchiPass from '../screens/huauchiPass/CardHuauchiPass';
import PassEspera from '../screens/huauchiPass/PassEspera';

const Stack = createNativeStackNavigator();

export default function HuauchiPassStack() {
    return (
        <Stack.Navigator>
             <Stack.Screen
                name='PassEspera'
                component={PassEspera}
                options={{
                    title: 'PassEspera',
                    headerStyle: {
                        backgroundColor: "white",
                        height: 75
                    },
                    headerTintColor: '#fff',
                    headerShown: false
                }}
            />
            <Stack.Screen
                name='huauchiPass'
                component={HuauchiPass}
                options={{
                    title: 'Huauchi Pass',
                    headerStyle: {
                        backgroundColor: 'gray',
                    },
                    headerTintColor: '#fff',
                    headerTitleStyle: {
                        fontWeight: 'bold',
                    },
                    headerTitleAlign: 'center',
                    headerShown: false,
                    animation: 'fade'

                }}
            />
            <Stack.Screen
                name='crearhuauchiPass'
                component={CrearHuauchiPass}
                options={{
                    title: 'Crear Huauchi Pass',
                    headerStyle: {
                        backgroundColor: 'gray',
                    },
                    headerTintColor: '#fff',
                    headerTitleStyle: {
                        fontWeight: 'bold',
                    },
                    headerTitleAlign: 'center',
                    headerShown: false,
                    animation: 'slide_from_right'
                }}
            />
            <Stack.Screen
                name='huauchiPassCreado'
                component={HuauchiPassCreado}
                options={{
                    title: 'HuauchiPass creado',
                    headerStyle: {
                        backgroundColor: 'gray',
                    },
                    headerTintColor: '#fff',
                    headerTitleStyle: {
                        fontWeight: 'bold',
                    },
                    headerTitleAlign: 'center',
                    headerShown: false
                }}
            />
            <Stack.Screen
                name='cardHuauchiPass'
                component={CardHuauchiPass}
                options={{
                    title: 'Card de HuauchiPass',
                    headerStyle: {
                        backgroundColor: 'gray',
                    },
                    headerTintColor: '#fff',
                    headerTitleStyle: {
                        fontWeight: 'bold',
                    },
                    headerTitleAlign: 'center',
                    headerShown: false
                }}
            />
        </Stack.Navigator>
    );
}