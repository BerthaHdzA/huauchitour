import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { Ionicons } from '@expo/vector-icons';

import CuentaStack from './CuentaStack'
import HuauchiPassStack from './HuauchiPassStack';
import InicioStack from './InicioStack';

const Tab = createBottomTabNavigator();

export default function Navigation() {

    return (
        <NavigationContainer>
            <Tab.Navigator
                screenOptions={({ route }) => ({
                    tabBarIcon: ({ color }) => options(route, color),
                    tabBarActiveTintColor: '#3498DB',
                    tabBarInactiveTintColor: 'black',
                    headerShown: false
                })}
            >
                <Tab.Screen name="inicioStack" component={InicioStack} options={{ title: "Inicio", }} />
                <Tab.Screen name="huauchiPassStack" component={HuauchiPassStack} options={{ title: "Huauchi Pass", }} />
                <Tab.Screen name="cuentaStack" component={CuentaStack} options={{ title: "Cuenta", }} />
            </Tab.Navigator>
        </NavigationContainer>
    );
}

const options = (route, color) => {
    let iconName;
    switch (route.name) {
        case "inicioStack":
            iconName = "home"
            break;
        case "huauchiPassStack":
            iconName = "card"
            break;
        case "cuentaStack":
            iconName = "person-circle-outline"
            break;
    }

    return (
        <Ionicons type="font-awesome" name={iconName} size={22} color={color} />
    )
}