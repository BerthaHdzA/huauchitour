import React, { useState, useEffect, useCallback } from "react";
import { ActivityIndicator, Alert, FlatList, SafeAreaView, ScrollView, StatusBar, StyleSheet, Text, TouchableOpacity, View, ImageBackground, RefreshControl} from "react-native";
import { Image } from "react-native-elements";
import { useNavigation, useFocusEffect } from "@react-navigation/native";
import { Shadow } from "react-native-shadow-2";
import { size } from "lodash";
import { useFonts } from "expo-font";
import Loader from "../../components/Loader";
import { getValueFor, saveValue } from "../../utils/localStorage";

import Api from "../../utils/Api";
import { url } from "../../utils/config";
import LoaderLineal from "../../components/LoaderLineal";
import Constants from 'expo-constants'

const Item = ({ urlImgPerfil, nombre, descripcion, onPress }) => (
  <View style={styles.items}>
    <TouchableOpacity onPress={onPress} style={styles.touchable} allowFontScaling={false} activeOpacity={1}>
      {
        !urlImgPerfil ?
          <Image
            source={require('../../../assets/notUrl.png')}
            style={styles.profilePicture}
            resizeMode={'contain'}
          />
          : (
            <Image
              source={{ uri: urlImgPerfil }}
              style={styles.profilePicture}
              resizeMode={"contain"}
            />
          )
      }
      <View style={{ height: "40%", width: "100%" }}>
        <Text style={styles.title} allowFontScaling={false}>
          {nombre}
        </Text>
        <Text style={styles.subtitle} allowFontScaling={false}>
          {descripcion}
        </Text>
      </View>
    </TouchableOpacity>
  </View>
);

const ItemExp = ({ urlImgPerfil, nombre, onPress }) => (
  <View style={styles.itemsecond}>
    <TouchableOpacity
      onPress={onPress}
      style={styles.touchable}
      allowFontScaling={false}
      activeOpacity={1}
    >
      {
        !urlImgPerfil ?
          <Image
            source={require('../../../assets/notUrl.png')}
            style={styles.rectangularImages}
          />
          : (
            <Image
              source={{ uri: urlImgPerfil }}
              style={styles.rectangularImages}
            />
          )
      }
      <View style={{ height: "100%", width: "70%" }}>
        <Text style={styles.title} allowFontScaling={false}>
          {nombre}
        </Text>
      </View>
    </TouchableOpacity>
  </View>
);

export default function Inicio() {
  const navegacion = useNavigation();
  const [lista, setLista] = useState([]);
  const [listaExp, setListaExp] = useState([]);
  const [error, setError] = useState(null);
  const [refreshing, setRefreshing] = useState(false);

  useFocusEffect(
    useCallback(()=>{
      StatusBar.setBarStyle('dark-content');
    })
  );

  const getData = async () => {
    let uri = "";
    let api = null;

    let id = await getValueFor('id');
    let tokenPush = await getValueFor('tokenPush');
    if(id && tokenPush){
      uri = `${url}eventos/tokenPush`;
      let parametrosToken = {
        id: id,
        token: tokenPush,
        plataforma: "Movil"
      }
      api = new Api(uri, "POST", parametrosToken);
      api.call().then((res)=>{
        if(res.response){
          console.log(tokenPush)
          console.log(res.message + " Token registrado");
        }else{
          console.log(res.message + " Inicia Sesion Para Registrar Token");
        }
      });
    }else{
      console.log(" Inicia Sesion Para Registrar Token");
    }

    uri = `${url}establishment/listapp`;
    api = new Api(uri, "GET");
    api.call().then((res) => {
      if (res.response) {
        setLista(res.result);
      } else {
        setError(res.errors);
      }
    });

    uri = `${url}establishment/listexperiencies`;
    api = new Api(uri, "GET");
    api.call().then((res) => {
      if (res.response) {
        setListaExp(res.result);
      } else {
        setError(res.errors);
      }
    });
  }

  const onRefresh = () => {
    // Lógica de recarga aquí
    getData();

    // Simulamos un tiempo de espera de 2 segundos antes de finalizar la recarga
    setTimeout(() => {
      setRefreshing(false);
    }, 2000);
  };

  useEffect(() => {
    getData();
  }, [])

  const renderItem = ({ item }) => (
    <Item
      urlImgPerfil={item.urlImgPerfil}
      nombre={item.nombre}
      descripcion={item.descripcion}
      onPress={() => {
        navegacion.navigate("establishmentInformation", { id: item.id })
        console.log(item);
      }
      }
    />
  );

  const renderExp = ({ item }) => (
    <ItemExp
      urlImgPerfil={item.urlImgPerfil}
      nombre={item.nombre}
      onPress={() =>
        navegacion.navigate("experiencesInformation", { id: item.id })
      }
    />
  );

  //permitendo acceso a la vista de el mapa
  const darAccesoAEventos = async () => {
    let id = await getValueFor('id');
    let token = await getValueFor('token');

    if(!id && !token){
      // si no hay sesion iniciada
      Alert.alert(
        "Alerta",
        "Inicie sesión para poder obtener acceso a los Eventos de Temporada.",
        [
          {
            text: "Ok"
          }
        ]
      );
    } else {
      // si hay sesion iniciada
      let uri = `${url}eventos/traerEvento`;
      let api = new Api(uri, "GET");
      api.call().then((res) => {

        if (res.response) {

          let evento = res.result;
          saveValue("idEvento", evento.idEvento);
          uri = `${url}eventos/obtenerPocision/${evento.idEvento}/${id}`;
          api = new Api(uri, "GET");
          api.call().then((res) => {

            if (res.response) {

              if(res.result.puesto <= 3){

                // uri = `${url}eventos/obtenerPremio/${res.result.puesto}/${evento.idEvento}`;
                // api = new Api(uri, "GET");
                // api.call().then((res) => {

                //   if (res.response) {
                    // if(res.result.reclamado == "reclamado"){
                    //   alert("Ya has completado todas las actividades de este evento y ya haz reclamado tu premio, espera a que inicie uno nuevo");
                    // } else {
                      navegacion.navigate("mapEvents");
                    // }
                //   } else {
                //     alert("Aun no hay un premio para tu puesto espera a que se cree el premio")
                //     console.log(res.message);
                //   }
                // });
              } else {
                alert("Ya has completado todas las actividades de este evento espera a que inicie uno nuevo");
              }

            } else {
              navegacion.navigate("mapEvents");
            }
          });

        } else {
          alert(res.message);
        }
      });
    }
  }

  const [loaded] = useFonts({
    "Montserrat-Bold": require("../../../assets/fuentes/Montserrat-Bold.ttf"),
    "Montserrat-Italic": require("../../../assets/fuentes/Montserrat-Italic.ttf"),
    "Montserrat-Light": require("../../../assets/fuentes/Montserrat-Light.ttf"),
    "Montserrat-Medium": require("../../../assets/fuentes/Montserrat-Medium.ttf"),
    "Montserrat-Regular": require("../../../assets/fuentes/Montserrat-Regular.ttf"),
    "Montserrat-SemiBold": require("../../../assets/fuentes/Montserrat-SemiBold.ttf"),

  });
  if (!loaded) {
    return null;
  }

  return (
    <View style={[styles.container, { flex: 1, backgroundColor: '#fff' }]}>
      <ScrollView 
        style={{ backgroundColor: "#fff", height: "100%", width: "100%" }} 
        overScrollMode='never'
        refreshControl={<RefreshControl refreshing={refreshing} onRefresh={onRefresh}/>}
      >
        <View>
          <Image
            source={require("../../../assets/home_image.png")}
            style={{ width: "100%", height: 549, resizeMode: "cover", marginTop: "-20%", }}
            PlaceholderContent={<ActivityIndicator />}
          />
        </View>

        <Shadow distance={175} startColor={"#fff"} offset={[0, 1]}>
          <View style={styles.containerTitle}>
            <Text style={styles.mainTitle} allowFontScaling={false}>
              ¿Qué harás hoy?
            </Text>
          </View>
        </Shadow>

        <View style={{ marginTop: "6%", width: "100%" }}>
          <Text style={styles.secondTitle} allowFontScaling={false}>
            Disfruta de Huauchinango ahora con los beneficios del Huauchi Pass.
          </Text>
        </View>

        <View style={[styles.contenedorSeason, { width: "90%"}]}>
          <Text style={[styles.informationText, {height: "20%", marginStart: 0}]} allowFontScaling={false}>
           Eventos de temporada.
          </Text>
          
          <View style={styles.contenedorImagen} >
            <TouchableOpacity onPress={() => darAccesoAEventos()} style={styles.touchImage}>
                <ImageBackground style={styles.imagenBanner} source={require('../../../assets/bannerEventos.jpg')} resizeMode={"cover"}></ImageBackground>
            </TouchableOpacity>
          </View >
        </View>

        <View style={{ marginTop: "14%", width: "100%" }}>
          <Text style={styles.informationText} allowFontScaling={false}>
            Promos y descuentos en estos
          </Text>
          <Text style={styles.lineText} allowFontScaling={false}>establecimientos</Text>
          <Text style={styles.seeAll} onPress={() => navegacion.navigate("SeeAll")} allowFontScaling={false}>Ver todos</Text>
        </View>
    
        <View>
          {
            !lista ? (
              <View style={styles.nullStyles} >
                <Text style={{ color: "gray", fontFamily: "Montserrat-Medium" }} allowFontScaling={false} >
                  No hay establecimientos.
                </Text>
              </View>
            ) : (
              size(lista) > 0 ? (
                <FlatList
                  horizontal
                  overScrollMode='never'
                  data={lista.slice(-5)}
                  renderItem={renderItem}
                />  
              ):(
                <View style={styles.loaderStyles}>
                <LoaderLineal />
              </View>
              )           
            )}
        </View>

        <View style={{ width: "100%", paddingBottom: "5%", paddingTop: "10%", }} >
          <Text style={styles.informationText} allowFontScaling={false}>
            Experiencias para ti
          </Text>
        </View>

        <View style={styles.space}>
          <Text style={styles.seeExperiences} onPress={() => navegacion.navigate("SeeAllExperiences")} allowFontScaling={false}>Ver todos</Text>
        </View>

        <View style={{ paddingBottom: "5%" }}>
          {
            !listaExp ? (
              <View style={styles.nullStyles}>
                <Text style={{ color: "gray", fontFamily: "Montserrat-Medium" }} allowFontScaling={false} >
                  No hay experiencias.
                </Text>
              </View>
            ) : (
            size(listaExp) > 0 ? (
               <FlatList
                horizontal
                overScrollMode='never'
                data={listaExp.slice(-5)}
                renderItem={renderExp}
              />
            ):(
              <View style={styles.loaderStyles}>
                <LoaderLineal />
              </View>
            )
         )}
        </View>
      </ScrollView>
    </View>
  );
}

const styles = StyleSheet.create({
  items: {
    backgroundColor: "#fff",
    padding: 5,
    paddingLeft: "5%",
    marginVertical: 20,
    marginHorizontal: 8,
    width: 225,
    height: 250,
  },
  itemsecond: {
    backgroundColor: "#fff",
    paddingLeft: "5%",
    marginHorizontal: 8,
    width: 225,
    height: 300,
  },
  rectangularImages: {
    height: 250,
    width: "90%",
    resizeMode: 'cover',
    borderRadius: 10,
    overflow: 'hidden'
  },
  touchable: {
    width: "100%",
  },
  profilePicture: {
    height: 190,
    width: "90%",
    borderRadius: 7,
  },
  title: {
    color: "black",
    fontSize: 16,
    marginTop: "9%",
    fontFamily: "Montserrat-Bold",
  },
  subtitle: {
    color: "black",
    fontSize: 13,
    fontFamily: "Montserrat-Regular",
    width: '100%'
  },
  container: {
    flex: 1,
    width: "100%"
  },
  containerTitle: {
    marginTop: "-15%",
    width: "100%",
  },
  mainTitle: {
    marginStart: "5%",
    marginRight: "5%",
    fontSize: 50,
    color: "#000",
    fontFamily: "Montserrat-Bold",
  },
  secondTitle: {
    marginStart: "5%",
    marginRight: "5%",
    fontSize: 18,
    fontFamily: "Montserrat-Light",
    lineHeight: 25,
  },
  informationText: {
    marginStart: "5%",
    marginRight: "5%",
    fontSize: 18,
    fontFamily: "Montserrat-SemiBold",
    lineHeight: 34,
  },
  seeAll: {
    paddingBottom: '4%',
    flexDirection: 'row',
    paddingLeft: '70%',
    color: '#0f93e3',
    marginTop: -20,
    fontSize: 15,
    fontFamily: 'Montserrat-SemiBold',
  },
  nullStyles: {
    width: "100%",
    height: "100%",
    alignItems: "center",
    justifyContent: "center",
  },
  space: {
    paddingBottom: '4%',
    flexDirection: 'row',
    paddingLeft: '70%',
  },
  seeExperiences: {
    color: "#0f93e3",
    marginTop: -44,
    fontSize: 15,
    fontFamily: 'Montserrat-SemiBold',
  },
  lineText: {
    marginStart: "5%",
    marginRight: "35%",
    fontSize: 18,
    fontFamily: "Montserrat-SemiBold",
    lineHeight: 20,
  },
  contenedorSeason: {
    height: 'auto',
    maxHeight: 160,
    flexDirection: "column",
    marginTop: "14%", 
    alignSelf: 'center'
  },
  contenedorImagen: {
    height:'90%',
    width:'100%',
    alignSelf:'center',
    alignItems: "center",
    justifyContent: "center"
  },

  touchImage:{
    width:'100%',
    height:'70%',
    shadowColor: "#000",
      shadowOffset: {
	      width: 0,
	      height: 12,
      },
    shadowOpacity: 0.58,
    shadowRadius: 16.00,
    elevation: 24,
  },

  imagenBanner: {
    height: "100%",
    width: '100%',
    borderRadius: 20,
    overflow: "hidden"
  }
});
