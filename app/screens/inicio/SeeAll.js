import React, { useState, useCallback, useEffect } from "react";
import { Dimensions, FlatList, SafeAreaView, StyleSheet, RefreshControl, Text, TouchableOpacity, View, StatusBar } from "react-native";
import { Image, } from "react-native-elements";
import { Ionicons } from "@expo/vector-icons";
import { useNavigation, useFocusEffect } from "@react-navigation/native";
import { useFonts } from "expo-font";
import { size } from "lodash";
import Constants from 'expo-constants';

import Api from "../../utils/Api";
import { url } from "../../utils/config";
import LoaderLineal from "../../components/LoaderLineal";
import { Platform } from "react-native";

const Item = ({ urlImg, nombre, Calificacion, onPress }) => (
  <View style={styles.items}>
    <TouchableOpacity onPress={onPress} style={styles.touchable} allowFontScaling={false} activeOpacity={1}>
      {
        !urlImg ?
          <View style={styles.itemStyles}>
            <Image source={require('../../../assets/notUrl.png')}
              style={styles.logo}
              resizeMode={"contain"}
            />
          </View>
          : (
            <View style={styles.itemStyles}>
              <Image
                source={{ uri: urlImg }}
                style={styles.logo}
                resizeMode={"contain"}
              />
            </View>
          )
      }
      <View style={{ height: "100%", width: "100%" }}>
        {
          Calificacion == 0 ?
            <Text style={styles.puntuacion} allowFontScaling={false}>
              <Ionicons name='star' size={19} color={'#FED554'} /> 5
            </Text>
            : (
              <Text style={styles.puntuacion} allowFontScaling={false}>
                <Ionicons name='star' size={19} color={'#FED554'} /> {Calificacion}
              </Text>
            )
        }
        <Text style={styles.title} allowFontScaling={false}>
          {nombre}
        </Text>
      </View>
    </TouchableOpacity>
  </View>
)

const formatData = (lista, numColumns) => {
  const total = Math.floor(lista.length / numColumns);
  let totalLastRow = lista.length - (total * numColumns);
  while (totalLastRow !== numColumns && totalLastRow !== 0) {
    lista.push({ key: `blank-${totalLastRow}`, empty: true });
    totalLastRow++;
  }
  return lista;
};

const numColumns = 2;

export default function SeeAll() {
  const navegacion = useNavigation();
  const [lista, setLista] = useState([]);
  const [error, setError] = useState(null);

  const [refreshing, setRefreshing] = useState(false);

  const getEstab = async () => {

    let uri = `${url}establishment/listapp`;
    let api = new Api(uri, "GET");
    api.call().then((res) => {
      if (res.response) {
        setLista(res.result);
        console.log(res.result, "Conectado");
      } else {
        setError(res.errors);
      }
    });
  }

  useEffect(() => {
      if(Platform.OS == 'ios'){
        StatusBar.setBarStyle('dark-content');
      }
      getEstab()
    }, []
  )

  const onRefresh = () => {
    // Lógica de recarga aquí
    getEstab()

    // Simulamos un tiempo de espera de 2 segundos antes de finalizar la recarga
    setTimeout(() => {
      setRefreshing(false);
    }, 2000);
  };

  const [loaded] = useFonts({
    "Montserrat-Bold": require("../../../assets/fuentes/Montserrat-Bold.ttf"),
    "Montserrat-Italic": require("../../../assets/fuentes/Montserrat-Italic.ttf"),
    "Montserrat-Light": require("../../../assets/fuentes/Montserrat-Light.ttf"),
    "Montserrat-Medium": require("../../../assets/fuentes/Montserrat-Medium.ttf"),
    "Montserrat-Regular": require("../../../assets/fuentes/Montserrat-Regular.ttf"),
    "Montserrat-SemiBold": require("../../../assets/fuentes/Montserrat-SemiBold.ttf"),
  });
  if (!loaded) {
    return null;
  }

  renderItem = ({ item, index }) => {
    if (item.empty == true) {
      return <View style={[styles.items, styles.itemInvisible]} />;
    }
    return (
      <Item
        urlImg={item.urlImgPerfil}
        Calificacion={item.Calificacion}
        nombre={item.nombre}
        onPress={() =>
          navegacion.navigate("establishmentInformation", { id: item.id })
        }
      />
    );
  };

  return (
    <View style={{ flex: 1, backgroundColor: '#fff', paddingTop: Platform.OS == 'ios' ? Constants.statusBarHeight : 0}}>
    {/* <StatusBar backgroundColor={'#fff'} barStyle={'dark-content'}/> */}
      <View style={styles.estilo1}>
        <TouchableOpacity onPress={() => navegacion.goBack()}>
          <View style={{ flexDirection: 'row',  alignItems:'center', height: '100%'}}>
            <Ionicons
              allowFontScaling={false}
              name="arrow-back-outline"
              size={30}
              style={{ color: '#555454', marginStart: '3%', paddingRight: '5%' }}
            />
            <Text style={styles.letra} allowFontScaling={false}>
              Establecimientos
            </Text>
          </View>
        </TouchableOpacity>
      </View>
      <View>
        {
          !lista ? (
            <View style={styles.nullStyles} >
              <Text style={{ color: "grey", fontFamily: "Montserrat-Bold" }} allowFontScaling={false} >
                No hay establecimientos.
              </Text>
            </View>
          ) : (
            size(lista) > 0 ? (
              <FlatList
                numColumns={numColumns}
                horizontal={false}
                data={formatData(lista, numColumns)}
                renderItem={renderItem}
                overScrollMode='never'
                keyExtractor={item => item.id}
                style={{ marginBottom: '21%', }}
                refreshControl={<RefreshControl refreshing={refreshing} onRefresh={onRefresh}/>}
              />
            ) : (
              <View style={styles.loaderStyles} >
                <LoaderLineal />
              </View>
            )
          )
        }
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  items: {
    marginVertical: 10,
    marginHorizontal: 23,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
    flex: 1,
    paddingTop: '21%',
    height: 210
    
  },
  touchable: {
    width: "100%",
  },
  itemStyles: {
    backgroundColor: '#fff',
    borderRadius: 13,
    elevation: 5,
  },
  estilo1: {
    backgroundColor: '#fff',
    width: '100%',
    height: '10%',
  },
  letra: {
    fontSize: 20,
    color: '#555454',
    fontFamily: 'Montserrat-Bold',
    marginLeft: "12%"
  },
  title: {
    color: "black",
    fontSize: 16,
    fontFamily: "Montserrat-Bold",
    marginTop: '-2%',
    textAlign: 'center',
  },
  puntuacion: {
    color: 'black',
    fontSize: 16,
    // lineHeight: 50, 
    textAlign: 'center',
    fontFamily: 'Montserrat-Bold',
  },
  logo: {
    height: 150,
    width: "100%",
    borderRadius: 13,
  },
  itemInvisible: {
    backgroundColor: 'transparent'
  },
  nullStyles: {
    width: "100%",
    height: "100%",
    alignItems: "center",
    justifyContent: "center",
  },
  loaderStyles: {
    width: "100%",
    height: "100%",
    alignItems: "center",
    justifyContent: "center",
  },
});
