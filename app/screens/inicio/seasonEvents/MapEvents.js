import * as React from 'react';
import * as Location from 'expo-location';
import { useFocusEffect, useIsFocused } from '@react-navigation/native';
import MapView, { Marker, Polyline } from 'react-native-maps';
import { SafeAreaView, Image, TouchableOpacity, LogBox, View, Text, StyleSheet, Dimensions, Platform, ImageBackground, StatusBar } from 'react-native';
import { useNavigation } from '@react-navigation/native';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import * as FAW from '@fortawesome/free-solid-svg-icons';
import { FlatList, GestureHandlerRootView, ScrollView } from 'react-native-gesture-handler';
import BottomSheet from '@gorhom/bottom-sheet';
import { url } from '../../../utils/config';
import { useFonts } from 'expo-font';
import { getValueFor,saveValue } from "../../../utils/localStorage";
import Api from "../../../utils/Api";
import QRCode from 'react-native-qrcode-svg';
import Modal from "react-native-modal";
import Animated, {
  Extrapolate,
  interpolate,
  useAnimatedStyle,
} from "react-native-reanimated";
import * as Notifications from 'expo-notifications';
import Constants from 'expo-constants'

// marcadores estaticos de prueba
// const marcas = [
//   {
//       "idActividad": "4",
//       "titulo": "Jugar",
//       "descripcionCorta": "lorem",
//       "descripcionLarga": "lorem",
//       "fechaInicial": "2022-10-26",
//       "fechaFinal": "2022-11-26",
//       "latitud": "20.1750541",
//       "longitud": "-98.0518269",
//       "status": "activo",
//       "imagen": "http://192.168.1.252/gamma/backend/img/actividades/luzDeLuna.png",
//       "evento_idEvento": "1",
//       "establecimiento_id": "1"
//   },
//   {
//       "idActividad": "2",
//       "titulo": "Comer",
//       "descripcionCorta": "lorem",
//       "descripcionLarga": "lorem",
//       "fechaInicial": "2022-10-26",
//       "fechaFinal": "2022-11-26",
//       "latitud": "20.173308",
//       "longitud": "-98.051710",
//       "status": "activo",
//       "imagen": "http://192.168.1.252/gamma/backend/img/actividades/luzDeLuna.png",
//       "evento_idEvento": "1",
//       "establecimiento_id": "1"
//   },
//   {
//       "idActividad": "6",
//       "titulo": "Responder",
//       "descripcionCorta": "lorem",
//       "descripcionLarga": "lorem",
//       "fechaInicial": "2022-10-26",
//       "fechaFinal": "2022-11-26",
//       "latitud": "20.1748836",
//       "longitud": "-98.0503761",
//       "status": "activo",
//       "imagen": "http://192.168.1.252/gamma/backend/img/actividades/luzDeLuna.png",
//       "evento_idEvento": "1",
//       "establecimiento_id": "1"
//   },
//   {
//       "idActividad": "1",
//       "titulo": "Saltar",
//       "descripcionCorta": "lorem",
//       "descripcionLarga": "lorem",
//       "fechaInicial": "2022-10-26",
//       "fechaFinal": "2022-11-26",
//       "latitud": "20.1751525",
//       "longitud": "-98.0512152",
//       "status": "activo",
//       "imagen": "http://192.168.1.252/gamma/backend/img/actividades/luzDeLuna.png",
//       "evento_idEvento": "1",
//       "establecimiento_id": "1"
//   },
//   {
//       "idActividad": "7",
//       "titulo": "Cocinar",
//       "descripcionCorta": "lorem",
//       "descripcionLarga": "lorem",
//       "fechaInicial": "2022-10-26",
//       "fechaFinal": "2022-11-26",
//       "latitud": "20.1742416",
//       "longitud": "-98.0521086",
//       "status": "activo",
//       "imagen": "http://192.168.1.252/gamma/backend/img/actividades/luzDeLuna.png",
//       "evento_idEvento": "1",
//       "establecimiento_id": "1"
//   },
//   {
//       "idActividad": "5",
//       "titulo": "Escuchar",
//       "descripcionCorta": "lorem",
//       "descripcionLarga": "lorem",
//       "fechaInicial": "2022-10-26",
//       "fechaFinal": "2022-11-26",
//       "latitud": "20.174374",
//       "longitud": "-98.054524",
//       "status": "activo",
//       "imagen": "http://192.168.1.252/gamma/backend/img/actividades/luzDeLuna.png",
//       "evento_idEvento": "1",
//       "establecimiento_id": "3"
//   },
//   {
//       "idActividad": "8",
//       "titulo": "Aprender",
//       "descripcionCorta": "lorem",
//       "descripcionLarga": "lorem",
//       "fechaInicial": "2022-10-26",
//       "fechaFinal": "2022-11-26",
//       "latitud": "20.1735199 ",
//       "longitud": "-98.0496213",
//       "status": "activo",
//       "imagen": "http://192.168.1.252/gamma/backend/img/actividades/luzDeLuna.png",
//       "evento_idEvento": "1",
//       "establecimiento_id": "1"
//   }
// ]

//datos estaticos para flor

const progreso = [
  {
    route: require('../../../../assets/logoHT_0.png')
  },
  {
    route: require('../../../../assets/logoHT_1.png')
  },
  {
    route: require('../../../../assets/logoHT_2.png')
  },
  {
    route: require('../../../../assets/logoHT_3.png')
  },
  {
    route: require('../../../../assets/logoHT_4.png')
  },
  {
    route: require('../../../../assets/logoHT_5.png')
  },
  {
    route: require('../../../../assets/logoHT_6.png')
  },
  {
    route: require('../../../../assets/logoHT_7.png')
  }
]

export default function MapEvents() { 
  //verificar si la pantalla esta focus
  const focused = useIsFocused();
  //Ruta de origen para el mapa si no se proporciona ubicacion
  const [origin, setOrigin] = React.useState({
    latitude: 20.1743588,
    longitude: -98.05111
  });
  //posicion del marcador del usuario
  const [userPosition, setUserPosition] = React.useState({
    latitude: 20.1743588,
    longitude: -98.05111
  });
  //marcas para el mapa
  const [marcas, setMarcas] = React.useState([]);
  //progreso del evento
  const [progress, setProgress] = React.useState(0);
  //controlar que la ubicacion no se resete cada renderizado ----
  // saber cuando debe seguir al usuario y cuando no
  const focus = React.useRef(false);
  // saber cuando usuar coordenadas de coor y cuando las de origin
  const change = React.useRef(false);
  //contar renderizaciones
  const count = React.useRef(0);
  // guardar coordenadas cuando onchange se ejecuta
  const coor = React.useRef(0);
  //renderizar para ir a una nueva region 
  const [irA, setIrA] = React.useState(1);
  //saber si cambio de estado de el bottomsheet
  const [isOpen, setIsOpen] = React.useState(false); 
  //saber si el modal debe ser visible o no
  const [visible, setVisible] = React.useState(false);
  const [visible2, setVisible2] = React.useState({
    visibility: false,
    datos: {}
  });  
  const[visible3, setVisible3] = React.useState(false);
  const [persona, setPersona] = React.useState(0);
  // texto para modal informativo
  const [informative, setInformative] = React.useState("Cargando...");
  const notificationListener = React.useRef();

  //enviando progreso por local storage
  saveValue("progreso", JSON.stringify(progress));

  //#region Mapa y actividades
  const navegacion = useNavigation();

  // obtener datos cuando cargue la pagina
  useFocusEffect(
    React.useCallback(() => {

      if(Platform.OS == 'ios'){
          StatusBar.setBarStyle('dark-content');
      }

      // obtiene el texto para modal de informacion 
      const getInformative = async () => {
        let uri = "";
        let api = null;
        let parametros = {
          modal: 1
        }
        uri = `${url}eventos/getInformative`;
        api = new Api(uri, "POST", parametros);
        api.call().then((res) => {
          if(res.response){
            setInformative(res.result.textInformative);
          }else{
            setInformative(res.message);
          }
        });
      }
      getInformative();

      const checkLogin = async () =>{

        let id = await getValueFor('id');
        let token = await getValueFor('token');    

        if(!id && !token){
          // si no hay sesion iniciada
          navegacion.navigate("inicio");
        }
      }
      checkLogin();

      const traerActividades = async () => {
        
        let id = await getValueFor('id');
        setPersona(id);
        let idEvento = await getValueFor('idEvento');
        let uri = "";
        let api = null;
        
        //comprobando si hay actividades
        uri = `${url}eventos/comprobarActividades/${idEvento}/${id}`;
        api = new Api(uri, "GET");
        api.call().then((res) => {
          
          if (res.response) {
            //si hay actividades verifica cuales se han completado y devuelve las que no se han completado
            uri = `${url}eventos/traerListaActividades/${idEvento}/${id}`;
            api = new Api(uri, "GET");
            api.call().then((res) => {
              if(res.response){
                setMarcas(res.result);

                uri = `${url}eventos/actualizarProgreso/${id}/${idEvento}`;
                api = new Api(uri, "GET");
                api.call().then((res) => {
                  if(res.response){
                    setProgress(res.result.progreso);
                  }else{
                    setProgress([0]);
                    console.log(res.message);
                  }
                });

              }else{
                console.log(res.message);
              }
            });
          } else {
            //si no hay actividades asigna actividades
            uri = `${url}eventos/obtenerActividades/${idEvento}/${id}`;
            api = new Api(uri, "GET");
            api.call().then((res) => {
              if(res.response){
                setMarcas(res.result);

                uri = `${url}eventos/actualizarProgreso/${id}/${idEvento}`;
                api = new Api(uri, "GET");
                api.call().then((res) => {
                  if(res.response){
                    setProgress(res.result.progreso);
                  }else{
                    setProgress([0]);
                    console.log(res.message);
                  }
                });

              }else{
                console.log(res.message);
              }
            });
          }
        });
      }
      traerActividades();

      const getLocationPermission = async() => {
        //esto abre la ventana que pregunta si puedes usar su ubicacion
        let { status } = await Location.requestForegroundPermissionsAsync();
        if(status !== 'granted'){
          alert('Permiso denegado');
          return;
        }
        let location = await Location.getCurrentPositionAsync({});

        const current = {
          latitude: location.coords.latitude,
          longitude: location.coords.longitude
        }
        if(origin.latitude != current.latitude || origin.longitude != current.longitude){
          setOrigin(current)
          setUserPosition(current);
        }
      }
      getLocationPermission();

      //modificara la vista solo si la pantalla se encuentre focuseada y permite modificar la lista de actividades cuando se escanea un qr para completar una actividad
      if(focused){
        notificationListener.current = Notifications.addNotificationReceivedListener((notification) => {
          if(notification.request.content.data.iden === "ADECHT"){
            consumir(notification.request.content.data.idPersona);
          }
        });
      }
    }, [marcas.lenght]
    )
  )
  
  //funcion para ir a la ubicacion del usuario
  const irAMiUbicacion = () => {
    if(irA == 1){
      focus.current = false;
      setIrA(2);
      coor.current.longitude = origin.longitude;
      coor.current.latitude = origin.latitude;
    }else{
      focus.current = false;
      setIrA(1);
      coor.current.longitude = origin.longitude;
      coor.current.latitude = origin.latitude;
    }
  }
  //#endregion

  //#region Bottom Sheet
  // ref
  const bottomSheetRef = React.useRef(null);

  //tamaños en los que abrira la ventana
  const snapPoints = ['33%', '80%'];
  
  //cuando cambia la pantalla pone la capa negra sobre la pantalla
  const handleSheetChange = React.useCallback(
    (index) => {
      if(index == 1){
        setIsOpen(true);
      }else{
        setIsOpen(false);
      }
    }
  ); 

  //opacidad de el backdrop del bottomsheet
  const CustomBackdrop = ({animatedPosition, animatedIndex, style }) => {
    // variables de animacion
    const containerAnimatedStyle = useAnimatedStyle(() => ({
      opacity: interpolate(
        animatedIndex.value,
        [0, 1],
        [0, 1],
        Extrapolate.CLAMP
      )
    }));
  
    // estilos para el backdrop
    const containerStyle = React.useMemo(
      () => [
        style,
        {
          // backgroundColor: "#00000050",
          top: 50,
          display: isOpen ? 'flex' : 'none'
        },
        containerAnimatedStyle,
      ],
      [style, containerAnimatedStyle]
    );
  
    return <Animated.View style={containerStyle} />;
  };

  //#endregion

  //#region Popup información

  //funcion para abrir y cerrar el popup
  const abrirCerrar = () => {
    if(visible){
      setVisible(false);
    }else{
      setVisible(true)
    }
  }

  const abrir = (datosI) =>{
      setVisible2({
        visibility: true,
        datos: datosI
    });
  }

  const cerrar = (datosI) =>{
    setVisible2({
      visibility: false,
      datos: datosI
    });
  }

  const notificacionAC = () =>{
    if(visible3){
      setVisible3(false);
    }else{
      setVisible3(true);
    }
  }
  //#endregion

  //funcion para actualizar actividades completadas
  const consumir = async (idP) => {
        let uri = "";
        let api = null;
        let idEvento = await getValueFor('idEvento');

        uri = `${url}eventos/traerListaActividades/${idEvento}/${idP}`;
        api = new Api(uri, "GET");
        api.call().then((res) => {
          if(res.response){
            setMarcas(res.result);
            uri = `${url}eventos/actualizarProgreso/${idP}/${idEvento}`;
            api = new Api(uri, "GET");
            api.call().then((res) => {
              if(res.response){
                cerrar(visible2.datos);
                notificacionAC();
                setProgress(res.result.progreso);
              }else{
                console.log(res.message);
              }
            });
          }else{
            console.log(res.message);
          }
        });
  }
  const [loaded] = useFonts({
    "Montserrat-Bold": require("../../../../assets/fuentes/Montserrat-Bold.ttf"),
    "Montserrat-Italic": require("../../../../assets/fuentes/Montserrat-Italic.ttf"),
    "Montserrat-Light": require("../../../../assets/fuentes/Montserrat-Light.ttf"),
    "Montserrat-Medium": require("../../../../assets/fuentes/Montserrat-Medium.ttf"),
    "Montserrat-Regular": require("../../../../assets/fuentes/Montserrat-Regular.ttf"),
    "Montserrat-SemiBold": require("../../../../assets/fuentes/Montserrat-SemiBold.ttf"),

  });
  if (!loaded) {
    return null;
  }

  return (
    <View style={styles.container}>
      <GestureHandlerRootView style={styles.container2}>
        
        <View style={styles.header}>
          <TouchableOpacity onPress={() => navegacion.navigate("inicio")}>
            <FontAwesomeIcon style={styles.volver} icon={FAW.faArrowLeft} size={28}/>
          </TouchableOpacity>
          {/* Envviando un objeto hacia la ventana de progress */}
          <TouchableOpacity style={styles.florC} onPress={() => navegacion.navigate("progressScreen")}>
            <ImageBackground style={styles.flor} source={progreso[progress].route} resizeMode={"contain"}></ImageBackground>
          </TouchableOpacity>
        </View>
        
        <MapView
          style={styles.map}
          region={(()=>{
            // aumentando count para identificar cuando se cambia a false focus
            count.current++;
            // ubicacion cuando se centra la region donde esta el usuario
            if (focus.current) {
              change.current = true;
              return {
                latitude: origin.latitude,
                longitude: origin.longitude,
                latitudeDelta: 0.003,
                longitudeDelta: 0.003,
              }
            // ubicacion cuando no se sigue la zona del usuario
            } else {
              if(!change.current){
                focus.current = true;
              }
              if (!change.current) {
                return {
                  latitude: origin.latitude,
                  longitude: origin.longitude,
                  latitudeDelta: 0.003,
                  longitudeDelta: 0.003,
                }
              } else {
                  return {
                    latitude: coor.current.latitude,
                    longitude: coor.current.longitude,
                    latitudeDelta: coor.current.latitudeDelta,
                    longitudeDelta: coor.current.longitudeDelta,
                  }
              }
            }
          })()}
          followsUserLocation={true}
          rotateEnabled={true}
          provider={MapView.PROVIDER_GOOGLE}
          showsCompass={true}
          onRegionChangeComplete={region => { 
            if (change.current) {
              if (count.current > 5) {
                focus.current = false;
              }
              coor.current = region;
            }
          }}
        >
          {/* ubicacion del usuario */}
          <Marker
            coordinate={{
              latitude: userPosition.latitude ? userPosition.latitude : 20.1743588,
              longitude: userPosition.longitude ? userPosition.longitude : -98.05111,
            }}
            title="Yo"
            description='Esta es tu ubicacion actual'
          >
            <Image style={{bottom: -10, left: 5, height: 80, width: 80}} source={require('../../../../assets/meSprite.png')}/>
          </Marker>
        
          {
            marcas ? (marcas.map(marker => (
              <Marker
              coordinate={{
                longitude: parseFloat(marker.longitud),
                latitude: parseFloat(marker.latitud)
              }}
              title={marker.titulo}
              description={marker.descripcionCorta}
              key={marker.idActividad}
            >
              {
                marker.completado ? (
                  <Image style={{left: 3, height: 40, width: 40}} source={require('../../../../assets/sombraCompletada2.png')}/>
                ):(
                  <Image style={{left: 3, height: 40, width: 40}} source={require('../../../../assets/sombra.png')}/>
                )
              }

            </Marker>
            ))
            ) : (
              console.log("Sin actividades puede que ya hayan sido completadas o no se ha obtenido una respuesta de la base de datos")
            )
          }
        </MapView>

        <TouchableOpacity style={styles.ubicameT} onPress={() => irAMiUbicacion()}>
          <View style={styles.ubicameC}>
            <FontAwesomeIcon icon={FAW.faLocation} color={"#fff"} size={30}/>
          </View>
        </TouchableOpacity>

        <BottomSheet
          style={styles.sheet}
          backgroundStyle={styles.sombra}
          handleStyle={{top: 10}}
          ref={bottomSheetRef}
          snapPoints={snapPoints}
          onChange={handleSheetChange}
          overDragResistanceFactor={0}
          backdropComponent={CustomBackdrop}
          >
          <TouchableOpacity style={styles.infoO} onPress={() => abrirCerrar()}>
            <View style={styles.infoC}>
              <FontAwesomeIcon icon={FAW.faInfoCircle} size={25}/>
              <Text style={styles.infoText} allowFontScaling={false}>Info</Text>
            </View>
          </TouchableOpacity>
          {!marcas || marcas.length == 0 ? (
            <Text style={styles.mensaje} allowFontScaling={false}>No hay actividades</Text>
          ):(
            <FlatList
              style={styles.flat}
              scrollEnabled={isOpen}
              overScrollMode='never'
              data={marcas}
              keyExtractor={(item) => item.idActividad }
              renderItem={({item}) => (
                <TouchableOpacity  onPress={() => abrir(item)}>
                  <View style={styles.misionC}>
                    <Image source={{uri: item.imagen}} style={styles.misionI}></Image>
                    <View style={styles.misionCT}>
                      <Text style={styles.misionT} allowFontScaling={false}>{item.titulo}</Text>
                      <Text style={styles.misionD} allowFontScaling={false}>{item.descripcionCorta}</Text>
                    </View>
                    {item.completado ? (
                      <FontAwesomeIcon icon={FAW.faCheckCircle} style={styles.completada} color={'#86D65D'} size={25}/>
                    ):(
                      <View></View>
                    )}
                  </View>
                </TouchableOpacity> 
              )}
            />
          )}
        </BottomSheet>

      </GestureHandlerRootView>

      <Modal
        isVisible={visible}
        animationIn={'fadeIn'}
        animationInTiming={700}
        animationOut={'fadeOut'}
        animationOutTiming={700}
        onBackdropPress={abrirCerrar}
        backdropTransitionOutTiming={0}
        deviceHeight={Dimensions.get("window").height}
        deviceWidth={Dimensions.get("window").width}
        backdropOpacity={.5}
      >
        <View style={styles.ayudaC}>
          <View style={styles.iPopC}>
            <ImageBackground style={styles.iPop} source={require("../../../../assets/htlogo.png")} resizeMode={'contain'}></ImageBackground>
          </View>
          <Text style={styles.tituloPop} allowFontScaling={false}>¿Como funcionan las actividades y los eventos de temporada?</Text>
          <View style={styles.textoPopC}>
            <ScrollView overScrollMode='never'>
              <Text style={styles.textoPop} allowFontScaling={false}>{informative + informative}</Text>
            </ScrollView>
          </View>
          <TouchableOpacity style={styles.touchPop}>
            <Text style={styles.entendidoPop} onPress={abrirCerrar} allowFontScaling={false}>Entendido</Text>
          </TouchableOpacity>
        </View>
      </Modal>

      <Modal
        isVisible={visible2.visibility}
        animationIn={"slideInRight"}
        animationInTiming={700}
        animationOut={'slideOutRight'}
        animationOutTiming={700}
        onBackdropPress={()=>cerrar(visible2.datos)}
        backdropTransitionOutTiming={1000}
        deviceHeight={Dimensions.get("window").height}
        deviceWidth={Dimensions.get("window").width}
        backdropOpacity={.5}
      >
        <View style={styles.actividadC}>
          <View style={styles.iActividadC}>
            <ImageBackground style={styles.actividadI} source={{uri: visible2.datos.imagen}} resizeMode={'cover'}></ImageBackground>
          </View>
          <Text style={styles.tituloActividad} allowFontScaling={false}>{visible2.datos.titulo}</Text>
          <Text style={styles.textoActividad} allowFontScaling={false}>{visible2.datos.descripcionLarga}</Text>
            {visible2.datos.completado ? (
              <View style={styles.qrActividadC}>
                <Text style={styles.CompT} allowFontScaling={false}>Actividad Completada</Text>
                <FontAwesomeIcon icon={FAW.faCheckCircle} color={'#86D65D'} size={130}/>
              </View>
              ):( 
              <View style={styles.qrActividadC}>
                <Text style={styles.qrActividad} allowFontScaling={false}>Escanea este QR cuando completes la actividad</Text>
                <QRCode
                  value={global.btoa("Evento" + "-" + visible2.datos.idActividad + "-" + persona)}
                  size={150}
                  color={"#e6078e"}
                />
              </View>
              )
            }
        </View>
      </Modal >
     
      <Modal
        isVisible={visible3}
        animationIn={"fadeIn"}
        animationInTiming={700}
        animationOut={"fadeOut"}
        animationOutTiming={700}
        onBackdropPress={notificacionAC}
        backdropTransitionOutTiming={1000}
        deviceHeight={Dimensions.get("window").height}
        deviceWidth={Dimensions.get("window").width}
        backdropOpacity={.5}
      >
        <View style={styles.notificationC}>
          <Text style={styles.textoNotification} allowFontScaling={false}>Actividad Completada</Text>
          <FontAwesomeIcon icon={FAW.faCheckCircle} color={'#86D65D'} size={65}/>
          <TouchableOpacity onPress={() => notificacionAC()} style={styles.x}>
            <FontAwesomeIcon icon={FAW.faX} size={20}/>
          </TouchableOpacity>
        </View>
      </Modal>

    </View>
  )
}

//estilos
const styles = StyleSheet.create({
  container: {
    flex: 1, 
    backgroundColor: '#fff',
    alignItems: 'center',
    flexDirection: "column",
    height: '100%',
    width: '100%',
    justifyContent: 'center',
    paddingTop: Platform.OS == 'ios' ? Constants.statusBarHeight : 0
  },
  container2: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'flex-start',
    flexDirection: "column",
    height: '100%',
    width: '100%',
    position: 'relative'
  },
  map: {
    width : '100%',
    height: '61.5%'
  },
  header: {
    height: '7%',
    width: '100%',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingHorizontal: 20
  },
  volver: {
    color: '#000'
  },
  florC: {
    height: '70%',
    width: "15%",
  },
  flor: {
    height: '100%',
    width: '100%',
    alignSelf: 'center'
  },
  sheet: {
    position: 'relative',
    flex: 1,
  },
  mensaje: {
    zIndex: 1,
    alignSelf: 'center',
    marginTop: '20%',
    fontFamily: 'Montserrat-SemiBold'
  },
  sombra: {
    shadowColor: "#000",
    shadowOffset: {
    	width: 0,
    	height: 12,
    },
    shadowOpacity: 0.58,
    shadowRadius: 16.00,
    elevation: 24,
  },
  infoO: {
    position: 'absolute',
    width: 70,
    height: 30,
    zIndex: 5,
    left: 10
  },
  infoC: {
    width: '100%',
    height: '100%',
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center'
  },
  infoText:{
    fontFamily: 'Montserrat-SemiBold'
  },
  flat: {
    width: '100%',
    marginTop: 35,
    zIndex: 4
  },
  misionC: {
    height: 120,
    width: '90%',
    alignSelf: 'center',
    marginTop: 15,
    position: 'relative',
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center'
  },
  misionI: {
    height: '80%',
    width: "30%",
    borderRadius: 25,
    overflow: 'hidden',
    resizeMode: 'cover'
  },
  misionCT: {
    width: '70%',
    height: '100%',
    flexDirection: 'column',
    paddingLeft: 20
  },
  misionT: {
    color: "#000",
    fontFamily: 'Montserrat-SemiBold',
    fontSize: 20,
    marginTop: 10,
  },
  misionD: {
    color: "#000",
    fontFamily: 'Montserrat-Light',
    fontSize: 12,
    marginTop: 10,
    textAlign: 'justify',
    width: '75%'
  },
  ubicameT: {
    position: 'absolute',
    width: 60,
    height: 60,
    backgroundColor: '#e6078e',
    overflow: 'hidden',
    borderRadius: 30,
    top: "57%",
    right: "5%"
  },
  ubicameC: {
    width: "100%",
    height: "100%",
    alignItems: "center",
    justifyContent: "center"
  },
  ayudaC: {
    height: "80%",
    width: "90%",
    alignSelf: "center",
    backgroundColor: "#fff",
    borderRadius: 30,
    overflow: "hidden",
    flexDirection: "column",
    alignItems: "center",
    padding: "5%"
  },
  iPopC: {
    height: "10%",
    width: "20%"
  },
  iPop: {
    height: "100%",
    width: "100%"
  },
  tituloPop: {
    fontFamily: "Montserrat-SemiBold",
    fontSize: 17,
    marginTop: "5%",
    textAlign: 'center',
  },
  textoPop: {
    fontFamily: "Montserrat-Light",
    fontSize: 15,
    textAlign: "justify",
    color: "#000"
  },
  textoPopC: {
    marginTop: "10%",
    height: '60%'
  },
  touchPop: {
    position: 'relative',
    marginTop: '10%'
  },
  entendidoPop: {
    fontFamily: "Montserrat-Bold",
    fontSize: 22,
    color: "#e6078e"
  },
  actividadC:{
    height: "110%",
    width: "80%",
    alignSelf: "flex-end",
    backgroundColor: "#fff",
    position:"absolute",
    overflow: "hidden",
    flexDirection: "column",
    alignItems: "center",
    padding: "15%",
    paddingTop: "30%",
    right: "-5%",
  },
  iActividadC: {
    height: 110,
    width: 110,
    overflow: "hidden",
  },
  actividadI: {
    height: "100%",
    width: "100%",
  },
  tituloActividad: {
    fontFamily: "Montserrat-SemiBold",
    fontSize: 22,
    marginTop: "15%"
  },
  textoActividad: {
    fontFamily: "Montserrat-Regular",
    fontSize: 14,
    marginTop: "8%",
    textAlign: "center",
    color: "#000"
  },
  qrActividadC: {
    height: "40%",
    width: "100%",
    marginTop: "15%",
    alignItems: "center",
    justifyContent: 'space-around'
  },
  qrActividad: {
    fontFamily: "Montserrat-SemiBold",
    fontSize: 18,
    marginTop: "10%",
    textAlign: "center"
  },
  CompT: {
    fontFamily: "Montserrat-Bold",
    fontSize: 18,
    marginTop: "10%",
    textAlign: "center",
    color: "#e6078e"
  },
  completada:{
    position: 'absolute',
    right: "0%"
  },
  notificationC:{
    height: "30%",
    width: "90%",
    alignSelf: "center",
    backgroundColor: "#fff",
    borderRadius: 30,
    overflow: "hidden",
    flexDirection: "column",
    alignItems: "center",
    padding: 10,
    justifyContent: "center"
  },
  textoNotification:{
    fontFamily: "Montserrat-Bold",
    fontSize: 20,
    marginBottom: 30,
    color: "#e6078e"
  },
  x:{
    position: "absolute",
    top: 20,
    right: 20
  }
});