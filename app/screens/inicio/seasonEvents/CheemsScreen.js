import * as React from 'react';
import { Alert, LogBox, View, Text, Image, StyleSheet, TouchableOpacity, ImageBackground, Dimensions, StatusBar, ScrollView, Linking } from 'react-native';
import { SafeAreaView } from 'react-native-safe-area-context';
import { GestureHandlerRootView } from 'react-native-gesture-handler';
import BottomSheet from '@gorhom/bottom-sheet';
import * as FAW from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import { useNavigation } from '@react-navigation/native';
import { useFonts } from 'expo-font';
import { useFocusEffect, useIsFocused } from '@react-navigation/native';
import { getValueFor,saveValue } from "../../../utils/localStorage";
import { url } from '../../../utils/config';
import Api from "../../../utils/Api";
import Constants from 'expo-constants';
import QRCode from 'react-native-qrcode-svg';
import Modal from 'react-native-modal';
import * as Notifications from 'expo-notifications';
import { Platform } from 'react-native';
import { Ionicons } from '@expo/vector-icons';

export default function CheemsScreen() {

    //Puesto del usuario
    const [posicion, setPosicion] = React.useState(0);
    const [persona, setPersona] = React.useState(0);
    const notificationListener = React.useRef();
    
    const navegacion = useNavigation();

    //saber si el modal debe ser visible o no
    const [visible, setVisible] = React.useState(false);

    //verificar si el premio esta o no esta reclamado
    const [premio, setPremio] = React.useState({});

    // texto para modal informativo
    const [informative, setInformative] = React.useState("Cargando...");
    
    //#region Bottom Sheet
    const bottomSheetRef = React.useRef(null);
    const snapPoints = ['14%', '80%'];
    //#endregion
    
    //Verificar si la pantalla esta en Focus
    const focused = useIsFocused();

    //#region Popup información
    //funcion para abrir y cerrar el popup
    const abrirCerrar = () => {
      if(visible){
        setVisible(false);
      }else{
        setVisible(true)
      }
    }
    //#endregion  

    //#region Traer Datos 
    // funcion para cambiar imagen dependiendo el lugar
    const traerMedalla = (posicion) => {
        switch (posicion) {
            case 1:
                return {
                    rute: require('../../../../assets/medalla1.png'),
                    leyenda: 'Has alcanzado el primer lugar'
                }
                break;
            case 2:
                return {
                    rute: require('../../../../assets/medalla2.png'),
                    leyenda: 'Has alcanzado el segundo lugar'
                }
                break;
            case 3:
                return {
                    rute: require('../../../../assets/medalla3.png'),
                    leyenda: 'Has alcanzado el tercer lugar'
                }
                break;
            default:
                return {
                    rute: require('../../../../assets/nopuemdeser.png'),
                    leyenda: 'Casi pero no pegaste'
                }
                break;
        }
    }

    // Verificamos sesion iniciada y obtenemos los datos del premio 
    useFocusEffect(
        React.useCallback(() => {
            if(Platform.OS == 'ios'){
                StatusBar.setBarStyle('light-content');
            }

            // obtiene el texto para modal de informacion 
            const getInformative = async () => {
                let uri = "";
                let api = null;
                let parametros = {
                modal: 2
                }
                uri = `${url}eventos/getInformative`;
                api = new Api(uri, "POST", parametros);
                api.call().then((res) => {
                if(res.response){
                    setInformative(res.result.textInformative);
                }else{
                    setInformative(res.message);
                }
                });
            }
            getInformative();

            // Verifica si hay una sesion iniciada
            const checkLogin = async () =>{

                let id = await getValueFor('id');
                let token = await getValueFor('token');    
        
                if(!id && !token){
                  // si no hay sesion iniciada
                  navegacion.navigate("inicio");
                }else{
                    setPersona(id);
                }
              }
            checkLogin();
            
            //Verificación de puesto del usuario.
            const verificarPuestoDelUsuario = async() => {
                let id = await getValueFor('id');
                let idEvento = await getValueFor('idEvento');
                let uri = `${url}eventos/obtenerPocision/${idEvento}/${id}`;
                let api = new Api(uri, "GET");
                api.call().then((res) => {
                        if(res.response){
                            setPosicion(JSON.parse(res.result.puesto));
                            let uri = `${url}eventos/obtenerPremio/${res.result.puesto}/${idEvento}`;
                            let api = new Api(uri, "GET");
                            api.call().then((res) => {
                                    if(res.response){
                                        setPremio(res.result);
                                    }else{
                                        console.log(res.message);
                                    }
                            });
                        }else{
                            console.log(res.message);
                        }
                });
            }
            verificarPuestoDelUsuario();

            if(focused){
                notificationListener.current = Notifications.addNotificationReceivedListener((notification)=>{
                    if(notification.request.content.data.iden === "PRDHT"){
                        verificarPuestoDelUsuario();
                    }
                })
            }
        }, []
        )
    );
    //#endregion
    
    const [loaded] = useFonts({
        "Montserrat-Bold": require("../../../../assets/fuentes/Montserrat-Bold.ttf"),
        "Montserrat-Italic": require("../../../../assets/fuentes/Montserrat-Italic.ttf"),
        "Montserrat-Light": require("../../../../assets/fuentes/Montserrat-Light.ttf"),
        "Montserrat-Medium": require("../../../../assets/fuentes/Montserrat-Medium.ttf"),
        "Montserrat-Regular": require("../../../../assets/fuentes/Montserrat-Regular.ttf"),
        "Montserrat-SemiBold": require("../../../../assets/fuentes/Montserrat-SemiBold.ttf"),
    });

    if (!loaded) {
        return null;
    }
    
return (
    <View style={styles.container}>
        {(() => {
            if(posicion != 0){
                if(posicion <= 3 && posicion >= 1){
                    return (
                        <GestureHandlerRootView style={styles.container2}>
                            <ImageBackground style={styles.imagenFondo} resizeMode='cover' source={require('../../../../assets/backgroundMedallas.png') }>
                                <TouchableOpacity style={styles.regresoCF} onPress={()=> navegacion.navigate('progressScreen')}>
                                    <FontAwesomeIcon style={styles.regresoF} size={28} icon={FAW.faArrowLeft}/>
                                </TouchableOpacity>
                                <View style={styles.cajaTitulo1}>
                                    <Text style={styles.titulo1} allowFontScaling={false}>¡FELICIDADES!</Text>
                                </View>
                                <View style={styles.cajaTitulo2}>
                                    <Text style={[styles.titulo1, styles.titulo2]} allowFontScaling={false}>{traerMedalla(posicion).leyenda}</Text>
                                </View> 
                                <View style={styles.cajaImagenMedalla}>
                                    <ImageBackground style={styles.imagenMedalla} source={traerMedalla(posicion).rute} resizeMode={'contain'}></ImageBackground>
                                </View>
                            </ImageBackground>

                            <BottomSheet
                                style={styles.sheet}
                                backgroundStyle={styles.sombra}
                                ref={bottomSheetRef}
                                snapPoints={snapPoints}
                            >
                                <TouchableOpacity style={styles.infoO} onPress={() => abrirCerrar()}>
                                    <View style={styles.infoC}>
                                      <FontAwesomeIcon icon={FAW.faInfoCircle} size={20}/>
                                      <Text style={styles.infoText} allowFontScaling={false}>Info</Text>
                                    </View>
                                </TouchableOpacity>

                                { premio != {} && premio != null && premio != [] ?
                                    (()=>{
                                        if(premio.reclamado == "noReclamado"){
                                            let premio2 = {};
                                            premio2.titulo = premio.titulo;
                                            premio2.descripcion = premio.descripcion;
                                            premio2 = JSON.stringify(premio2);
                                            return(
                                                <View style={{flexDirection: 'column', height: 200, marginTop: 5, paddingHorizontal: 24}} >
                                                    {/* <Text style={styles.titulo4} allowFontScaling={false}>Reclama tu premio</Text> */}
                                                    <Text style={[styles.titulo4, {marginBottom: 10, width: '100%'}]} allowFontScaling={false}>¡Felicidades! ganaste:</Text>
                                                
                                                    <Text style={styles.titulo6} allowFontScaling={false}>{premio.descripcion}</Text>
                                                    <ImageBackground source={{uri: premio.imagen}} style={{width: 85, height: 85, alignSelf: 'center'}} resizeMode={'cover'}></ImageBackground>
                                                    <Text style={[styles.titulo4, {fontSize: 20, marginTop: 10}]} allowFontScaling={false}>{premio.titulo}</Text>
                                                
                                                    <View style={styles.qrActividadC}>
                                                        <QRCode
                                                          value={global.btoa("Premio" + "-" + premio.idPremio + "-" + premio2 + "-" + persona)}
                                                          size={180}
                                                          color={"#e6078e"}
                                                          logo={require('./../../../../assets/iconoQR.png')}
                                                          logoSize={60}
                                                          logoMargin={10}
                                                          logoBackgroundColor='transparent'
                                                        />
                                                        <Text style={styles.qrActividad} allowFontScaling={false}>Por favor, escanea este QR para poder obtener tu premio.</Text>
                                                    </View>
                                                </View>
                                            )
                                        }else if (premio == "reclamado"){
                                            return(
                                                <View style={{height:'100%', alignItems:'center', marginTop: 2}}>
                                                    
                                                    <View style={{flexDirection: 'column', height: 1000 }}>

                                                        <Text style={styles.titulo4} allowFontScaling={false}>¡Premio reclamado!</Text>
                                                        <Text style={[styles.titulo6, {marginTop: 40, fontSize: 25, fontFamily: 'Montserrat-SemiBold', marginHorizontal: 40, marginBottom: 10}]} allowFontScaling={false}>Gracias por participar en este evento de Huauchitour, esperamos verte en el próximo.</Text>
                                                        <View style={{height:250, justifyContent:'center'}}>
                                                        
                                                            <ImageBackground 
                                                                source={require("../../../../assets/check-mark-verified.png" )} 
                                                                style={{width: 150, height: 150, alignSelf:'center', justifyContent:'center',
                                                                marginTop: -70}}
                                                                resizeMode={'contain'}
                                                            >
                                                            </ImageBackground>
                                                        </View>

                                                        <TouchableOpacity style={{alignContent:'center', alignItems:'center', width: 200, alignSelf: 'center', marginTop: -50}} onPress={() => navegacion.navigate('inicio')}>
                                                            <View style={{backgroundColor:"#f47fa7", width: '100%', height:50, textAlign:'center', justifyContent:'center', alignItems:'center', borderRadius:20}}>
                                                                <Text style={{color:"#fff", fontSize:20, fontWeight:'bold'}} allowFontScaling={false}>Volver al inicio</Text>
                                                            </View>
                                                        </TouchableOpacity>

                                                    </View>

                                                </View>
                                            )
                                        } else {
                                            return(
                                            <View style={{flexDirection: 'column', height: 200, marginTop: 5, width: '100%', justifyContent: 'center', alignItems: 'center'}}>
                                                <Text allowFontScaling={false} style={styles.tituloPop}>Error, porfavor intentalo de nuevo mas tarde.</Text>
                                            </View>
                                            )
                                        }                
                                    })()
                                :
                                   <View style={{flexDirection: 'column', height: 200, marginTop: 5, width: '100%', justifyContent: 'center', alignItems: 'center'}}>
                                        <Text allowFontScaling={false} style={styles.tituloPop}>Error, porfavor intentalo de nuevo mas tarde.</Text>
                                   </View>
                                }
                                
                            </BottomSheet>
                        </GestureHandlerRootView>
                    )
                }else{
                    return (
                        <View
                            View style={styles.container2}>
                            <TouchableOpacity style={styles.regresoO} onPress={() => navegacion.navigate('progressScreen')}>
                                <View style={styles.regresoC}>
                                    <FontAwesomeIcon style={styles.regreso} size={28} icon={FAW.faArrowLeft}/>
                                </View>
                            </TouchableOpacity>
                            <View style={[styles.cajaTitulo1, {marginTop: 50}]}>
                                <Text style={[styles.titulo1, {color: '#00c7d6'}]} allowFontScaling={false}>¡NO PUEMDE SER!</Text>
                            </View>
                            <View style={styles.cajaTitulo2}>
                                <Text style={[styles.titulo1, styles.titulo2, styles.titulo3]} allowFontScaling={false}> "No has logrado ganar, sigue participando."</Text>
                            </View>
                            <View style={styles.cajaImagenMedalla}>
                                <ImageBackground style={styles.imagenMedalla} source={traerMedalla(posicion).rute} resizeMode={'contain'}></ImageBackground>
                            </View>
                            <TouchableOpacity style={styles.volverInicioT} onPress={() => navegacion.navigate('inicio')}>
                                <View style={styles.volverInicioC}>
                                    <Text style={styles.volverInicio} allowFontScaling={false}>Volver al inicio</Text>
                                </View>
                            </TouchableOpacity>
                        </View>
                    )
                }
            } else {
                return(
                    <Text style={[styles.titulo1, {color: "#000", fontSize: 25}]} allowFontScaling={false}>Cargando...</Text>
                )
            }
        })()}

        <Modal
        isVisible={visible}
        animationIn={"fadeIn"}
        animationInTiming={700}
        animationOut={'fadeOut'}
        animationOutTiming={700}
        onBackdropPress={abrirCerrar}
        backdropTransitionOutTiming={0}
        deviceHeight={Dimensions.get("window").height}
        deviceWidth={Dimensions.get("window").width}
        backdropOpacity={.5}
        >
            <View style={styles.ayudaC}>
                <ScrollView 
                  overScrollMode='never' 
                  contentContainerStyle={{
                    flexDirection: "column",
                    alignItems: "center"
                  }}
                >
                    <View style={styles.iPopC}>
                        <ImageBackground style={styles.iPop} source={require("../../../../assets/htlogo.png")} resizeMode={'cover'}></ImageBackground>
                    </View>
                    <Text style={styles.tituloPop} allowFontScaling={false}>¿Donde puedo reclamar mi premio?</Text>
                    <Text style={styles.textoPop} allowFontScaling={false}>{informative}</Text>
                    {premio.direccion ? 
                        <View style={{width: '100%', marginTop: "2%"}}>
                            <View style={styles.namePE}>
                                <Text allowFontScaling={false} style={styles.tituloPop}>{premio.nombreCentro ? premio.nombreCentro : premio.nombre}</Text>
                            </View>
                            <TouchableOpacity 
                              activeOpacity={1} 
                              style={styles.directionC}
                              onPress={()=>{
                                let latitud = parseFloat(premio.latitud);
                                let longitud = parseFloat(premio.longitud);
                                let googleMapsUrl = `https://www.google.com/maps/search/?api=1&query=${latitud},${longitud}`;
                                Linking.openURL(googleMapsUrl);
                              }}        
                            >
                                <FontAwesomeIcon
                                    icon={FAW.faLocationArrow}
                                    size={25}
                                    style={{color: '#e6078e', marginRight: 10}}
                                />
                                <Text allowFontScaling={false} style={[styles.textoPop, {marginTop: 0, width: "80%"}]}>{premio.direccion}</Text>
                            </TouchableOpacity>
                        </View>
                    :
                        <></>
                    }
                    <TouchableOpacity style={styles.touchPop}>
                        <Text style={styles.entendidoPop} onPress={abrirCerrar} allowFontScaling={false}>Entendido</Text>
                    </TouchableOpacity>
                </ScrollView>
            </View>
      </Modal>
    </View>
)};

const styles = StyleSheet.create({
    container: {
      flex: 1, 
      backgroundColor: '#fff',
      alignItems: 'center',
      justifyContent: 'center',
      flexDirection: "column",
      height: '100%',
      width: '100%',
    },
    container2: {
      flex: 1,
      alignItems: 'center',
      justifyContent: 'flex-start',
      flexDirection: "column",
      height: '100%',
      width: '100%',
      position: 'relative'
    },
    regresoO: {
      width: '12%',
      height: '6%',
      left: 20,
      alignSelf: 'flex-start'
    },
    regresoC:{
      position:'relative',
      justifyContent: 'center',
      width: '100%',
      height:'100%'
    },
    regreso:{
        color: "#c9c9c9",
        fontSize: 50
    },
    volverInicioT: {
      width: '100%',
      height: '6%',
      alignSelf: 'center',
      marginTop: 20
    },
    volverInicioC:{
      position:'relative',
      justifyContent: 'center',
      alignItems: 'center',
      width: '100%',
      height:'100%'
    },
    volverInicio:{
        color: "#e6078e",
        fontSize: 20,
        fontFamily: 'Montserrat-SemiBold'
    },
    imagenFondo:{
        flex: 1,
        width: '100%',
        height: '100%',
        flexDirection: 'column',
        alignItems: 'center',
        paddingTop: Platform.OS == 'ios' ? Constants.statusBarHeight : 0
    },
    cajaTitulo1:{
        marginTop: '20%',
        width: '100%',
        height: '7%',
        justifyContent: 'center',
        alignItems:'center',
        flexDirection: 'row'
     },
    titulo1:{
        color: "#fff",
        fontSize: 38,
        fontWeight: 'bold',
        textAlign: 'center',
     }, 
    cajaTitulo2:{
        width: '80%',
        height: '15%',
        alignItems: 'center'
     },
    titulo2:{
        fontSize: 22,
        fontFamily: 'Montserrat-SemiBold',
        fontStyle: 'italic'
     }, 
    cajaImagenMedalla:{
        width:'100%',
        height:'45%',
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: '10%',
    },
    imagenMedalla:{
        height: '100%',
        width: '100%'
    },
    titulo3:{
        color: '#000', 
        fontStyle: 'normal', 
        marginTop: 30
    },
    titulo4: {
        color: "#000",
        fontSize: 35,
        fontWeight: 'bold',
        textAlign: 'center'
    },
    titulo5: {
        color: "#000",
        fontSize: 20,
        fontWeight:'bold',
        textAlign:'center'
    },
    titulo6: {
        color: "#000",
        fontSize: 16,
        textAlign:'center',
        marginBottom: 20,
    },
    titulo7: {
        color: "#000",
        fontSize: 25,
        fontWeight: 'bold',
        textAlign: 'center',
        alignItems:'center',
    },
    regresoCF: {
        width: '10%',
        position: 'absolute',  
        justifyContent: 'center',
        alignSelf:'flex-start',
        left: 20, 
        marginTop: "5%"
    },
    regresoF: {
        fontSize: 50,
        color: "#fff",
        marginTop: Platform.OS == 'ios' ? Constants.statusBarHeight : 0
    },
    infoO: {
        position:'relative',
        width: 70,
        height: 30,
        zIndex: 5,
        left: 10
    },
    infoC: {
        width: '100%',
        height: '100%',
        flexDirection: 'row',
        justifyContent: 'space-around',
        alignItems: 'center'
    },
    infoText:{
        fontFamily: 'Montserrat-SemiBold'
    },
    ayudaC: {
        height: "80%",
        width: "90%",
        alignSelf: "center",
        backgroundColor: "#fff",
        borderRadius: 30,
        overflow: "hidden",
        padding: "5%"
    },
    iPopC: {
        height: 50,
        width: 50
    },
    iPop: {
        height: "100%",
        width: "100%"
    },
    tituloPop: {
        fontFamily: "Montserrat-SemiBold",
        fontSize: 17,
        textAlign: 'center',
        marginTop: "10%"
    },
    textoPop: {
        fontFamily: "Montserrat-Light",
        fontSize: 15,
        textAlign: "justify",
        marginTop: "10%",
        color: "#000"
    },
    touchPop: {
        position: 'relative',
        marginTop: "10%",
        marginBottom: "10%",
    },
    entendidoPop: {
        fontFamily: "Montserrat-Bold",
        fontSize: 25,
        color: "#e6078e"
    },
    namePE:{
        flexDirection: 'column'
    },
    directionC: {
        width: '100%',
        flexDirection: 'row',
        alignItems: 'center',
        marginTop: '5%'
    },
    qrActividadC: {
        height: "100%",
        width: "100%",
        marginTop: "15%",
        alignItems: "center",
        justifyContent: 'space-around'
    },
    qrActividad: {
        fontFamily: "Montserrat-Italic",
        fontSize: 14,
        marginTop: "23%",
        margin:29,
        textAlign:'center',
        fontWeight:'normal',
        fontStyle: 'italic'
    },
    sheet: {
        flex: 1
    },
    sombra: {
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 12,
        },
        shadowOpacity: 0.58,
        shadowRadius: 16.00,
        elevation: 24,
    },
});