import * as React from 'react';
import { LogBox, View, Text, Image, StyleSheet, TouchableOpacity, ImageBackground, StatusBar} from 'react-native';
import { color } from 'react-native-elements/dist/helpers';
import { SafeAreaView } from 'react-native-safe-area-context';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import { useNavigation } from '@react-navigation/native';
import * as FAW from '@fortawesome/free-solid-svg-icons';
import { useFonts } from 'expo-font';
import { getValueFor,saveValue } from "../../../utils/localStorage";
import { useFocusEffect } from '@react-navigation/native';
import { url } from '../../../utils/config';
import Api from "../../../utils/Api";
import Constants from 'expo-constants';

const progreso = [
  {
    route: require('../../../../assets/logoHT_0.png'), 
    progress: 0,
    texto: "¡Vaya! Parece que aún no has visitado ningún negocio",
    estado: false
  },
  {
    route: require('../../../../assets/logoHT_1.png'), 
    progress: 1,
    texto: "¡Sigue con tu camino para completar la flor!",
    estado: false
  },
  {
    route: require('../../../../assets/logoHT_2.png'), 
    progress: 2,
    texto: "¡Sigue con tu camino para completar la flor!",
    estado: false
  },
  {
    route: require('../../../../assets/logoHT_3.png'), 
    texto: "¡Sigue con tu camino para completar la flor!",
    progress: 3,
    estado: false
  },
  {
    route: require('../../../../assets/logoHT_4.png'), 
    progress: 4,
    texto: "¡Sigue con tu camino para completar la flor!",
    estado: false
  },
  {
    route: require('../../../../assets/logoHT_5.png'), 
    progress: 5,
    texto: "¡Sigue con tu camino para completar la flor!",
    estado: false
  },
  {
    route: require('../../../../assets/logoHT_6.png'), 
    progress: 6,
    texto: "¡Sigue con tu camino para completar la flor!",
    estado: false
  },
  {
    route: require('../../../../assets/logoHT_7.png'), 
    progress: 7,
    texto: "¡Reclama tu premio!",
    estado: true
  }
]

export default function ProgressScreen() {

  const [progress, setProgress] = React.useState(0);

  const navegacion = useNavigation();

  useFocusEffect(
    React.useCallback(() => {
      if(Platform.OS == 'ios'){
          StatusBar.setBarStyle('dark-content');
      }

      const  checkLogin = async () =>{

        let id = await getValueFor('id');
        let token = await getValueFor('token');    

        if(!id && !token){
          // si no hay sesion iniciada
          navegacion.navigate("inicio");
        }
      }
      checkLogin();
      
      const traerProgreso = async () => {
        let p = await getValueFor('progreso');
        let idEvento = await getValueFor('idEvento');
        let idPersona = await getValueFor('id');
        let uri = "";
        let api = null;

        setProgress(JSON.parse(p))
        if(JSON.parse(p)== 7){
          uri = `${url}eventos/colocarPocision/${idEvento}/${idPersona}`;
          api = new Api(uri, "POST");
          api.call().then((res) => {
            if(res.response){
              console.log(res.message);
            }else{
              console.log(res.message);
            }
          });
        }
      }
      traerProgreso()
    }, [progress])
  );
 
  let ruta = progreso[progress];

  return (
    <View style={styles.container}>
        <TouchableOpacity style={styles.regresoC} onPress={()=> navegacion.navigate('mapEvents')}>
            <FontAwesomeIcon style={styles.regreso} size={28} icon={FAW.faArrowLeft}/>
        </TouchableOpacity>
        <View style={styles.cajaTitulo}>
            <Text style={styles.titulo} allowFontScaling={false}>PROGRESO DE RECORRIDO</Text>
        </View>

        <View style={styles.cajaLogoHT}>
            <ImageBackground style={styles.logoHT} source={ruta.route} resizeMode={'contain'}></ImageBackground>
        </View>

        <View style={styles.cajaTexto1}>
            <Text style={styles.texto1} allowFontScaling={false}><Text allowFontScaling={false}>{ruta.progress}</Text>/7</Text>
        </View>

        <View style={styles.cajaTexto2}>
              {!ruta.estado?(
                <Text style={styles.texto2} allowFontScaling={false}>{ruta.texto}</Text> 
                ):(
                  <TouchableOpacity onPress={() => navegacion.navigate("cheemsScreen")}>
                    <View style={styles.boton}>
                      <Text style={[styles.texto2, styles.texto3]}  allowFontScaling={false}>{ruta.texto}</Text>
                    </View>
                  </TouchableOpacity>
                )
              }
        </View> 
    </View>
  )};

  const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#fff',
        paddingTop: Platform.OS == 'ios' ? Constants.statusBarHeight : 0
      },
    regresoC: {
        width: '10%',
        position: 'relative',  
        justifyContent: 'center',
        alignSelf:'flex-start',
        left: 20
      },
    regreso: {
        fontSize: 50,
        color: "#000"
      },
    cajaTitulo:{
        width:'100%',
        height: '20%',
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: '2.5%',
        flexDirection: 'row'
    },
    titulo: {
      color: "#000",
      fontSize: 40,
      textAlign: "center",
      color: "#00cbd5",
      fontWeight: 'bold'
    },
    cajaLogoHT:{
      width: '50%',
      height: '35%',
      alignItems: "center",
      justifyContent: 'center'
    },
    logoHT:{
      width:'100%',
      height:'100%'
    },
    cajaTexto1:{
      width: '100%',
      height: '10%',
      alignItems: "center",
      justifyContent: 'center'
    },
    texto1:{
      color: "#000",
      fontSize: 35,
      fontWeight: 'bold'
    },
    cajaTexto2:{
      width: '100%',
      height: '25%',
      alignItems: "center",

    },
    texto2:{
      marginTop: 30,
      color: "#000",
      fontSize: 25,
      textAlign:"center" 
    },
    texto3:{
      marginTop: 0,
      color: "#fff",
    },
    boton:{
      width:"90%",
      height: "70%",
      backgroundColor: "#00cbd5",
      borderRadius: 30,
      paddingHorizontal: 20,
      justifyContent: 'center'
    },
})