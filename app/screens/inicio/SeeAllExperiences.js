import React, { useState, useCallback, useEffect } from "react";
import { Dimensions, FlatList, SafeAreaView, StyleSheet, Text, TouchableOpacity, View, StatusBar, Platform, RefreshControl } from "react-native";
import { Image, } from "react-native-elements";
import { Ionicons } from "@expo/vector-icons";
import { useNavigation, useFocusEffect } from "@react-navigation/native";
import { useFonts } from "expo-font";
import { size } from "lodash";
import Constants from 'expo-constants';
import Api from "../../utils/Api";
import { url } from "../../utils/config";
import LoaderLineal from "../../components/LoaderLineal";

const Item = ({ urlImg, nombre, onPress }) => (
  <View style={styles.items}>
    <TouchableOpacity onPress={onPress} style={styles.touchable} allowFontScaling={false} activeOpacity={1}>
      {
        !urlImg ?
          <View style={styles.itemStyles}>
            <Image source={require('../../../assets/notUrl.png')}
              style={styles.logo}
              resizeMode={"contain"}
            />
          </View>
          : (
            <View style={styles.itemStyles}>
              <Image
                source={{ uri: urlImg }}
                style={styles.logo}
                resizeMode={"contain"}
              />
            </View>
          )
      }
      <View style={{ height: "100%", width: "100%" }}>
        <Text style={styles.title} allowFontScaling={false}>
          {nombre}
        </Text>
      </View>
    </TouchableOpacity>
  </View>
)

const formatData = (lista, numColumns) => {
  const total = Math.floor(lista.length / numColumns);
  let totalLastRow = lista.length - (total * numColumns);
  while (totalLastRow !== numColumns && totalLastRow !== 0) {
    lista.push({ key: `blank-${totalLastRow}`, empty: true });
    totalLastRow++;
  }
  return lista;
};

const numColumns = 2;

export default function SeeAllExperiences() {
  const navegacion = useNavigation();
  const [lista, setLista] = useState([]);
  const [error, setError] = useState(null);

  const [refreshing, setRefreshing] = useState(false);

  const getExp = async () => {

    let uri = `${url}establishment/listexperiencies`;
    let api = new Api(uri, "GET");
    api.call().then((res) => {
      if (res.response) {
        setLista(res.result);
      } else {
        setError(res.errors);
      }
    });
  }

  useEffect(() => {
      if(Platform.OS == 'ios'){
        StatusBar.setBarStyle('dark-content');
      }

      getExp();
    }, []
  )


  const onRefresh = () => {
    // Lógica de recarga aquí
    getExp();

    // Simulamos un tiempo de espera de 2 segundos antes de finalizar la recarga
    setTimeout(() => {
      setRefreshing(false);
    }, 2000);
  };


  renderItem = ({ item, index }) => {
    if (item.empty == true) {
      return <View style={[styles.items, styles.itemInvisible]} />;
    }
    return (
      <Item
        urlImg={item.urlImgPerfil}
        nombre={item.nombre}
        onPress={() =>
          navegacion.navigate("experiencesInformation", { id: item.id })
        }
      />
    );
  };

  const [loaded] = useFonts({
    "Montserrat-Bold": require("../../../assets/fuentes/Montserrat-Bold.ttf"),
    "Montserrat-Italic": require("../../../assets/fuentes/Montserrat-Italic.ttf"),
    "Montserrat-Light": require("../../../assets/fuentes/Montserrat-Light.ttf"),
    "Montserrat-Medium": require("../../../assets/fuentes/Montserrat-Medium.ttf"),
    "Montserrat-Regular": require("../../../assets/fuentes/Montserrat-Regular.ttf"),
    "Montserrat-SemiBold": require("../../../assets/fuentes/Montserrat-SemiBold.ttf"),
  });

  if (!loaded) {
    return null;
  }

  return (
    <View style={{ flex: 1, backgroundColor: '#fff', paddingTop: Platform.OS == 'ios' ? Constants.statusBarHeight : 0}}>
    {/* <StatusBar backgroundColor={'#fff'} barStyle={'dark-content'}/> */}
      <View style={styles.estilo1}>
        <TouchableOpacity onPress={() => navegacion.goBack()}>
          <View style={{ flexDirection: 'row', alignItems:'center', height:'100%' }}>
            <Ionicons
              allowFontScaling={false}
              name="arrow-back-outline"
              size={30}
              style={{ color: '#555454', marginStart: '3%', paddingRight:'5%' }}
            />
            <Text style={styles.letra} allowFontScaling={false}>
              Experiencias
            </Text>
          </View>
        </TouchableOpacity>
      </View>
      <View>
        {
          !lista ? (
            <View style={styles.nullStyles} >
              <Text style={{ color: "grey", fontFamily: "Montserrat-Bold" }} allowFontScaling={false} >
                No hay establecimientos.
              </Text>
            </View>
          ) : size(lista) > 0 ? (
            <FlatList
              numColumns={numColumns}
              horizontal={false}
              data={formatData(lista, numColumns)}
              // data={lista}
              renderItem={renderItem}
              keyExtractor={item => item.id}
              overScrollMode='never'
              style={{ marginBottom: '21%', }}
              refreshControl={<RefreshControl refreshing={refreshing} onRefresh={onRefresh}/>}
            />
          ) : (
            <View style={styles.loaderStyles} >
              <LoaderLineal />
            </View>
          )}
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  items: {
    marginVertical: 10,
    marginHorizontal: 23,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
    flex: 1,
    paddingTop: '21%',
    height: 210
  },
  touchable: {
    width: "100%",
  },
  itemStyles: {
    backgroundColor: '#fff',
    borderRadius: 13,
    elevation: 5,
  },
  logo: {
    height: 150,
    width: "100%",
    borderRadius: 13,
  },
  title: {
    color: "black",
    fontSize: 16,
    fontFamily: "Montserrat-Bold",
    marginTop: '4%',
    textAlign: 'center',
  },
  itemInvisible: {
    backgroundColor: 'transparent'
  },
  estilo1: {
    backgroundColor: '#fff',
    width: '100%',
    height: '10%',
  },
  letra: {
    fontSize: 20,
    color: '#555454',
    fontFamily: 'Montserrat-Bold'
  },
  nullStyles: {
    width: "100%",
    height: "100%",
    alignItems: "center",
    justifyContent: "center",
  },
  loaderStyles: {
    width: "100%",
    height: "100%",
    alignItems: "center",
    justifyContent: "center",
  },
});
