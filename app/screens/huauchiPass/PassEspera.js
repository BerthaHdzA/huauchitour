import React, { useState, useCallback } from 'react';
import { useFocusEffect } from '@react-navigation/native';

import HuauchiPass from '../huauchiPass/HuauchiPass';
import HuauchiPassCreado from './HuauchiPassCreado';

import { getValueFor } from '../../utils/localStorage';
import Loader from '../../components/Loader';
import { VerifyQR } from '../../utils/VerifyQR';

const PassEspera = () => {
  const [login, setLogin] = useState(null);
  const [loading, setloading] = useState(true);
  useFocusEffect(
    useCallback(() => {
      async function loginVerification(){
        setloading(true)
        await getValueFor("token").then( async (token) => {
          if(token === false){
            setLogin(false) 
          }else{
            let qr = await VerifyQR();
            if(qr){
              setLogin(true);
            }
          }
        })
        setloading(false)
      }
      loginVerification();
    }, [login]
    )
  )
    return (
        (loading) ? (
          <Loader isVisible={true} text={"Cargando..."} />
        ): (
          login ? <HuauchiPassCreado setLogin={setLogin} /> : <HuauchiPass />
        )
    )
  // if (loading) return <Loader isVisible={true} text={"Cargando..."} />
  // return login ? <Cuenta setLogin={setLogin} /> : <Invitado />
}
export default PassEspera
