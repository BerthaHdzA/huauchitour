import React from 'react';
import { SafeAreaView, StyleSheet, Text, View, TouchableOpacity, StatusBar, ImageBackground, Dimensions} from 'react-native';
import { Image} from 'react-native-elements';
import { useNavigation, useFocusEffect } from '@react-navigation/native';
import {useFonts}from 'expo-font';
import { VerifyQR } from '../../utils/VerifyQR';
import { getValueFor } from '../../utils/localStorage';

const HuauchiPass = () => {
  const navigation = useNavigation();

    //const [loaded]=useFonts({
  //  'Helvetica-Bold': require('../../../assets/Fonts/Helvetica-Bold.ttf'),
  // 'Helvetica': require('../../../assets/Fonts/Helvetica.ttf'),
  //});
  useFocusEffect(
      React.useCallback(()=>{
          if(Platform.OS == 'ios'){
            StatusBar.setBarStyle('dark-content');
          }
          
          const checkLogin = async() =>{

            let id = await getValueFor('id');
            let token = await getValueFor('token');  
      
            if(id && token){
              let qr = await VerifyQR();
              if(qr){
                navigation.navigate('PassEspera');
              }
            }
          }
          checkLogin();
      }, [])
  );

  const [loaded]=useFonts({
    "Montserrat-Bold": require("../../../assets/fuentes/Montserrat-Bold.ttf"),
    "Montserrat-Italic": require("../../../assets/fuentes/Montserrat-Italic.ttf"),
    "Montserrat-Light": require("../../../assets/fuentes/Montserrat-Light.ttf"),
    "Montserrat-Medium": require("../../../assets/fuentes/Montserrat-Medium.ttf"),
    "Montserrat-Regular": require("../../../assets/fuentes/Montserrat-Regular.ttf"),
    "Montserrat-SemiBold": require("../../../assets/fuentes/Montserrat-SemiBold.ttf"),

  });

  if (!loaded){
    return null;
  }


  return (
    <View style={styles.container}>
      <View style={styles.card}>

        <View>
          <Text style={{ fontSize: 45, color: 'white', fontFamily:'Montserrat-Bold', }} allowFontScaling={false}>
            Consigue {"\n"}tu Huauchi{"\n"}Pass
          </Text>
          <Text style={{ fontSize: 22, color: 'white', fontFamily:'Montserrat-Medium', paddingTop: '9%', }} allowFontScaling={false}>
            Acceso promociones{"\n"}y descuentos en tus{"\n"}negocios favoritos.
          </Text>
        </View>

        <View style={{ flexDirection: 'row', paddingTop: '4%' }}>
          <View style={{ paddingTop: '65%' }}>
            <TouchableOpacity
              onPress={() => navigation.navigate("crearhuauchiPass")}>
              <Text style={{ fontSize: 30, color: 'white', fontFamily:'Montserrat-Medium', textDecorationLine: 'underline',fontWeight: 'bold'}} allowFontScaling={false}>
                Obtener
              </Text>
            </TouchableOpacity>
          </View>
          <View style={{ paddingLeft: '5%', paddingTop: '3%', width: '65%', backgroundColor: '#ffffff00'}}>
            <ImageBackground
              source={require("../../../assets/huauchi_pass_2.png")}
              style={styles.backgroundI}
            ></ImageBackground>
          </View>
        </View>

      </View>

    </View>
  );
};

export default HuauchiPass;

const styles = StyleSheet.create({
  container: {
    padding: 10,
    flex: 1,
    width: '100%',
    height: Dimensions.get('window').height,
    backgroundColor: '#fff',
    display: 'flex',
    justifyContent: 'center'
  },
  card: {
    height: 'auto',
    width: '100%',
    backgroundColor: "#30a1fa",
    padding: '8%',
    borderRadius: 40,
  },
  backgroundI: {
    width: '90%',
    height: 210,
    resizeMode: 'contain',
    transform: 'rotateY(-30deg) rotateZ(5deg) rotateX(20deg)',
    marginLeft: '10%'
  }
})


