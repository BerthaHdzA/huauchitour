import React from 'react';
import { View, Text } from 'react-native';
import { Image } from 'react-native-elements';


const CardHuauchiPass = () => {
    return (
        <View style={{ width: '100%', height: '100%', paddingTop: '10%', paddingBottom: '10%', paddingLeft: '3%', paddingRight: '3%' }}>
            <Image
                source={require("../../../assets/huauchi_pass_2.png")}
                style={{ width: '100%', height: '100%', resizeMode: 'center' }}
            />
        </View>
    )
}

export default CardHuauchiPass