import React, { useState, useCallback, useRef } from 'react';
import { Dimensions, Pressable, SafeAreaView, StyleSheet, View, StatusBar, Text, Platform, TouchableOpacity, ImageBackground } from 'react-native';
import { Image } from 'react-native-elements';
import { useFocusEffect, useNavigation} from '@react-navigation/native'
import QRCode from 'react-native-qrcode-svg';

import { getValueFor} from '../../utils/localStorage';
import { url } from '../../utils/config';
import Api from '../../utils/Api';
import Constants from 'expo-constants';
import { useFonts } from 'expo-font';

let codigo;
export default HuauchiPassCreado = () => {
  const navegacion = useNavigation();
  const [show, setShow] = useState(true);
  const color = useRef("000");
  const [dataMem, setDataMem] = useState({
    status: 'inactivo',
    text: "Cargando..."
  });

  useFocusEffect(
    useCallback( () => {
      if(Platform.OS == 'ios'){
        StatusBar.setBarStyle('dark-content');
      }
      
      // crea el qr del usuario
      async function crearQr(){

        let id = await getValueFor('id');
        let token = await getValueFor('token');

        let uri = `${url}membership/buscarQr/${id}`
        let api = new Api(uri, "GET", null, token);
        api.call().then(res => {
          if (res.response) {
            codigo = res.result;
          } else {
            navegacion.navigate("huauchiPass");
          }
        })
      }

      crearQr();

      // Verifica el estado de la membresia
      const obtenerInfo = async () => {

        let id = await getValueFor('id');
        let token = await getValueFor('token');

        let uri = `${url}scan/validateMembership/${id}`
        let api = new Api(uri, "GET", null, token);
        api.call().then(res => {
          if(res.response || res.errors == "qr no valido"){
            if(!res.errors){
              color.current = '16963a';
              setDataMem({
                status: 'activo',
                text: "Tu HuauchiPass está activo para promociones y descuentos."
              })
            }else{
              color.current = 'e93f2c'
              setDataMem({
                status: 'inactivo',
                text: "Tu HuauchiPass ha expirado, renueva tu membresía para recuperar tus beneficios."
              })
            }
          }else{
            if(!res.errors){
              color.current = 'e93f2c'
              setDataMem({
                status: 'inactivo',
                text: "No pudimos determinar el estado de tu HuauchiPass, por favor inténtalo de nuevo más tarde."
              })
            }else{
              color.current = 'e93f2c'
              setDataMem({
                status: 'inactivo',
                text: "Tu HuauchiPass no esta activo, renueva tu membresia para recuperar tus beneficios."
              })
            }
          }
        })
      }

      obtenerInfo();
    }, [codigo]
    )
  )

  const [loaded] = useFonts({
    "Montserrat-Bold": require("../../../assets/fuentes/Montserrat-Bold.ttf"),
    "Montserrat-Italic": require("../../../assets/fuentes/Montserrat-Italic.ttf"),
    "Montserrat-Light": require("../../../assets/fuentes/Montserrat-Light.ttf"),
    "Montserrat-Medium": require("../../../assets/fuentes/Montserrat-Medium.ttf"),
    "Montserrat-Regular": require("../../../assets/fuentes/Montserrat-Regular.ttf"),
    "Montserrat-SemiBold": require("../../../assets/fuentes/Montserrat-SemiBold.ttf"),
  });

  if (!loaded) {
    return null;
  }

  return (
    <View style={styles.container} >
      <View style={[styles.statusMembership, { overflow: 'hidden', justifyContent: 'center', alignItems: 'center'}]}>
        <Text allowFontScaling={false} style={{textAlign: 'center', width: '90%', fontSize: 18, fontFamily: 'Montserrat-SemiBold', color: `#${color.current}`}}>{dataMem.text}</Text>
      </View>
      <TouchableOpacity 
        style={{ marginTop: '5%', height: '75%'}} 
        onPress={() =>{ 
          setShow(!show);
        }}
        activeOpacity={0} 
      >
        <ImageBackground
          resizeMode='contain'
          source={require("../../../assets/huauchi_pass_back.png")}
          style={[styles.imageStyle, {display: !show ? 'flex' : 'none'}]}
        >
          <View style={{ justifyContent:'center', alignItems:'center', marginTop: '32%', height: "45%", width: Dimensions.get('window').height, alignSelf: 'center', }} >
            <QRCode
              value={codigo}
              size={200}
              quietZone={10}
              backgroundColor='transparent'
            />
          </View>
        </ImageBackground>
        <ImageBackground
          resizeMode='contain'
          source={require("../../../assets/huauchi_pass_2.png")}
          style={[styles.imageStyle, {display: show ? 'flex' : 'none'}]}
        >
        </ImageBackground>
      </TouchableOpacity>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    paddingTop: '5%',
    paddingBottom: '5%',
    paddingLeft: '4%',
    paddingRight: '4%',
    paddingTop: Platform.OS == 'ios' ? Constants.statusBarHeight : 0,
    backgroundColor: "#f2f2f2",
    height: '100%'
  },
  imageStyle: {
    width: '100%',
    height: '100%',
    // borderWidth: 1,
    // borderBottomWidth: 5,
    // borderRightWidth: 5,
    // borderRadius: 20,
    // borderColor: '#000'
  }, 
  statusMembership: {
    height: 100,
    width: '99%',
    alignSelf: 'center',
    marginTop: Constants.statusBarHeight / 2,
  }
})