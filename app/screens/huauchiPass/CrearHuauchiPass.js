import React, { useState } from 'react';
import { SafeAreaView, StyleSheet, Text, View, Alert, ScrollView, StatusBar, Platform, TouchableOpacity, ImageBackground } from 'react-native';
import { Image, Button } from 'react-native-elements';
import { useFocusEffect, useNavigation} from '@react-navigation/native';
import { useFonts } from 'expo-font';
import { Ionicons } from '@expo/vector-icons';
import Constants from 'expo-constants';
import { getValueFor } from '../../utils/localStorage';
import Api from '../../utils/Api';
import { url } from '../../utils/config';
import Loader from '../../components/Loader';
import { VerifyQR } from '../../utils/VerifyQR';

const CrearHuauchiPass = () => {
  const navegacion = useNavigation();
  const [showLoader, setShowLoader] = useState(false)
  const [progress, setProgress] = React.useState(0);

  useFocusEffect(
  React.useCallback(() => {
    if(Platform.OS == 'ios'){
      StatusBar.setBarStyle('light-content');
    }
    
    const checkLogin = async() =>{

      let id = await getValueFor('id');
      let token = await getValueFor('token');  

      if(id && token){
        let qr = await VerifyQR();
        if(qr){
          navegacion.navigate('PassEspera')
        }
      }
    }
    checkLogin();
  },[progress])
  );

  //funcion crer qr
  const crearQr = async () => {
    let id = await getValueFor('id');
    let token = await getValueFor('token');
    console.log(id, "id-------------")
    console.log(token, "token-------------")

    if (!id && !token) {
      setShowLoader(true);
      Alert.alert(
        "Error",
        "Inicie sesión para poder obtener su Huauchi Pass.",
        [
          {
            text: "Cancelar",
            onPress: () => console.log("Cancel Pressed", setShowLoader(false)),
            style: "cancel"
          },
          {
            text: "Iniciar",
            onPress: () => navegacion.navigate("cuentaStack")
          }
        ]
      );

    } else {
      const uri = `${url}membership/validatePass/${id}`
      const api = new Api(uri, "GET", null, token);
      api.call()
        .then(res => {
          console.log(res)
          res.response && navegacion.navigate("huauchiPassCreado")
          setShowLoader(false);
          // !res.response && 
        })
    }
    setShowLoader(true);

  }
  const createTwoButtonAlert = () =>
    Alert.alert(
      "Confirmación",
      `Al dar click al botón Crear, generaremos tu Huauchi Pass digital. \n ¿Quieres que generemos tu pass?`,
      [
        {
          text: "Cancelar",
          onPress: () => console.log("Cancel Pressed"),
          style: "cancel"
        },
        {
          text: "Crear",
          // onPress: () => console.log("OK Pressed")
          onPress: () => crearQr()
        }
      ]
    );

  const [tituloBoton, setTituloBoton] = useState("Continuar")
  var texto = "El precio de la membresía es de $29 por 28 días, puedes renovar las veces que tu quieras. Para poder hacer válido tu Huauchi Pass en los establecimientos y tener acceso a las promociones y descuentos, es necesario que tengas pagado el costo de la memebresía del mes en que lo quieres usar. Puedes usar las veces que quieras el Huauchi Pass dentro del mes que tengas activa tu membresía.";
  const [textVisible, setTextVisible] = useState(true)
  const SiguientePaso = () => {
    setTextVisible(false)
    setTituloBoton("Crear mi Huauchi Pass")
    if (tituloBoton == "Crear mi Huauchi Pass") {
      createTwoButtonAlert()
    }
  }
  const ChangeState = () => {
    textVisible == true ? (
      navegacion.goBack()
    ) : (
      setTituloBoton("Continuar"),
      setTextVisible(true)
    )
  }
  console.log(textVisible, "este su estado")

  const [loaded] = useFonts({
    "Montserrat-Bold": require("../../../assets/fuentes/Montserrat-Bold.ttf"),
    "Montserrat-Italic": require("../../../assets/fuentes/Montserrat-Italic.ttf"),
    "Montserrat-Light": require("../../../assets/fuentes/Montserrat-Light.ttf"),
    "Montserrat-Medium": require("../../../assets/fuentes/Montserrat-Medium.ttf"),
    "Montserrat-Regular": require("../../../assets/fuentes/Montserrat-Regular.ttf"),
    "Montserrat-SemiBold": require("../../../assets/fuentes/Montserrat-SemiBold.ttf"),
  });
  if (!loaded) {
    return null;
  }

  // if(showLoader) return 
  <Loader isVisible={true} text={"Cargando..."} />

  return (
    <View style={{ flex: 1, backgroundColor: '#fff', height: '100%', width: '100%'}}>
      <View >
        <ImageBackground
          source={require("../../../assets/MembresiaFrentePreview.jpg")}
          style={{ width: '100%', height: 300, resizeMode: "contain", paddingTop: Platform.OS == 'ios' ? Constants.statusBarHeight : 0, overflow: 'hidden'}}>
          {
            textVisible == true ? (
              <TouchableOpacity onPress={() => ChangeState()}>
                <Ionicons
                  name="arrow-back-circle"
                  size={43}
                  style={{ color: 'white', marginStart: '3%' }}
                />
              </TouchableOpacity>) : (
              <TouchableOpacity onPress={() => ChangeState()}>
                <Ionicons
                  name="arrow-back-circle"
                  size={43}
                  style={{ color: 'white', marginStart: '3%' }}
                />
              </TouchableOpacity>
            )
          }
          <View style={{ paddingTop: '5%' }}>
            <Image
              source={require("../../../assets/huauchi_pass_2.png")}
              style={{ width: '100%', height: '100%', resizeMode: "contain", marginTop: '1%' }}
            />
          </View>
        </ImageBackground>
      </View>
      <ScrollView overScrollMode='never'>
        <View style={{ paddingLeft: 22, paddingRight: 22 }}>
          <View style={{ paddingTop: '5%', paddingBottom: '3%' }}>
            <Text style={styles.textHuauchiPass} allowFontScaling={false}>
              Huauchi Pass
            </Text>
          </View>
          <View style={{ paddingBottom: '5%' }}>
            <Text style={styles.textMembresia} allowFontScaling={false}>
              El Huauchi Pass es una membresía con la que obtienes descuentos y promociones únicos en
              tus negocios favoritos, ya sea un café, restaurantes, estéticas para tu mascota o lo que quieras.
            </Text>
          </View>
          <View style={{ paddingTop: '3%', paddingBottom: '3%' }}>
            {
              textVisible == true ? (
                <Text style={styles.textGray} allowFontScaling={false}> {texto} </Text>
              ) : (null)
            }
          </View>
        </View>

        <View style={{ paddingTop: '3%', justifyContent: "center", alignContent: "center" }}>
          <Button
            onPress={() => SiguientePaso()}
            title={tituloBoton}
            titleStyle={{ fontSize: 21, letterSpacing: -0.5750000000000001, fontFamily: 'Montserrat-Medium',}}
            buttonStyle={styles.buttonContinuar}
            containerStyle={styles.containerContinuar}
          />
        </View>
      </ScrollView>
    </View>
  );
};

export default CrearHuauchiPass;


const styles = StyleSheet.create({
  textHuauchiPass: {
    color: 'black',
    fontSize: 30,
    textAlign: 'center',
    fontFamily: 'Montserrat-Bold'
  },
  textMembresia: {
    color: 'black',
    fontSize: 15,
    textAlign: 'center',
    fontFamily: 'Montserrat-Medium',
    lineHeight: 20,
  },
  textGray: {
    color: 'gray',
    fontSize: 12,
    textAlign: 'center',
    fontFamily: 'Montserrat-Light',
    lineHeight: 15
  },
  buttonContinuar: {
    width: "95%",
    height: 55,
    
    borderRadius: 6,
    backgroundColor: "#30a1fa",
    
  
  },
  containerContinuar: {
    paddingLeft: '5%'
   
   
    
  },
})
