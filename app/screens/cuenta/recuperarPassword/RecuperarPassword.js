import * as React from 'react';
import { SafeAreaView, ScrollView, StyleSheet, Text, View, StatusBar, Alert, Platform} from 'react-native';
import { Button, Input } from 'react-native-elements';
import { useNavigation, useFocusEffect} from '@react-navigation/native';
import {useFonts}from 'expo-font';
import Api from '../../../utils/Api';
import { url } from '../../../utils/config';
import Constants from 'expo-constants';

import ReturnToLogin from '../../../components/ReturnToLogin';
import Loader from '../../../components/Loader';

import { getValueFor, deleteValue } from '../../../utils/localStorage';

export default function RecuperarPassword() {
  const navegacion = useNavigation();
  const [isVisible, setIsVisible] = React.useState(false);
  const [data, setData] = React.useState({
      user:""
  });
  const [errors, setErrors] = React.useState({
    state: false,
    message: ""
  })


  useFocusEffect(
      React.useCallback(()=>{
        if(Platform.OS == 'ios'){
          StatusBar.setBarStyle('dark-content');
        }

        // Saber si hay un codigo solicitado recientemente
        const getval =  async () => {
          let soli = await getValueFor("soli");
          let fecha = await getValueFor("fechaSoli");
          fecha = JSON.parse(fecha);
          fecha = new Date(fecha);
          let fechaA = new Date();
          if(soli && fecha){
            fecha.setDate(fecha.getDate() + 1)
            if(fechaA < fecha){
              Alert.alert(
                "Atención",
                "Recientemente solicitaste un código para cambiar tu contraseña.\n¿Deseas usar ese código?\nDe lo contrario, puedes solicitar uno nuevo ingresando tu correo nuevamente.",
                [
                  {
                    text: "Solicitar uno nuevo",
                    onPress: async () => {
                      await deleteValue("soli");
                      await deleteValue("fechaSoli");
                    }
                  },
                  {
                    text: "Usar mi código",
                    onPress: async () => {
                      navegacion.navigate("CodigoRecuperacion", {"user": soli})
                    }
                  }
                ]
              );
            }
          }
        }
        getval();
      }, [])
  );

  const saveValue = (name,value) => {
      setData({...data,[name]:value})
  }

  // Enviar codigo de verificacion
  const sendCode = async () => {
    let valores = data.user;
    let valoresAceptados = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if(!valores.match(valoresAceptados)){
      setErrors({state:true, message: "Ingresa una dirección de correo válido."});
    } else {
      let fechaB = await getValueFor("fechaBloq");
      const send = async () => {
        setIsVisible(true);
        let uri = "";
        let api = null;
    
        let params = {
          correo: data.user,
          tipoPersona: 1
        }
    
        uri = `${url}auth/sendCodeEmail`;
        api = new Api(uri,'POST', params);
        await api.call().then(res => {
          console.log(res);
          if (res.response) {
            setIsVisible(false)
            navegacion.navigate("CodigoRecuperacion", data); 
            setErrors({state:false, message: ""})
          } else {
            setIsVisible(false)
            setErrors({state:true, message: res.errors})
          }
        });
      }
      if(fechaB){
        let fecha = new Date();
        fechaB = JSON.parse(fechaB);
        fechaB = new Date(fechaB);
        if(fecha > fechaB){
          send();
        }else{
          alert("Has exedido el numero de intentos permitidos para enviar correos, espera 10 minutos para poder enviar correo de recuperación nuevamente."); 
        }
      }else{
        send();
      }
    }
  }

  const [loaded]=useFonts({
    "Montserrat-Bold": require("../../../../assets/fuentes/Montserrat-Bold.ttf"),
    "Montserrat-Italic": require("../../../../assets/fuentes/Montserrat-Italic.ttf"),
    "Montserrat-Light": require("../../../../assets/fuentes/Montserrat-Light.ttf"),
    "Montserrat-Medium": require("../../../../assets/fuentes/Montserrat-Medium.ttf"),
    "Montserrat-Regular": require("../../../../assets/fuentes/Montserrat-Regular.ttf"),
    "Montserrat-SemiBold": require("../../../../assets/fuentes/Montserrat-SemiBold.ttf"),
  });

  if (!loaded){
    return null;
  }

  return (
    <View style={{ backgroundColor: "#fff", height: "100%", width: '100%', paddingTop: Platform.OS == 'ios' ? Constants.statusBarHeight : 0}}>
      <ReturnToLogin />
      <ScrollView overScrollMode='never' keyboardShouldPersistTaps="handled" style={{ backgroundColor: "#fff", height: "100%", width: '100%'}}>
        <View style={{ margin: 20 }}>
          <Text style={{ fontSize: 27, textAlign: 'left', fontFamily:'Montserrat-Medium' }} allowFontScaling={false}>
            Recuperar contraseña.</Text>
          <Text style={{ fontSize:17 ,fontFamily:'Montserrat-Regular', color: 'gray',}} allowFontScaling={false}>
            Por favor ingresa tu correo con el que te registraste en la app.{"\n"}
          </Text>
        </View>

        <View style={{ justifyContent: 'center', alignItems: 'center' }}>
          <Input
            allowFontScaling={false}
            placeholder='Correo electrónico '
            style={styles.inputStyle}
            selectionColor={'#3D5CA4'}
            containerStyle={styles.inputContainer}
            inputContainerStyle={{ borderBottomWidth: 0 }}
            onChangeText={(e) => saveValue("user", e)}
            maxLength={50}
            errorMessage={errors.message}
          />
          {/* {errors.state ? 
            <View style={{height: 50, width: "100%"}}>
              <Text style={{alignSelf: 'flex-start', marginHorizontal: 20, color: 'red'}} allowFontScaling={false}>{errors.message}</Text>
            </View>
          :
            <View style={{height: 50}}></View>
          } */}

        </View>
        <View style={{ justifyContent: 'center', alignContent: 'center', paddingTop: '10%', marginBottom: 50 }}>
          <Button
            containerStyle={styles.containerBtn}
            buttonStyle={styles.btnStyle}
            title="Confirmar"
            titleStyle={{ fontSize: 22, letterSpacing: -0.5750000000000001, fontFamily:'Montserrat-Medium'}}
            onPress={() => {
              if(data.user != ""){ 
                sendCode();
              } else { 
                setErrors({state:true, message: "Por favor ingresa un correo"})
              }}}
          />
        </View>
        <Loader
          isVisible={isVisible}
          text={"Validando Correo..."}
        />
      </ScrollView>
    </View>
  )
}

const styles = StyleSheet.create({
  containerBtn: {
    paddingLeft: '5%',
  },
  btnStyle: {
    width: "95%",
    height: 55,
    borderRadius: 6,
    backgroundColor: "#3498DB",
  },
  inputStyle: {
    textAlign: "center",
    fontSize: 18,
    marginBottom: 5
  },
  inputContainer: {
    width: "89%",
    height: 49,
    borderWidth: 1,
    borderColor: "rgba(151,151,151,1)",
    marginBottom: 20,
    borderRadius: 6,
  },
})