import * as React from 'react';
import { SafeAreaView, StyleSheet, Text, View, ScrollView, StatusBar, Platform } from 'react-native';
import { Button, Input, Icon } from 'react-native-elements';
import { useNavigation, useFocusEffect} from '@react-navigation/native';
import {useFonts}from 'expo-font';
import Api from '../../../utils/Api';
import { url } from '../../../utils/config';
import Loader from '../../../components/Loader';
import ReturnRecuperar from '../../../components/ReturnRecuperar';
import Constants from 'expo-constants';

export default function EscribirPass(props) {
  const navegacion = useNavigation();
  const [isVisible, setIsVisible] = React.useState(false);
  const[verPassword, setVerPassword] = React.useState(false);

  const [data, setData] = React.useState({
    pass: ""
  });

  const [errors, setErrors] = React.useState({
    state: false,
    message: ""
  })
  const saveValue = (name,value) => {
      setData({...data,[name]:value})
  }

  useFocusEffect(
      React.useCallback(()=>{
          if(Platform.OS == 'ios'){
            StatusBar.setBarStyle('dark-content');
          }
      }, [])
  );

  const savePass = async () => {
    setIsVisible(true);
    if(data.pass.length <= 5){
        setErrors({state:true, message: "Ingresa una contraseña de mínimo 6 caracteres"})
        setInterval(() => {
            setIsVisible(false);
        }, 1000);
    }
    let uri = "";
    let api = null;

    let params = {
      password: data.pass,
    }

    uri = `${url}user/updatePassword/${props.route.params.user}`;
    api = new Api(uri,'PUT', params, props.route.params.token);
    api.call().then(res => {
      if (res.response) {
        setIsVisible(false)
        navegacion.navigate("login")
        setErrors({state:false, message: ""})
      } else {
        setIsVisible(false)
        setErrors({state:true, message: res.errors})
      }
    });
  }

  const [loaded]=useFonts({
    'Helvetica-Bold': require('../../../../assets/Fonts/Helvetica-Bold.ttf'),
    'Helvetica': require('../../../../assets/Fonts/Helvetica.ttf'),

  });

  if (!loaded){
    return null;
  }

  return (
    <View style={{ backgroundColor: "#fff", height: "100%", width: '100%', paddingTop: Platform.OS == 'ios' ? Constants.statusBarHeight : 0}}>
      <ReturnRecuperar/>
      <ScrollView overScrollMode='never' keyboardShouldPersistTaps='handled' style={{ backgroundColor: "#fff", height: "100%", width: '100%'}}>
        <View style={{ margin: 20, }}>
          <Text style={{ fontSize: 27, textAlign: 'left', fontFamily:'Helvetica' }} allowFontScaling={false}>
            Cambio de contraseña.</Text>
          <Text style={{fontSize:17 ,fontFamily:'Helvetica', color: 'gray',}} allowFontScaling={false}>
            Por favor ingresa tu nueva contraseña, recuerda que una vez realizada esta acción no podrá revertirse.{"\n"}
            {"\n"}Una vez tu contraseña haya sido cambiada seras redirigido automáticamente a la pantalla de inicio de sesión para que puedas iniciar sesión con tu nueva contraseña.
          </Text>


        </View>

        <View style={{ justifyContent: 'center', alignItems: 'center' }}>
          <Input
            allowFontScaling={false}
            placeholder='Contraseña'
            style={styles.inputStyle}
            selectionColor={'#3D5CA4'}
            containerStyle={styles.inputContainer}
            inputContainerStyle={{ borderBottomWidth: 0 }}
            onChangeText={(e) => saveValue("pass", e)}
            maxLength={50}
            password={true}
            secureTextEntry={verPassword ? false : true}
            keyboardType="default"
            errorMessage={errors.message}
            rightIcon={
                <Icon
                    type="font-awesome" 
                    name = {verPassword ? "eye-slash" : "eye"}
                    size={20}
                    iconStyle = {{color:"gray"}}
                    onPress={()=>setVerPassword(!verPassword)}
                />
            }
          />
        </View>

        <View style={{ justifyContent: 'center', alignContent: 'center', paddingTop: '7%', marginBottom: 50 }}>
          <Button
            containerStyle={styles.containerBtn}
            buttonStyle={styles.btnStyle}
            title="Confirmar"
            titleStyle={{ fontSize: 21, letterSpacing: -0.5750000000000001, fontFamily:'Montserrat-Medium' }}
            onPress={() => {
              if(data.pass != ""){ 
                savePass();
              } else { 
                setErrors({state:true, message: "Por favor, ingresa una contraseña"})
              }}}
          />
        </View>
        <Loader
          isVisible={isVisible}
          text={"Guardando contraseña..."}
        />
      </ScrollView>
    </View>
  )
}

const styles = StyleSheet.create({
  containerBtn: {
    paddingLeft: '5%'
  },
  btnStyle: {
    width: "95%",
    height: 55,
    borderRadius: 6,
    backgroundColor: "#3498DB",
   
  },

  inputStyle: {
    textAlign: "center",
    fontSize: 18,
    marginBottom: 5
  },
  inputContainer: {
    width: "89%",
    height: 49,
    borderWidth: 1,
    borderColor: "rgba(151,151,151,1)",
    marginBottom: 30,
    borderRadius: 6,
  },
})