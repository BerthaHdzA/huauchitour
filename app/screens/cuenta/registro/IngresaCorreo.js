import React, { useState, useCallback, useEffect } from 'react';
import { SafeAreaView, StyleSheet, Text, View, ScrollView, StatusBar, Platform, Alert} from 'react-native';
import { Button, Input, } from 'react-native-elements';
import { useNavigation, useFocusEffect, } from '@react-navigation/native';
import { useFonts } from 'expo-font';
import Constants from 'expo-constants';

import ReturnHeader from '../../../components/ReturnHeader'

import { saveValue, getValueFor, deleteValue } from '../../../utils/localStorage';
import Loader from '../../../components/Loader';
import Api from '../../../utils/Api';
import { url } from '../../../utils/config';

export default function IngresaCorreo() {
  const navegacion = useNavigation();
  const [isVisible, setIsVisible] = useState(false);
  const [errors, setErrors] = useState({});
  const [temcorreo, setTemcorreo] = useState(null);

  const [parametros, setParametros] = useState({
    email: null,
    tipoPersona: "1"
  });

  useFocusEffect(
      useCallback(() => {
          if(Platform.OS == 'ios'){
            StatusBar.setBarStyle('dark-content');
          }

          // Saber si hay un codigo solicitado recientemente
          const getval =  async () => {
            let soli = await getValueFor("soli2");
            let fecha = await getValueFor("fechaSoli2");
            fecha = JSON.parse(fecha);
            fecha = new Date(fecha);
            let fechaA = new Date();
            if(soli && fecha){
              fecha.setDate(fecha.getDate() + 1)
              if(fechaA < fecha){
                Alert.alert(
                  "Atención",
                  "Recientemente solicitaste un código para crear una cuenta.\n¿Deseas usar ese código?\nDe lo contrario, puedes solicitar uno nuevo ingresando tu correo nuevamente.",
                  [
                    {
                      text: "Solicitar uno nuevo",
                      onPress: async () => {
                        await deleteValue("soli2");
                        await deleteValue("fechaSoli2");
                      }
                    },
                    {
                      text: "Usar mi código",
                      onPress: async () => {
                        navegacion.navigate("validarCodigo", {"email": soli, "tipoPersona": 1})
                      }
                    }
                  ]
                );
              }
            }
          }
          getval();
      }, []
    )
  )

  const onchange = (e, tipo) => {
    setParametros({ ...parametros, [tipo]: e.nativeEvent.text })
  };

  // verifica que sea un correo valido
  const validarCorreo = async () => {
    let valores = parametros.email;
    let valoresAceptados = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    setErrors({});
    if (!parametros.email) {
      setErrors({ email: "Por favor ingresa un correo." });
    } else if (!valores.match(valoresAceptados)) {
      setErrors({ email: "Ingresa una dirección de correo válido." })
    } else {
      let fechaB = await getValueFor("fechaBloq2");

      const send = async () => { 
        setIsVisible(true);
        let uri = `${url}auth/validateEmail`;
        let api = new Api(uri, "POST", parametros);
        await api.call().then(async (res) => {
          if (res.response) {
            setIsVisible(false);
            navegacion.navigate("validarCodigo", { email: parametros.email, tipoPersona: parametros.tipoPersona });
          } else {
            setIsVisible(false);
            setErrors({ email: res.errors }); 
            setErrors({ email: res.message });
          }
        })
      }

      if(fechaB){
        let fecha = new Date();
        fechaB = JSON.parse(fechaB);
        fechaB = new Date(fechaB);
        if(fecha > fechaB){
          send();
        }else{
          alert("Has exedido el numero de intentos permitidos para enviar correos, espera 10 minutos para poder enviar correo de recuperación nuevamente."); 
        }
      }else{
        send();
      }
    }
  };

  const [loaded] = useFonts({
    "Montserrat-Bold": require("../../../../assets/fuentes/Montserrat-Bold.ttf"),
    "Montserrat-Italic": require("../../../../assets/fuentes/Montserrat-Italic.ttf"),
    "Montserrat-Light": require("../../../../assets/fuentes/Montserrat-Light.ttf"),
    "Montserrat-Medium": require("../../../../assets/fuentes/Montserrat-Medium.ttf"),
    "Montserrat-Regular": require("../../../../assets/fuentes/Montserrat-Regular.ttf"),
    "Montserrat-SemiBold": require("../../../../assets/fuentes/Montserrat-SemiBold.ttf"),
  });
  if (!loaded) {
    return null;
  }

  return (
    <View style={{ backgroundColor: "#fff", height: "100%", width: '100%', paddingTop: Platform.OS == 'ios' ? Constants.statusBarHeight : 0}}>
      <ReturnHeader/>
      <ScrollView overScrollMode='never' keyboardShouldPersistTaps='handled' style={{ backgroundColor: "#fff", height: "100%", width: '100%'}}>
        <View style={{ padding: 30, }}>
          <Text style={{ fontSize: 19, textAlign: 'center', fontFamily: 'Montserrat-Medium' }} allowFontScaling={false}>
          Ingresa tu dirección de correo electrónico para comenzar con la creación de tu cuenta.</Text>
        </View>

        <View style={{ justifyContent: 'center', alignItems: 'center' }}>
          <Input
            allowFontScaling={false}
            placeholder='Correo'
            style={styles.inputStyle}
            selectionColor={'#3D5CA4'}
            containerStyle={styles.inputContainer}
            inputContainerStyle={{ borderBottomWidth: 0 }}
            autoCapitalize='none'
            errorMessage={errors.email}
            onChange={(e) => onchange(e, "email")}
          />
        </View>
        <View style={{ justifyContent: 'center', alignContent: 'center', paddingTop: '20%' }}>
          <Button
            containerStyle={styles.containerBtn}
            buttonStyle={styles.btnStyle}
            title="Siguiente"
            titleStyle={{ fontSize: 21, letterSpacing: -0.5750000000000001, fontFamily: 'Montserrat-Medium' }}
            onPress={() => validarCorreo()}
          />
          {temcorreo && <Text allowFontScaling={false} onPress={() => { navegacion.navigate("validarCodigo", { email: temcorreo, tipoPersona: parametros.tipoPersona }) }} style={{ color: "grey", paddingTop: 25, paddingLeft: '5%' }} >Tengo un código de validación para:{'\n'}{temcorreo} </Text>}
        </View>
        <Loader
          isVisible={isVisible}
          text={"Validando Correo..."}
        />
      </ScrollView>
    </View>
  )
}

const styles = StyleSheet.create({
  containerBtn: {
    paddingLeft: '5%'
  },
  btnStyle: {
    width: "95%",
    height: 55,
    borderRadius: 6,
    backgroundColor: "#00BCD4",
  },
  inputStyle: {
    textAlign: "center",
    fontSize: 18,
    marginBottom: 5
  },
  inputContainer: {
    width: "89%",
    height: 49,
    borderWidth: 1,
    borderColor: "rgba(151,151,151,1)",
    marginBottom: 20,
    borderRadius: 6,
  },
})

