import React, { useState } from 'react';
import { SafeAreaView, StyleSheet, Text, View, StatusBar, Platform} from 'react-native';
import { Button, Input } from 'react-native-elements';
import { useNavigation, useFocusEffect} from '@react-navigation/native';
import { useFonts } from 'expo-font';
import Constants from 'expo-constants';

import ReturnIngCorreo from '../../../components/ReturnIngCorreo';
import { size } from 'lodash';

import { deleteValue, saveValue, getValueFor } from '../../../utils/localStorage';
import Loader from '../../../components/Loader';
import Api from '../../../utils/Api';
import { url } from '../../../utils/config';
import { ScrollView } from 'react-native-gesture-handler';

export default function ValidarCodigo(props) {
  const navegacion = useNavigation();
  const corr = React.useRef(props.route.params);
  const [count, setCount] = useState(120);

  const { route } = props;
  const { email, tipoPersona } = route.params;

  const [isVisible, setIsVisible] = useState(false)
  const [errors, setErrors] = useState({});
  const [parametros, setParametros] = useState({
    codigo: null,
    tipoPersona: tipoPersona
  })
  const onchange = (e, tipo) => {
    setParametros({ ...parametros, [tipo]: e.nativeEvent.text })
  };

  useFocusEffect(
      React.useCallback(()=>{
          if(Platform.OS == 'ios'){
            StatusBar.setBarStyle('dark-content');
          }

          // guardar datos de solicitud despues de enviar un correo
          const savval =  async () => {
            let soli = await getValueFor("soli2");
            let fecha2 = await getValueFor("fechaSoli2");
            let intentos = await getValueFor("intentos2");
            let fechaBloqueo = await getValueFor("fechaBloq2");

            if(intentos && !soli && !fecha2){
              intentos = JSON.parse(intentos);
              await saveValue("intentos2", JSON.stringify(intentos + 1));
              if(intentos >= 5){
                let fecha = new Date();
                fecha.setMinutes(fecha.getMinutes() + 10);
                fecha = JSON.stringify(fecha);
                await saveValue("fechaBloq2", fecha);
                deleteValue("intentos2");
              }
            }

            if(fechaBloqueo){
              fechaBloqueo = JSON.parse(fechaBloqueo);
              fechaBloqueo = new Date(fechaBloqueo);
              let fecha = new Date();
              if(fecha > fechaBloqueo){
                await saveValue("intentos2", JSON.stringify(1));
                deleteValue("fechaBloq2");
              }
            }

            if(!soli && !fecha2){
              let fecha = new Date();
              fecha = JSON.stringify(fecha);
              let value = corr.current.email;
              await saveValue("soli2", value);
              await saveValue("fechaSoli2", fecha);
            }

            if(!intentos && !fechaBloqueo){
              await saveValue("intentos2", JSON.stringify(1));
            }
          }
          savval();
      }, [])
  );

  // iniciar cuenta regresiva para volvera a enviar correo
  const regresiva = () => {
    const timer = setInterval(() => {
      setCount(prevCount => {
        if (prevCount === 0) {
          clearInterval(timer);
          return "Reenviar código";
        } else {
          return prevCount - 1;
        }
      });
    }, 1000);

    return () => {
      clearInterval(timer);
    };
  }

  // ejecuta la cuenta regresiva cuando se ingresa a la pantalla
  React.useEffect(() => {
    regresiva();
  }, []);

  // verifica que el codigo escrito sea valido
  const validarCodigo = async () => {
    setErrors({});
    if (!parametros.codigo) {
      setErrors({ codigo: "Ingresa tu código." });
    } else if (size(parametros.codigo) < 6) {
      setErrors({ codigo: "Hacen falta dígitos en el código." });
    } else {
      setIsVisible(true);
      let uri = `${url}auth/validateCode/${email}`;
      let api = new Api(uri, "POST", parametros);
      await api.call().then(res => {
        if (!res.response) {
          alert("Se ha enviado un nuevo código a tu correo.");
          setIsVisible(false);
          setErrors({ codigo: res.errors });
        } else {
          deleteValue("soli2");
          deleteValue("fechaSoli2");
          deleteValue("intentos2");
          navegacion.navigate("registro", { email, tipoPersona, tokenRegistro: res.result })
          setIsVisible(false);
        }
      });
    }
  };

  // envia un codigo por correo si aun no se exeden lso 5 intentos
  const sendCode = async () => {
    let intentos = await getValueFor("intentos2");
    let fechaBloqueo = await getValueFor("fechaBloq2");

    if( intentos && intentos < 5 ){

      intentos = JSON.parse(intentos);
      await saveValue("intentos2", JSON.stringify(intentos + 1));

      setCount(120);
      regresiva();

      let parametros = {
        email: corr.current.user,
        tipoPersona: 1
      }

      let uri = `${url}auth/validateEmail`;
      let api = new Api(uri, "POST", parametros);
      await api.call().then(async (res) => {
        if (res.response) {
          setIsVisible(false);
          alert("Se ha enviado un nuevo código a tu correo.");
        } else {
          setIsVisible(false);
          setErrors({ email: res.errors }); 
          setErrors({ email: res.message });
        }
      })
    }else{
      alert("Has exedido el numero de intentos permitidos para enviar correos, espera 10 minutos para poder enviar correo de recuperación nuevamente."); 
      if(!fechaBloqueo){
        let fecha = new Date();
        fecha.setMinutes(fecha.getMinutes() + 10);
        fecha = JSON.stringify(fecha);
        await saveValue("fechaBloq2", fecha);
        deleteValue("intentos2");
      }else{
        fechaBloqueo = JSON.parse(fechaBloqueo);
        fechaBloqueo = new Date(fechaBloqueo);
        let fecha = new Date();
        if(fecha > fechaBloqueo){
          await saveValue("intentos2", JSON.stringify(1));
          deleteValue("fechaBloq2");
        }
      }
    }
  }

  const formatTime = (time) => {
    const minutes = Math.floor(time / 60);
    const seconds = time % 60;
    return `${minutes.toString().padStart(2, '0')}:${seconds.toString().padStart(2, '0')}`;
  };

  const [loaded] = useFonts({
    "Montserrat-Bold": require("../../../../assets/fuentes/Montserrat-Bold.ttf"),
    "Montserrat-Italic": require("../../../../assets/fuentes/Montserrat-Italic.ttf"),
    "Montserrat-Light": require("../../../../assets/fuentes/Montserrat-Light.ttf"),
    "Montserrat-Medium": require("../../../../assets/fuentes/Montserrat-Medium.ttf"),
    "Montserrat-Regular": require("../../../../assets/fuentes/Montserrat-Regular.ttf"),
    "Montserrat-SemiBold": require("../../../../assets/fuentes/Montserrat-SemiBold.ttf"),
  });
  if (!loaded) {
    return null;
  }

  return (
    <View style={{ backgroundColor: "#fff", height: "100%", width: '100%', paddingTop: Platform.OS == 'ios' ? Constants.statusBarHeight : 0}}>
      <ReturnIngCorreo/>
      <ScrollView overScrollMode='never' keyboardShouldPersistTaps='handled' style={{ backgroundColor: "#fff", height: "100%", width: '100%'}}>
        <View style={{ padding: 30, }}>
          <Text style={{ fontSize: 24, textAlign: 'justify', fontFamily: 'Montserrat-Medium', paddingBottom:'5%' }} allowFontScaling={false}>
            Confirmación de código
          </Text>
          <Text style={{ fontFamily: 'Montserrat-Medium', color: 'gray', fontSize: 16, textAlign: 'justify' }} allowFontScaling={false}>
            Por favor ingresa el código que fue enviado al email:
          </Text>
          <Text style={{ fontFamily: 'Montserrat-Medium', color: 'gray', fontSize: 16, textAlign: 'justify' }} allowFontScaling={false}>
            <Text style={{fontWeight: 'bold', color: 'black'}}>
              {"\n"+email}
            </Text>
            {"\n\n"}Si el código no ha sido recibido en un lapso de 2 minutos, puedes solicitar uno nuevo.
          </Text>
        </View>

        <View style={{ justifyContent: 'center', alignItems: 'center' }}>
          <Input
            allowFontScaling={false}
            placeholder='Código'
            style={styles.inputStyle}
            selectionColor={'#3D5CA4'}
            containerStyle={styles.inputContainer}
            inputContainerStyle={{ borderBottomWidth: 0 }}
            autoCapitalize='none'
            onChange={(e) => onchange(e, "codigo")}
            errorMessage={errors.codigo}
            maxLength={6}
            keyboardType="number-pad"
          />
        </View>

        <View style={{ justifyContent: 'center', alignContent: 'center', paddingTop: '20%' }}>
          <Button
            containerStyle={styles.containerBtn}
            buttonStyle={styles.btnStyle}
            title="Siguiente"
            titleStyle={{ fontSize: 21, letterSpacing: -0.5750000000000001, fontFamily: 'Montserrat-Medium' }}
            onPress={() => validarCodigo()}
          />
          <Loader
            isVisible={isVisible}
            text="Verificando código..."
          />
        </View>

        <View style={{ justifyContent: 'center', alignContent: 'center', paddingTop: '7%', marginBottom: 50 }}>
          <Button
            containerStyle={styles.containerBtn}
            buttonStyle={styles.btnStyle}
            title={ typeof count == "string" ? count : formatTime(count)}
            titleStyle={{ fontSize: 21, letterSpacing: -0.5750000000000001, fontFamily:'Montserrat-Medium' }}
            onPress={() => {
              sendCode()
            }}
            disabled={ typeof count != "string" ? true : false}
          />
        </View>
      </ScrollView>
    </View>
  )
}

const styles = StyleSheet.create({
  containerBtn: {
    paddingLeft: '5%'
  },
  btnStyle: {
    width: "95%",
    borderRadius: 6,
    backgroundColor: "#00BCD4",
    height: 55
  },
  inputStyle: {
    textAlign: "center",
    fontSize: 18,
    marginBottom: 5
  },
  inputContainer: {
    width: "89%",
    height: 49,
    borderWidth: 1,
    borderColor: "rgba(151,151,151,1)",
    marginBottom: 20,
    borderRadius: 6,
  },
})