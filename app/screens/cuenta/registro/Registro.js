import React, { useState, } from 'react';
import { SafeAreaView, StyleSheet, Text, View, ScrollView, StatusBar, Platform} from 'react-native';
import { Button, Input, CheckBox, Icon, } from 'react-native-elements';
import { useNavigation, useFocusEffect } from '@react-navigation/native';
import { useFonts } from 'expo-font';
import Constants from 'expo-constants';

import { size } from 'lodash';
import ReturnIngCorreo from '../../../components/ReturnIngCorreo';

import Loader from '../../../components/Loader';
import Api from '../../../utils/Api';
import { url } from '../../../utils/config';
import { saveValue, getValueFor } from '../../../utils/localStorage';

const Registro = (props) => {
  const { route } = props;
  const { email, tipoPersona, tokenRegistro } = route.params;
  const [parametros, setParametros] = useState({
    tipoPersona: tipoPersona,
    nombre: "",
    apellidos: "",
    correo: email,
    telefono: "",
    password: "",
  })

  useFocusEffect(
      React.useCallback(()=>{
          if(Platform.OS == 'ios'){
            StatusBar.setBarStyle('dark-content');
          }
      })
  );

  const onchange = (e, tipo) => {
    setParametros({ ...parametros, [tipo]: e.nativeEvent.text })
  };

  const navegacion = useNavigation();
  const [isVisible, setIsVisible] = useState(false);
  const [errors, setErrors] = useState({});
  const [verPassword, setVerPassword] = useState(false);
  const [terminos, setTerminos] = useState(false);

  const registrar = async () => {
    setErrors({})
    if (!parametros.nombre || !parametros.apellidos || !parametros.password || !parametros.telefono) {
      setErrors({ password: "Todos los campos son obligatorios." })
    } else if ((size(parametros.telefono) <= 9) || (size(parametros.telefono) >= 11)) {
      setErrors({ password: "El número de teléfono debe contener al menos 10 digitos" })
    } else if (size(parametros.password) <= 5) {
      setErrors({ password: "La contraseña debe tener 6 caracteres minimo." })
    } else if (!terminos) {
      setErrors({ password: "Tiene que aceptar los términos y condiciones y nuestro aviso de privacidad." })
    } else {
      setIsVisible(true);

      let tokenPush = null;
      await getValueFor('tokenPush').then(res => tokenPush = res);
      let uri = `${url}user/registerUser`;
      let api = new Api(uri, "POST", parametros, tokenRegistro);
      await api.call().then(async (res) => {
        console.log(uri, parametros, tokenRegistro)
        if (res.response) {
          let parametrosLog = {
            user: parametros.correo,
            password: parametros.password,
            token: `${tokenPush}`,
            plataforma: "Movil"
          }
          console.log(parametrosLog, 'parametros log=========')
          let uriLog = `${url}auth/user`;
          let apiLog = new Api(uriLog, "POST", parametrosLog);
          setIsVisible(true);
          await apiLog.call()
            .then(async (resLog) => {
              console.log(resLog, '===============reslog=======')
              if (resLog.response) {
                await saveValue("token", resLog.result.token)
                await saveValue("id", resLog.result.id);
                setIsVisible(false);
                navegacion.navigate("cuentaEspera");
              } else {
                setIsVisible(false)
                setErrors({ password: `${resLog.errors}` })
                alert(`${resLog.message}`)
              }
            });
        } else {
          console.log(res)
          alert(`${res.errors}`)
          setIsVisible(false)
        }
      });
    }
  }

  const [loaded] = useFonts({
    "Montserrat-Bold": require("../../../../assets/fuentes/Montserrat-Bold.ttf"),
    "Montserrat-Italic": require("../../../../assets/fuentes/Montserrat-Italic.ttf"),
    "Montserrat-Light": require("../../../../assets/fuentes/Montserrat-Light.ttf"),
    "Montserrat-Medium": require("../../../../assets/fuentes/Montserrat-Medium.ttf"),
    "Montserrat-Regular": require("../../../../assets/fuentes/Montserrat-Regular.ttf"),
    "Montserrat-SemiBold": require("../../../../assets/fuentes/Montserrat-SemiBold.ttf"),
  });
  if (!loaded) {
    return null;
  }

  return (
    <View style={{ flex: 1, paddingTop: Platform.OS == 'ios' ? Constants.statusBarHeight : 0, backgroundColor: '#fff'}}>
      <ReturnIngCorreo />
      <ScrollView overScrollMode='never' keyboardShouldPersistTaps='handled' style={{ backgroundColor: "#fff", height: "100%"}} >
        <View style={{ padding: 20, }} >
          <Text style={styles.registrationTitle} allowFontScaling={false}>Regístrate</Text>
          <Text style={styles.title} allowFontScaling={false}>
            Para disfrutar de todos los beneficios de{'\n'}Huauchi Tour, regístrate.
          </Text>
          <Text style={styles.emailText} allowFontScaling={false}>
            {email}
          </Text>
        </View>

        <View style={styles.styleInputs}>
          <Input
            allowFontScaling={false}
            placeholder="Nombre"
            style={styles.inputStyle}
            inputContainerStyle={styles.inputContainer}
            selectionColor={'#3D5CA4'}
            onChange={(e) => onchange(e, "nombre")}
            errorMessage={errors.nombre}
          />
          <Input
            allowFontScaling={false}
            placeholder="Apellidos"
            style={styles.inputStyle}
            inputContainerStyle={styles.inputContainer}
            selectionColor={'#3D5CA4'}
            onChange={(e) => onchange(e, "apellidos")}
            errorMessage={errors.apellidos}
          />
          <Input
            allowFontScaling={false}
            placeholder="Teléfono"
            style={styles.inputStyle}
            inputContainerStyle={styles.inputContainer}
            selectionColor={'#3D5CA4'}
            keyboardType={'numeric'}
            autoCapitalize='none'
            maxLength={10}
            onChange={(e) => onchange(e, "telefono")}
            errorMessage={errors.telefono}
          />
          <Input
            allowFontScaling={false}
            placeholder="Contraseña"
            style={styles.inputStyle}
            inputContainerStyle={styles.inputContainer}
            autoCapitalize='none'
            selectionColor={'#3D5CA4'}
            onChange={(e) => onchange(e, "password")}
            password={true}
            secureTextEntry={verPassword ? false : true}
            rightIcon={
              <Icon
                type="font-awesome"
                name={verPassword ? "eye-slash" : "eye"}
                size={18}
                iconStyle={{ color: "gray", }}
                onPress={() => setVerPassword(!verPassword)}
              />
            }
            errorMessage={errors.password}
            errorStyle={{width: '92%'}}
          />
        </View>

        <View style={{ flexDirection: "row", }}>
          <View style={{ paddingLeft: '1%', paddingBottom: '4%', marginTop: -4 }}>
            <CheckBox allowFontScaling={false}
              left
              containerStyle={{
                backgroundColor: "#fff",
                borderColor: "#fff",
              }}
              checked={terminos}
              onPress={() => setTerminos(!terminos)}
            />
          </View>

          <View style={{ paddingRight: '20%', }}>
            <Text style={styles.textContainer} allowFontScaling={false}>Acepto los
              <Text style={styles.textUnderline} allowFontScaling={false}
                onPress={() => navegacion.navigate("terminosCondiciones")}> términos y condiciones
              </Text>
            </Text>
            <Text style={styles.textContainer} allowFontScaling={false}>y
              <Text style={styles.textUnderline} allowFontScaling={false}
                onPress={() => navegacion.navigate("avisoPrivacidad")}> aviso de privacidad.
              </Text>
            </Text>
          </View>
        </View>

        <View style={{ paddingTop: 20, }} >
          <Button
            allowFontScaling={false}
            buttonStyle={styles.btnStyle}
            containerStyle={styles.containerBtn}
            title="Registrarse"
            titleStyle={{ fontSize: 21, letterSpacing: -0.5750000000000001, fontFamily: 'Montserrat-Medium' }}
            onPress={() => registrar()}
          />
          <Text style={styles.haveCount} allowFontScaling={false}>
            ¿Ya tienes cuenta? <Text style={{ fontFamily: 'Montserrat-Bold', }} onPress={() => navegacion.navigate("login")} allowFontScaling={false}>Inicia sesión.</Text>
          </Text>
        </View>
        <Loader
          isVisible={isVisible}
          text="Cargando..."
        />
      </ScrollView>
    </View>
  )
}

export default Registro

const styles = StyleSheet.create({
  registrationTitle: {
    fontSize: 27,
    fontFamily: 'Montserrat-Medium',
    textAlign: 'center',
    paddingBottom: '5%'
  },
  title: {
    color: "rgba(151,158,181,1)",
    fontSize: 17,
    letterSpacing: -0.4,
    fontFamily: 'Montserrat-Light',
    textAlign: 'center',
  },
  emailText: {
    fontFamily: 'Montserrat-Medium',
    color: 'gray',
    fontSize: 16,
    textAlign: 'center',
  },
  styleInputs: {
    alignItems: "center",
    paddingTop: 20,
    paddingBottom: '4%',
    paddingLeft: '2%',
  },
  inputStyle: {
    textAlign: "center",
    fontSize: 18,
  },
  inputContainer: {
    width: "95%",
    height: 50,
    borderWidth: 1,
    borderColor: "rgba(151,151,151,1)",
    borderRadius: 6,
  },
  textContainer: {
    flexDirection: 'row',
    color: "grey",
    fontSize: 16,
    fontFamily: 'Montserrat-Regular',
  },
  textUnderline: {
    textDecorationLine: 'underline',
    fontFamily: 'Montserrat-Bold',
  },
  btnStyle: {
    width: "95%",
    height: 55,
    borderRadius: 6,
    backgroundColor: "#00BCD4",
  },
  containerBtn: {
    paddingLeft: '5%',
  },
  haveCount: {
    color: "grey",
    paddingTop: 25,
    paddingBottom: 25,
    fontSize: 16,
    textAlign: 'center',
    fontFamily: 'Montserrat-Regular',
  },
})
