import React, { useCallback } from 'react';
import { SafeAreaView, StyleSheet, Text, View, ActivityIndicator, ImageBackground, StatusBar, Platform } from 'react-native';
import { Button, Image } from 'react-native-elements';
import { useFocusEffect, useNavigation } from '@react-navigation/native';
import {useFonts}from 'expo-font';
import Constants from 'expo-constants';


const Invitado = () => {
  const navegacion = useNavigation();

//const [loaded]=useFonts({
  //  'Helvetica-Bold': require('../../../../assets/Fonts/Helvetica-Bold.ttf'),
  // 'Helvetica': require('../../../../assets/Fonts/Helvetica.ttf'),
  //});

  useFocusEffect(
      React.useCallback(()=>{
        if(Platform.OS == 'ios'){
          StatusBar.setBarStyle('dark-content');
        }
      })
  );

  const [loaded]=useFonts({
    "Montserrat-Bold": require("../../../assets/fuentes/Montserrat-Bold.ttf"),
    "Montserrat-Italic": require("../../../assets/fuentes/Montserrat-Italic.ttf"),
    "Montserrat-Light": require("../../../assets/fuentes/Montserrat-Light.ttf"),
    "Montserrat-Medium": require("../../../assets/fuentes/Montserrat-Medium.ttf"),
    "Montserrat-Regular": require("../../../assets/fuentes/Montserrat-Regular.ttf"),
    "Montserrat-SemiBold": require("../../../assets/fuentes/Montserrat-SemiBold.ttf"),
  });

  if (!loaded){
    return null;
  }

  return (
    <View style={styles.container}>
      <View style={{ justifyContent: "center", alignItems: "center", marginTop: "-40%"}}>
        <ImageBackground style={{width: 150, height: 150}} source={require('../../../assets/htlogo.png')} resizeMode={"contain"}></ImageBackground>
        <Text style={{ fontFamily:'Montserrat-Bold', fontSize: 25, paddingTop: '2%' }} allowFontScaling={false}>Huauchi Tour</Text>
      </View>
      <View style={{ justifyContent: "center", alignItems: "center", marginTop: "20%", }}>
        <Button
          allowFontScaling={false}
          title={'Iniciar sesión'}
          titleStyle={styles.titleBtn}
          containerStyle={styles.containerBtnIniciar}
          buttonStyle={styles.btnIniciar}
          onPress={() => { navegacion.navigate("login") }}
        />
        <Button
          allowFontScaling={false}
          title={'Registrarte'}
          titleStyle={styles.titleBtn}
          containerStyle={styles.containerBtnRegistrar}
          buttonStyle={styles.btnRegistrar}
          onPress={() => { navegacion.navigate("ingresaCorreo") }}
        />
      </View>
    </View>
  )
}
export default Invitado

const styles = StyleSheet.create({
  container: {
    backgroundColor: "#fff",
    height: "100%",
    width: "100%",
    justifyContent: "center",
    paddingTop: Platform.OS == 'ios' ? Constants.statusBarHeight : 0
  },
  btnIniciar: {
    borderRadius: 6,
    backgroundColor: "#3498DB",
    height: 50,
  },
  containerBtnIniciar: {
    width: "90%",
    marginBottom: '8%',
  },
  titleBtn: {
    fontSize: 21,
    margin: -3,
    fontFamily:'Montserrat-Medium',
  },
  btnRegistrar: {
    borderRadius: 6,
    backgroundColor: "#00BCD4",
    height: 50
  },
  containerBtnRegistrar: {
    width: "90%",
  },
})