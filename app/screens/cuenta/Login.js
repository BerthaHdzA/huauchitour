import React, { useState } from 'react';
import { SafeAreaView, StyleSheet, Text, View, Linking, StatusBar, Platform} from 'react-native';
import { Input, Button, Icon, } from 'react-native-elements';
import { useNavigation, useFocusEffect } from '@react-navigation/native';
import { useFonts } from 'expo-font';
import Constants from 'expo-constants';

import ReturnHeader from '../../components/ReturnHeader';

import Loader from '../../components/Loader';
import { url } from '../../utils/config';
import { saveValue, getValueFor } from '../../utils/localStorage';
import Api from '../../utils/Api';
import { ScrollView } from 'react-native-gesture-handler';

export default function Login() {
  const navegacion = useNavigation();

  const [isVisible, setIsVisible] = useState(false);
  const [verPassword, setVerPassword] = useState(false);
  const [errors, setErrors] = useState({});

  const [parametros, setParametros] = useState({
    user: '',
    password: '',
    token: '',
    plataforma: "Movil"
  });


  useFocusEffect(
      React.useCallback(()=>{
        if(Platform.OS == 'ios'){
            StatusBar.setBarStyle('dark-content');
        }
      })
  );

  const onChange = (e, type) => {
    setParametros({ ...parametros, [type]: e.nativeEvent.text })
  }

  const handlePress = async () => {
    await Linking.openURL("mailto: correo@correo.com");
  }

  const iniciarsesion = async () => {
    
    setErrors({});
    let data = parametros;
    let valoresAceptados = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

    if(!parametros.password && !parametros.user){
      setErrors({password: "Ingresa una contraseña", mail: "Ingresa un correo"});
    } else if(!parametros.password) {
      setErrors({password: "Ingresa una contraseña"});
      if(!parametros.user.match(valoresAceptados)) {
        setErrors({password: "Ingresa una contraseña", mail: "Ingresa una dirección de correo válido"});
      }
    } else if(!parametros.user) {
      setErrors({mail: "Ingresa un correo"});
    } else if(!parametros.user.match(valoresAceptados)) {
      setErrors({mail: "Ingresa una dirección de correo válido"});
    } else {
      let uri = "";
      let api = "";
  
      uri = `${url}auth/user`;
      api = new Api(uri, "POST", data);
  
      setIsVisible(true);
  
      await api.call()
        .then(async (res) => {
          // console.log(res)
          if (res.response) {
  
            let tokenPush = await getValueFor('tokenPush');
            
            //servicio para notificaciones
            uri = `${url}eventos/tokenPush`;
            let parametrosToken = {
              id: res.result.id,
              token: tokenPush,
              plataforma: "Movil"
            }
            api = new Api(uri, "POST", parametrosToken);
            api.call().then((res)=>{
              if(res.response){
                console.log(tokenPush)
                console.log(res.message + " Token registrado");
              }else{
                console.log(res.message + " Inicia Sesion Para Registrar Token");
              }
            });
  
  
            await saveValue("token", res.result.token);
            await saveValue("id", res.result.id);
            setIsVisible(false);
            navegacion.navigate("cuentaEspera");
          }
          else {
            console.log(res.result);
            setIsVisible(false)
            // console.log(res)
            if(res.result == "Request"){
              alert(res.message);
            } else {
              setErrors({ password: `${res.message}` });
            }
          }
  
        }
      );
    }
  }

  const [loaded] = useFonts({
    "Montserrat-Bold": require("../../../assets/fuentes/Montserrat-Bold.ttf"),
    "Montserrat-Italic": require("../../../assets/fuentes/Montserrat-Italic.ttf"),
    "Montserrat-Light": require("../../../assets/fuentes/Montserrat-Light.ttf"),
    "Montserrat-Medium": require("../../../assets/fuentes/Montserrat-Medium.ttf"),
    "Montserrat-Regular": require("../../../assets/fuentes/Montserrat-Regular.ttf"),
    "Montserrat-SemiBold": require("../../../assets/fuentes/Montserrat-SemiBold.ttf"),
  });

  if (!loaded) {
    return null;
  }

  return (
    <View style={{ backgroundColor: "#fff", height: "100%", width: '100%', paddingTop: Platform.OS == 'ios' ? Constants.statusBarHeight : 0}}>
      <ReturnHeader />
      <ScrollView overScrollMode='never' keyboardShouldPersistTaps='handled' style={{ backgroundColor: "#fff", height: "100%", width: '100%'}}>
        <View style={{ margin: 20, }}>
          <Text style={{ fontSize: 27, fontFamily: 'Montserrat-Medium' }} allowFontScaling={false}>Bienvenido 👋</Text>
          <Text style={styles.title} allowFontScaling={false}>
            Estamos contentos de verte de nuevo. {"\n"}Inicia sesión con tu cuenta.
          </Text>
        </View>
        <View style={{ justifyContent: 'center', alignItems: 'center' }}>
          <Input
            allowFontScaling={false}
            placeholder='Correo Electrónico'
            style={styles.inputStyle}
            selectionColor={'#3D5CA4'}
            containerStyle={styles.inputContainer}
            inputContainerStyle={{ borderBottomWidth: 0 }}
            autoCapitalize={'none'}
            onChange={(e) => onChange(e, "user")}
            errorMessage={errors.mail}
          />
          <Input
            allowFontScaling={false}
            placeholder="Contraseña"
            style={styles.inputStyle}
            selectionColor={'#3D5CA4'}
            containerStyle={styles.inputContainer}
            inputContainerStyle={{ borderBottomWidth: 0 }}
            autoCapitalize={'none'}
            onChange={(e) => onChange(e, "password")}
            password={true}
            secureTextEntry={verPassword ? false : true}
            rightIcon={
              <Icon
                type="font-awesome"
                name={verPassword ? "eye-slash" : "eye"}
                size={18}
                iconStyle={{ color: "gray" }}
                onPress={() => setVerPassword(!verPassword)}
              />
            }
            errorMessage={errors.password}
          />
        </View>
        <View>
          <Text style={styles.forgottenPassword} allowFontScaling={false}
            onPress={() => navegacion.navigate("recuperarPassword")}
          >¿Olvidaste tu contraseña?</Text>
        </View>
          
        <View style={{ justifyContent: "center", alignContent: "center", }} >
          <Button
            allowFontScaling={false}
            containerStyle={styles.containerIngresar}
            buttonStyle={styles.btnIngresar}
            title="Iniciar"
            titleStyle={{ fontSize: 22, letterSpacing: -0.5750000000000001, fontFamily: 'Montserrat-Medium' }}
            onPress={() => iniciarsesion()}
          />

          </View>
          <View style={{ alignItems: "center"}} >

          <Text allowFontScaling={false}
            style={{ color: "rgba(151,158,181,1)", marginTop: 10, fontSize: 17, fontFamily: 'Montserrat-Regular' }}
          >¿No tienes cuenta? <Text style={{ fontFamily: 'Montserrat-Bold' }} onPress={() => navegacion.navigate("ingresaCorreo")} >Crea una.</Text> </Text>

          {/* <Text allowFontScaling={false}
            style={{ 
              color: "rgba(151,158,181,1)", 
              marginTop: 10, 
              fontSize: 17, 
              fontFamily: 'Montserrat-Regular', 
              textAlign: 'center',
              marginBottom: 20 
              }}>¿Dudas o aclaraciones? Contactanos: 
              <Text style={{
                fontSize: 19, 
                fontFamily: 'Montserrat-Bold', 
                textDecorationLine: 'underline' 
              }}
              allowFontScaling={false} 
              onPress={() => handlePress()}>
                correo@correo.com
            </Text>
          </Text> */}

        </View>
        <Loader isVisible={isVisible} text="Cargando..." />
      </ScrollView>
    </View>
  )
}

const styles = StyleSheet.create({
  title: {
    color: "rgba(151,158,181,1)",
    fontSize: 18,
    letterSpacing: -0.4,
    fontFamily: 'Montserrat-Regular',
  },
  containerIngresar: {
    paddingLeft: '5%',  
  },
  btnIngresar: {
    width: "95%",
    height: 55,
    borderRadius: 6,
    backgroundColor: "#3498DB", 
  },
  inputContainer: {
    width: "89%",
    height: 49,
    borderWidth: 1,
    borderColor: "rgba(151,151,151,1)",
    marginBottom: 30,
    borderRadius: 6,
  },
  inputStyle: {
    textAlign: "center",
    fontSize: 18,
    marginBottom: 5
  },
  forgottenPassword: {
    marginBottom: 25,
    marginTop: 4,
    marginRight: 20,
    color: "rgba(151,158,181,1)",
    textAlign: 'right',
    fontSize: 16,
    fontFamily: 'Montserrat-Bold',
  }
})