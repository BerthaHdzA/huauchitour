import React, { useState, useCallback } from 'react';
import { useFocusEffect } from '@react-navigation/native';

import Invitado from './Invitado';
import Cuenta from './Cuenta';

import { getValueFor } from '../../utils/localStorage';
import Loader from '../../components/Loader'

const CuentaEspera = () => {
  const [login, setLogin] = useState(null);
  const [loading, setloading] = useState(true);
  // console.log(login,"=============")
  useFocusEffect(
    useCallback(() => {
      async function loginVerification(){
        setloading(true)
        await getValueFor("token").then((token) => {
          (token === false) ? setLogin(false) : setLogin(true)
        })
        setloading(false)
      }
      loginVerification();
    }, [login]
    )
  )
    return (
        (loading) ? (
          <Loader isVisible={true} text={"Cargando..."} />
        ): (
          login ? <Cuenta setLogin={setLogin} /> : <Invitado />
        )
    )
  // if (loading) return <Loader isVisible={true} text={"Cargando..."} />
  // return login ? <Cuenta setLogin={setLogin} /> : <Invitado />
}
export default CuentaEspera
