import React, { useState, useCallback, useRef } from 'react';
import { SafeAreaView, Alert, ScrollView,Text, View,  StyleSheet, StatusBar, Platform} from 'react-native';
import { Divider, Input } from 'react-native-elements';
import CloseMenu from '../../../components/CloseMenu';
import { useFonts } from 'expo-font';
import Constants from 'expo-constants';
import { useFocusEffect, useIsFocused } from '@react-navigation/native';
import { url } from '../../../utils/config';
import Api from '../../../utils/Api';
import { getValueFor } from '../../../utils/localStorage';
import Loader from '../../../components/Loader';

export default function MembershipPass() {

    const [plan, setPlan] = useState([]);
    const [membresia, setMembresia] = useState([]);
    const color = useRef("000");
    const [dataMem, setDataMem] = useState({
      status: 'inactivo',
      text: "Cargando..."
    });

    const focus = useIsFocused()

    useFocusEffect(
        useCallback(()=>{

            if(Platform.OS == 'ios'){
                StatusBar.setBarStyle('light-content');
            }
            
            //obtiene los datos del ultimo plan del usuario
            const getPlan = async () => {

                let id = await getValueFor('id');
                let token = await getValueFor('token');

                let uri = "";
                let api = "";

                uri = `${url}membership/detail/${id}`;
                api = new Api(uri, "GET", null, token);

                await api.call().then(res => {
                    if(res.response){
                        setMembresia(res.result);
                        uri = `${url}membership/obtenerPlan/${res.result.plan_id}`;
                        api = new Api(uri, "GET", null, token);
                        
                        api.call().then(res => {
                            if (res.response) {
                                setPlan(res.result)
                            } else {
                                console.log(res.errors);
                            }
                        })
                    } else {
                        console.log(res.errors);
                    }
                })
            }
            getPlan();

            // Verifica el estado de la membresia
            const obtenerInfo = async () => {

                let id = await getValueFor('id');
                let token = await getValueFor('token');

                let uri = `${url}scan/validateMembership/${id}`
                let api = new Api(uri, "GET", null, token);
                api.call().then(res => {
                if(res.response || res.errors == "qr no valido"){
                    if(!res.errors){
                    color.current = '16963a';
                    setDataMem({
                        status: 'activo',
                        text: "Suscripción activa"
                    })
                    }else{
                    color.current = 'e93f2c'
                    setDataMem({
                        status: 'inactivo',
                        text: "Suscripción inactiva"
                    })
                    }
                }else{
                    if(!res.errors){
                    color.current = 'e93f2c'
                    setDataMem({
                        status: 'inactivo',
                        text: "No pudimos determinar el estado de tu suscripción, por favor inténtalo de nuevo más tarde."
                    })
                    }else{
                    color.current = 'e93f2c'
                    setDataMem({
                        status: 'inactivo',
                        text: "Suscripción inactiva"
                    })
                    }
                }
                })
            }
            obtenerInfo();
            }, []
        )
    );

    const procesarFecha = (fecha) => {
        let fechaS = fecha.split(" ");
        let fechaS2 = fechaS[0].split("-");
        let fechaF = "";

        switch(fechaS2[1]){
            case "01":
                fechaF = fechaS2[2] + " de Enero del " + fechaS2[0]
                break;
            case "02":
                fechaF = fechaS2[2] + " de Febrero del " + fechaS2[0]
                break;
            case "03":
                fechaF = fechaS2[2] + " de Marzo del " + fechaS2[0]
                break;
            case "04":
                fechaF = fechaS2[2] + " de Abril del " + fechaS2[0]
                break;
            case "05":
                fechaF = fechaS2[2] + " de Mayo del " + fechaS2[0]
                break;
            case "06":
                fechaF = fechaS2[2] + " de Junio del " + fechaS2[0]
                break;    
            case "07":
                fechaF = fechaS2[2] + " de Julio del " + fechaS2[0]
                break;
            case "08":
                fechaF = fechaS2[2] + " de Agosto del " + fechaS2[0]
                break;
            case "09":
                fechaF = fechaS2[2] + " de Septiembre del " + fechaS2[0]
                break;
            case "10":
                fechaF = fechaS2[2] + " de Octubre del " + fechaS2[0]
                break;
            case "11":
                fechaF = fechaS2[2] + " de Noviembre del " + fechaS2[0]
                break;
            case "12":
                fechaF = fechaS2[2] + " de Diciembre del " + fechaS2[0]
                break; 
            default:
                fechaF = "Error al encontrar la fecha correcta"
                break; 
        }

        return fechaF;
    }

    const [loaded] = useFonts({
        "Montserrat-Bold": require("../../../../assets/fuentes/Montserrat-Bold.ttf"),
        "Montserrat-Italic": require("../../../../assets/fuentes/Montserrat-Italic.ttf"),
        "Montserrat-Light": require("../../../../assets/fuentes/Montserrat-Light.ttf"),
        "Montserrat-Medium": require("../../../../assets/fuentes/Montserrat-Medium.ttf"),
        "Montserrat-Regular": require("../../../../assets/fuentes/Montserrat-Regular.ttf"),
        "Montserrat-SemiBold": require("../../../../assets/fuentes/Montserrat-SemiBold.ttf"),
    });
    if (!loaded) {
        return null;
    }

        return (
            plan == "" ? 
                <Loader isVisible={true} text="Cargando..." /> 
            :
                <View style={{ flex: 1, backgroundColor: 'white'}}>
                    <View style={styles.estilo1}>
                    <View style={styles.estilo2}></View>
                    <View style={styles.estilo3}>
                        <CloseMenu />
                        <Text style={styles.letra} allowFontScaling={false}>Suscripción Huauchi Pass</Text>
                    </View>
                    </View>
        
                    <View style={styles.container}>
                        <View style={{ paddingTop: '3%', paddingBottom: '3%', paddingLeft: '5%', }}></View>
                        <View style={{flexDirection: 'row', paddingBottom: '6%', flexDirection: 'column'}}>
                            <Text style={styles.textStyle} allowFontScaling={false}>Plan:</Text>
                            <Text style={styles.textSubs} allowFontScaling={false}>{ (plan != null && plan != "") && (membresia != null && membresia != "") ? plan.nombre + " (" + plan.tiempoEnDias + " dias)" : " "} </Text>
                        </View>
        
                        <Divider orientation='horizontal' width={2} color='#F2F3F4' />
                        <View style={{flexDirection: 'row', paddingBottom: '6%', flexDirection: 'column'}}>
                            <Text style={styles.textStyle} allowFontScaling={false}>Precio:</Text>
                            <Text style={styles.textSubs} allowFontScaling={false}>{ (plan != null && plan != "") && (membresia != null && membresia != "")  ? "$" + plan.precio + " MXN" : " "}</Text>
                        </View>
        
                        <Divider orientation='horizontal' width={2} color='#F2F3F4' />
                        <View style={{flexDirection: 'row', paddingBottom: '6%', flexDirection: 'column'}}>
                            <Text style={styles.textStyle} allowFontScaling={false}>Método de pago:</Text>
                            <Text style={styles.textSubs} allowFontScaling={false}>{ (plan != null && plan != "") && (membresia != null && membresia != "")  ? plan.metodoPago : " "}</Text>
                        </View>
        
                        <Divider orientation='horizontal' width={2} color='#F2F3F4' />
                        <View style={{flexDirection: 'row', paddingBottom: '6%', flexDirection: 'column'}}>
                            <Text style={styles.textStyle} allowFontScaling={false}>Fecha de activación:</Text>
                            <Text style={[styles.textSubs]} allowFontScaling={false}>{(plan != null && plan != "") && (membresia != null && membresia != "") ? procesarFecha(membresia.fechaUltimoPago) : " "}</Text>
                        </View>
        
                        <Divider orientation='horizontal' width={2} color='#F2F3F4' />
                        <View style={{flexDirection: 'row', paddingBottom: '6%', flexDirection: 'column'}}>
                            <Text style={styles.textStyle} allowFontScaling={false}>Fecha de expiración:</Text>
                            <Text style={[styles.textSubs]} allowFontScaling={false}>{(plan != null && plan != "") && (membresia != null && membresia != "") ? procesarFecha(membresia.fechaExpiracion) : " "}</Text>
                        </View>
                    </View>
                    <View style={[styles.statusMembership, { overflow: 'hidden', justifyContent: 'center', alignItems: 'center'}]}>
                        <Text allowFontScaling={false} style={{textAlign: 'center', width: '90%', fontSize: 18, fontFamily: 'Montserrat-SemiBold', color: `#${color.current}`}}>{dataMem.text}</Text>
                    </View>
                </View>
        )
}

const styles = StyleSheet.create({
    
    estilo1: {
        backgroundColor: '#30a1fa',
        height: 'auto',
        alignItems: 'center',
        justifyContent: 'center',
        flexDirection: 'column',
        width: '100%'
    },
    estilo2:{
        height: Platform.OS == 'ios' ? Constants.statusBarHeight : 0,
        width: '100%'
    },
    estilo3:{
        width: '100%',
        height: 80,
        alignItems: 'center',
        justifyContent: 'center',
        flexDirection: 'row',
        paddingHorizontal: 16
    },
    letra: {
        color: '#fff',
        fontSize: 20,
        fontFamily: 'Montserrat-Medium',
    },
    container: {
        flex: 1,
        backgroundColor: "white",
    },
    textStyle: {
        color: 'gray',
        fontSize: 18,
        paddingTop: '3%',
        paddingLeft: '5%',
        fontFamily: 'Montserrat-Regular'
    },
    textSubs: {
        color: 'black',
        fontSize: 18,
        paddingTop: '3%',
        fontFamily: 'Montserrat-Medium',
        marginLeft: '5%'
    },
    statusMembership: {
        height: 100,
        width: '99%',
        alignSelf: 'center',
    }
})