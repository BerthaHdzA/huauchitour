import * as React from 'react';
import {StatusBar, SafeAreaView, ScrollView, StyleSheet, Text, View, TouchableOpacity, Image, Platform, Share} from 'react-native';
import CloseMenu from '../../../components/CloseMenu';
import MapView, { Marker } from 'react-native-maps';
import { Ionicons } from '@expo/vector-icons';
import { useFonts } from 'expo-font';
import { useState } from 'react';
import { FlatList, GestureHandlerRootView} from 'react-native-gesture-handler';
import BottomSheet from '@gorhom/bottom-sheet';
import Constants from 'expo-constants';
import Api from '../../../utils/Api';
import { url } from '../../../utils/config';
import { useFocusEffect } from '@react-navigation/native';
import * as Linking from 'expo-linking';
import * as Application from 'expo-application';

export default function CentrosAutorizados() {

    const bottomSheetRef = React.useRef(null);
    const [data, setData] = React.useState([]);
    const [dataSheet, setDataSheet] = React.useState([]);
    const [isOpen, setIsOpen]=React.useState(false);
    const snapPoints=['33%','80%'];
    const handlerSheetChange=React.useCallback(
      (index)=>{
        if(index==1){
          setIsOpen(true);
        }
        else {
          setIsOpen(false)
        }
      }
    );

    useFocusEffect(
      React.useCallback(() => {
        if(Platform.OS == 'ios'){
          StatusBar.setBarStyle('light-content');
        }
        
        const getCentros = async () => {
          
          let uri = "";
          let api = null;

          uri = `${url}centros_autorizados/obtenerCentros`;
          api = new Api(uri, "GET");
          await api.call().then((res)=>{
            if(res.response){
              setData(res.result);
              setDataSheet(res.result[0]);
            }else{
              setData([]);
              setDataSheet([]);
            }
          });
        }
        getCentros();
      }, [data.length])
    );

    const compareHours = (hourA, hourC, hN, mN) => {
      let hourAArr = hourA.split(":");
      let hourCArr = hourC.split(":");

      if(hN > parseInt(hourAArr[0]) && hN < parseInt(hourCArr[0])){
        return "Abierto";
      }else if(hN < parseInt(hourAArr[0]) || hN > parseInt(hourCArr[0])){
        return "Cerrado";
      }else if(parseInt(hourAArr[0]) == hN && parseInt(hourCArr[0]) == hN){
        if(mN >= parseInt(hourAArr[1]) && mN <= parseInt(hourCArr[1])){
          return "Abierto";
        }else{
          return "Cerrado";
        }
      }else{
        if(hN == parseInt(hourAArr[0])){
          if(mN >= parseInt(hourAArr[1])){
            return "Abierto";
          }else{
            return "Cerrado";
          }
        }else if(hN == parseInt(hourCArr[0])){
          if(mN <= parseInt(hourCArr[1])){
            return "Abierto";
          }else{
            return "Cerrado";
          }
        }
      }
    }

    const openOrClose = (dataS) => {
      let today = new Date();
      let nameDay = today.getDay();
      let hourN = today.getHours();
      let minutesN = today.getMinutes();
      
      switch(nameDay){
        case 0:
          if(dataS.domingoA && dataS.domingoC){
            return compareHours(dataS.domingoA, dataS.domingoC, hourN, minutesN);
          }else{
            return "Cerrado";
          }
          break;
        case 1:
          if(dataS.lunesA && dataS.lunesC){
            return compareHours(dataS.lunesA, dataS.lunesC, hourN, minutesN);
          }else{
            return "Cerrado";
          }
          break;
        case 2:
          if(dataS.miemartesA && dataS.miemartesC){
            return compareHours(dataS.martesA, dataS.martesC, hourN, minutesN);
          }else{
            return "Cerrado";
          }
          break;
        case 3:
          if(dataS.miercolesA && dataS.miercolesC){
            return compareHours(dataS.miercolesA, dataS.miercolesC, hourN, minutesN);
          }else{
            return "Cerrado";
          }
          break;
        case 4:
          if(dataS.juevesA && dataS.juevesC){
            return compareHours(dataS.juevesA, dataS.juevesC, hourN, minutesN);
          }else{
            return "Cerrado";
          }
          break;
        case 5:
          if(dataS.viernesA && dataS.viernesC){
            return compareHours(dataS.viernesA, dataS.viernesC, hourN, minutesN);
          }else{
            return "Cerrado";
          }
          break;
          if(dataS.sabadoA && dataS.sabadoC){
            return compareHours(dataS.sabadoA, dataS.sabadoC, hourN, minutesN);
          }else{
            return "Cerrado";
          }
        case 6:
          break;
        default:
          return "Cerrado";
          break;
      }
      return nameDay;
    }

    const makePhoneCall = async (phoneNumber) => {
      const canOpen = await Linking.canOpenURL(`tel:${phoneNumber}`);
      if (canOpen) {
        Linking.openURL(`tel:${phoneNumber}`);
      } else {
        alert('La aplicación de teléfono no está disponible.');
      }
    };

    const openMaps = (latitude, longitude) => {
      const scheme = Platform.OS === 'ios' ? 'maps:' : 'geo:';
      const url = `${scheme}${latitude},${longitude}`;
      
      Linking.openURL(url);
      Linking.openURL(`http://www.google.com/maps/place/${latitude},${longitude}`)
    };

    const openUrl = async (url) => {
      const canOpen = await Linking.canOpenURL(url);
      if (canOpen) {
        Linking.openURL(url);
      } else {
        alert('No se puede abrir este sitio.');
      }
    };

    const openFace = async (url) => {
      // Linking.openURL(`${url}`); //La validacion 
      openUrl(url);
    }

    const openInsta = async (url) => {
      Linking.openURL(`instagram:${url}`);
      openUrl(url);
    }

    const shareLocation = async (latitude, longitude, nombre, direccion, telefono) => {
      try {
        const result = await Share.share({
          message: `Te comparto este centro autorizado de Huauchi Tour: http://www.google.com/maps/place/${latitude},${longitude}` + '\n' + nombre + '\n' + direccion + '\n' + telefono,
          title: "Centro autorizado "
        });
        if (result.action === Share.sharedAction) {
          console.log('La ubicación se compartió correctamente');
        } else if (result.action === Share.dismissedAction) {
          console.log('El usuario canceló el compartir');
        }
      } catch (error) {
        console.log(error.message);
      }
    };

    const [loaded]=useFonts({
        "Montserrat-Bold": require("../../../../assets/fuentes/Montserrat-Bold.ttf"),
        "Montserrat-Italic": require("../../../../assets/fuentes/Montserrat-Italic.ttf"),
        "Montserrat-Light": require("../../../../assets/fuentes/Montserrat-Light.ttf"),
        "Montserrat-Medium": require("../../../../assets/fuentes/Montserrat-Medium.ttf"),
        "Montserrat-Regular": require("../../../../assets/fuentes/Montserrat-Regular.ttf"),
        "Montserrat-SemiBold": require("../../../../assets/fuentes/Montserrat-SemiBold.ttf"),
      });
    if (!loaded) {
        return null;
    }
    
    return (
        <View style={{ flex: 1}}>
        {/* <StatusBar backgroundColor={'#30a1fa'} barStyle={'light-content'}/> */}
          <GestureHandlerRootView>
            <View style={styles.estilo1}>
              <View style={styles.estilo2}></View>
              <View style={styles.estilo3}>
                <CloseMenu />
                <Text style={styles.letra} allowFontScaling={false}>Centros Autorizados</Text>
              </View>
            </View>
            <View style={styles.scrollContainer}>
              <MapView 
                style={{ width: '100%', height: 700,}} 
                region ={{
                  latitude: 20.17503, 
                  longitude: -98.05180, 
                  latitudeDelta: 0.003, 
                  longitudeDelta: 0.003, 
                }}
              >
                {
                  data ? (data.map(marker => (
                    <Marker
                      coordinate={{
                        latitude: parseFloat(marker.latitud),
                        longitude: parseFloat(marker.longitud),
                      }}
                      key={marker.id}
                      title={marker.nombreCentro}
                      onPress={()=>{setDataSheet(marker)}}
                    >
                      <Image style={{ left: 3, height: 40, width: 40}} source={require('../../../../assets/sombra.png')}/>
                    </Marker>
                  ))
                  ):(
                    console.log("No hay centros autorizados regitrados.")
                  )
                }
              </MapView>
              <BottomSheet 
                style={styles.sheet}
                backgroundStyle={styles.sombra}
                ref={bottomSheetRef} 
                snapPoints={snapPoints} 
                onChange={handlerSheetChange}
              >
                {data.length == 0 ? (
                  <Text style={{fontSize:20, fontWeight: 'bold', marginTop: 30, textAlign: 'center', marginHorizontal: 30}} allowFontScaling={false}>No hay centros autorizados registrados</Text>
                ):(
                  <View style={{height: '100%'}}>
  
                    <View style={{flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', position: 'relative', height: "20%", width: "100%", paddingHorizontal: 25}}>
                      <Text allowFontScaling={false} style={{fontSize:23, width: "80%", fontWeight:'bold', textAlign:'left', marginBottom:'5%'}}>{dataSheet.nombreCentro}</Text>
                      <TouchableOpacity 
                        style={{top:-10}}
                        onPress={()=>{openMaps(parseFloat(dataSheet.latitud), parseFloat(dataSheet.longitud))}}
                      >
                        <Ionicons 
                          name="car"
                          size={45}
                        />
                      </TouchableOpacity>
                    </View>
  
                    <View style={{marginTop: 5 ,flexDirection: 'row', height: '7%', width: "100%", justifyContent: 'space-around'}}> 
                      <TouchableOpacity 
                        style={styles.iconBox}
                        onPress={()=>makePhoneCall(dataSheet.telefono)}
                      >
                        <Ionicons 
                          name="call"
                          size={25}
                          style={{color: '#3498DB'}}
                        />
                        <Text allowFontScaling={false} style={{color: '#3498DB'}}>Llamar</Text>
                      </TouchableOpacity>
                      {
                        dataSheet.sitioWeb ? (
                          <TouchableOpacity 
                            style={styles.iconBox}
                            onPress={()=>openUrl(dataSheet.sitioWeb)}
                          >
                            <Ionicons
                              name="globe"
                              size={25}
                              style={{color: '#3498DB'}}
                            />
                            <Text allowFontScaling={false} style={{color: '#3498DB'}}>Sitio Web</Text>
                          </TouchableOpacity>
                        ) : (
                          console.log("No posee sitio web")
                        )
                      }
                      <TouchableOpacity 
                        style={styles.iconBox}
                        onPress={()=>{shareLocation(parseFloat(dataSheet.latitud), parseFloat(dataSheet.longitud), dataSheet.nombreCentro, dataSheet.direccion, dataSheet.telefono)}}
                      >
                        <Ionicons
                          name="share-social"
                          size={25}
                          style={{color: '#3498DB'}}
                          />
                        <Text allowFontScaling={false} style={{color: '#3498DB'}}>Compartir</Text>
                      </TouchableOpacity>
                    </View>
  
                    <View style={{flexDirection: 'column', overflow: 'hidden', height: '23%', width: "100%", marginTop: 60}}>
                      <View style={styles.iconBox2}>
                        <Ionicons
                          name="location"
                          size={25}
                          style={{marginRight: 20}}
                        />
                        <Text allowFontScaling={false}>{dataSheet.direccion}</Text>
                      </View>
                      <View style={styles.iconBox2}>
                        <Ionicons
                          name='time'
                          size={25}
                          style={{marginRight: 20}}
                        />
                        <Text allowFontScaling={false}>{dataSheet ? openOrClose(dataSheet) : " "}</Text>
                      </View>
                      <View style={styles.iconBox2}>
                        <Ionicons
                          name='call'
                          size={25}
                          style={{marginRight: 20}}
                        />
                        <Text allowFontScaling={false}>{dataSheet.telefono}</Text>
                      </View>
                    </View>
                    
                    {
                      dataSheet.facebook || dataSheet.instagram ? (
                        <View style={{flexDirection: 'column', height: '25%', width: "100%", marginTop: 20, overflow: 'hidden'}}> 
                          <Text allowFontScaling={false} style={{fontSize:25, fontWeight:'bold', textAlign:'center'}}>Redes Sociales</Text>
                          <View style={{flexDirection: 'row', height: '40%', width: "100%", marginTop: 30, justifyContent: 'space-around'}}>
                            {
                              dataSheet.facebook ? (
                                <TouchableOpacity 
                                  style={styles.iconBox3}
                                  onPress={()=>{openFace(dataSheet.facebook)}}  
                                >
                                  <Image style={{ height: 30, width: 30}} source={require('../../../../assets/facebook.png')}/>
                                  <Text allowFontScaling={false} style={{fontWeight: 'bold', color: "#1778F2"}}>Facebook</Text>
                                </TouchableOpacity>
                              ):(
                                console.log("No posee facebook")
                              )
                            }
                            {
                              dataSheet.instagram ? (
                                <TouchableOpacity 
                                  style={styles.iconBox3}
                                  onPress={()=>{openInsta(dataSheet.instagram)}}  
                                >
                                  <Image style={{ height: 30, width: 30}} source={require('../../../../assets/instagram.png')}/>
                                  <Text allowFontScaling={false} style={{fontWeight: 'bold', color: '#DD2A7B'}}>Instagram</Text>
                                </TouchableOpacity>
                              ):(
                                console.log("No posee instagram")
                              )
                            }
                          </View>
                        </View>
                      ):(
                        console.log("No posee redes sociales")
                      )
                    }
  
                  </View>
                )}
              </BottomSheet>
            </View>
          </GestureHandlerRootView>
        </View>  
    )
}

const styles = StyleSheet.create({
  icon:{
    fontFamily: 'fa-solid-900'
  },
  estilo1: {
      backgroundColor: '#30a1fa',
      height: 'auto',
      alignItems: 'center',
      justifyContent: 'center',
      flexDirection: 'column',
      width: '100%'
  },
  estilo2:{
    height: Platform.OS == 'ios' ? Constants.statusBarHeight : 0,
    width: '100%'
  },
  estilo3:{
    width: '100%',
    height: 80,
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
    paddingHorizontal: 16
  },
  letra: {
      color: '#fff',
      fontSize: 20,
      fontFamily: 'Montserrat-Medium',
  },
  container: {
    flex: 1,
    backgroundColor: "white",
  },
  iconBox: {
    width: '33%',
    height: '100%',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'space-between'
  },
  iconBox2: {
    width: '100%',
    height: '33%',
    flexDirection: 'row',
    alignItems: 'center',
    paddingLeft: 30
  },
  iconBox3: {
    width: '50%',
    height: '100%',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'space-between'
  },
  sombra: {
    shadowColor: "#000",
    shadowOffset: {
    	width: 0,
    	height: 12,
    },
    shadowOpacity: 0.58,
    shadowRadius: 16.00,
    elevation: 24,
  },
  sheet: {
    flex: 1
  },
  flat: {
    width: '100%',
    marginTop: 35
  }
})