import React, { useEffect, useState, useCallback } from 'react';
import { ActivityIndicator, Alert, SafeAreaView, ScrollView, Text, TouchableOpacity, View, StyleSheet, StatusBar, Platform } from 'react-native';
import { Button, Divider, Image, Input, } from 'react-native-elements';
import * as ImagePicker from 'expo-image-picker';
import { Ionicons } from '@expo/vector-icons';
import CloseMenu from '../../../components/CloseMenu';
import { useFonts } from 'expo-font';

import Api from '../../../utils/Api';
import { url } from '../../../utils/config';
import Loader from '../../../components/Loader';
import Constants from 'expo-constants';
import { useFocusEffect, useNavigation } from '@react-navigation/native';
import { saveValue, deleteValue, getValueFor } from '../../../utils/localStorage';

export default function Perfiluser({route}) {
    const [parametros, setParametros] = useState({});
    const [infoUser, setInfoUser] = useState(null);
    const [tokenAuth, setTokenAuth] = useState(null);
    const [isVisible, setIsVisible] = useState(false);
    const [errors, setErrors] = useState();
    const navegacion = useNavigation();

    useFocusEffect(
        useCallback(()=>{
            if(Platform.OS == 'ios'){
              StatusBar.setBarStyle('light-content');
            }
        })
    );

    // Cerrar sesion
    const cerrarSesion = () => {
        Alert.alert(
        "Cerrar Sesión.",
        "¿Está seguro de cerrar sesión?",
        [
            {
            text: "Cancelar",
            style: "cancel"
            },
            {
            text: "Aceptar",
            onPress: async () => {
                let uri = "";
                let api = null;
                
                let id = await getValueFor('id');
                let tokenPush = await getValueFor('tokenPush');
                
                uri = `${url}eventos/deleteTokenByLogoff/${id}/${tokenPush}`;
                api = new Api(uri,'GET');
                api.call().then(res => {
                if(res.response){
                    console.log(res.message);
                }else{
                    console.log(res.message);
                }
                });
                
                await deleteValue("token");
                await deleteValue("id");
                navegacion.goBack();
            }
            }
        ],
        {
            cancelable: false
        }
        )
    }

    useEffect(() => {
        getValueFor("token").then(res => {
            setTokenAuth(res);
            let token = atob(res)
            let tokenDecode = JSON.parse(token);
            setInfoUser(tokenDecode.data);
            console.log(infoUser, "==============aqui  hay datos")
        });
    }, []);

    const onchange = (e, tipo) => {
        setParametros({ ...parametros, [tipo]: e.nativeEvent.text })
    }

    let uploadImage = async () => {
        const permissionResult = await ImagePicker.requestMediaLibraryPermissionsAsync();

        if (permissionResult.granted === false) {
            alert("Se requiere permiso para utilizar la cámara.");
            return;
        }
        const pickerResult = await ImagePicker.launchImageLibraryAsync();
        if (pickerResult.canceled === true) {
            return;
        }
        let localUri = pickerResult.uri;
        // console.log(localUri, "ESTE ES EL RESULTADO DEL LOCALURI");

        if (localUri) {
            Alert.alert(
                "Actualizar foto",
                "¿Desea subir la foto seleccionada?",
                [
                    {
                        text: "Cancelar",
                        style: "cancel",
                    },
                    {
                        text: "Aceptar",
                        onPress: () => { continuationProcess() }
                    }
                ],
            )
        }

        const continuationProcess = () => {
            if (localUri == null || localUri == '') {
                Alert.alert('Debe seleccionar una imagen.')
            } else {
                let filename = localUri.split('/').pop();
                let match = /\.(\w+)$/.exec(filename);
                let type = match ? `image/${match[1]}` : `image`;

                let formData = new FormData();
                formData.append('img', { uri: localUri, name: filename, type });

                fetch(`${url}image/add/4/${infoUser.idPersona}`, {
                    method: 'POST',
                    body: formData,
                    headers: {
                        'content-type': 'multipart/form-data',
                    },
                })
                    .then(response => response.json())
                    .then(data => {
                        console.log(data);
                        Alert.alert(
                            "Foto agregada correctamente.",
                            "Cierre e inicie sesión para ver los cambios.")
                    })
                    .catch(errors => {
                        console.log(errors);
                    })
            }
        }
    };

    const actualizarInfoUser = async () => {
        // console.log(tokenAuth)
        // console.log(infoUser,"==============aqui  hay datos")
        if (Object.keys(parametros).length === 0) {
            alert("No hay cambios en los datos.")
        } else {
            setIsVisible(true);
            let uri = `${url}user/updateInformationUser/${infoUser.idPersona}`
            // console.log(parametros, uri)
            let api = new Api(uri, "PUT", parametros, tokenAuth);
            await api.call().then(res => {
                // console.log(res)
                if (res.response) {
                    setIsVisible(false);
                    alert("Cierre e inicie sesión para ver los cambios...");
                } else {
                    setIsVisible(false);
                    alert("Error intentelo de nuevo más tarde.")
                }
            })

        }
    }

    const [loaded] = useFonts({
        "Montserrat-Bold": require("../../../../assets/fuentes/Montserrat-Bold.ttf"),
        "Montserrat-Italic": require("../../../../assets/fuentes/Montserrat-Italic.ttf"),
        "Montserrat-Light": require("../../../../assets/fuentes/Montserrat-Light.ttf"),
        "Montserrat-Medium": require("../../../../assets/fuentes/Montserrat-Medium.ttf"),
        "Montserrat-Regular": require("../../../../assets/fuentes/Montserrat-Regular.ttf"),
        "Montserrat-SemiBold": require("../../../../assets/fuentes/Montserrat-SemiBold.ttf"),
    });

    if (!loaded) {
        return null;
    }

    if (!infoUser) return <Loader isVisible={true} text="Cargando..." />
    return (
        <View style={{ flex: 1, backgroundColor: 'white', paddingTop: 0}}>
            <View style={styles.estilo1}>
              <View style={styles.estilo2}></View>
              <View style={styles.estilo3}>
                <CloseMenu />
                <Text style={styles.letra} allowFontScaling={false}>Perfil</Text>
              </View>
            </View>

            <ScrollView>
                <View style={styles.container}>
                    <Divider orientation='horizontal' width={10} color={'#F2F3F4'} />
                    <View style={{ paddingTop: '3%', paddingBottom: '3%', paddingLeft: '5%', }}>
                        {
                            !infoUser.urlImg ?
                            <Image
                            source={require('../../../../assets/sin_foto.png')}
                            style={styles.image}
                            PlaceholderContent={<ActivityIndicator color='#30a1fa' />}
                        >
                            <View style={{ paddingLeft: 50, paddingTop: 48, }}>
                                <TouchableOpacity onPress={() => uploadImage()} style={styles.cameraStyle}>
                                    <Ionicons name='camera' size={20} color={'white'} />
                                </TouchableOpacity>
                            </View>
                        </Image>
                        :(
                            <Image
                            source={{ uri: infoUser.urlImg }}
                            style={styles.image}
                            PlaceholderContent={<ActivityIndicator color='#30a1fa' />}
                        >
                            <View style={{ paddingLeft: 50, paddingTop: 48, }}>
                                <TouchableOpacity onPress={() => uploadImage()} style={styles.cameraStyle}>
                                    <Ionicons name='camera' size={20} color={'white'} />
                                </TouchableOpacity>
                            </View>
                        </Image>
                        )
                        }
                        
                    </View>
                    <Divider orientation='horizontal' width={10} color={'#F2F3F4'} />

                    <View>
                        <Text style={styles.textStyle} allowFontScaling={false}>Nombre</Text>
                        <Input
                            style={styles.inputStyle} allowFontScaling={false}
                            defaultValue={infoUser.Nombre}
                            selectionColor={'purple'}
                            inputContainerStyle={{ borderBottomWidth: 0, bottom: -5, paddingLeft: '3%', left: -1 }}
                            onChange={(e) => onchange(e, "nombre")}
                        />
                        <Divider orientation='horizontal' width={3} color={'#F2F3F4'} />

                        <Text style={styles.textStyle} allowFontScaling={false}>Apellido</Text>
                        <Input
                            style={styles.inputStyle} allowFontScaling={false}
                            defaultValue={infoUser.Apellidos}
                            selectionColor={'purple'}
                            inputContainerStyle={{ borderBottomWidth: 0, bottom: -5, paddingLeft: '3%', left: -1 }}
                            onChange={(e) => onchange(e, "Apellidos")}
                        />
                        <Divider orientation='horizontal' width={3} color={'#F2F3F4'} />

                        <Text style={styles.textStyle} allowFontScaling={false}>Correo electrónico</Text>
                        <Input
                            disabled
                            // rightIcon={{
                            //     name: 'chevron-right',
                            //     color: 'gray',
                            // }}
                            style={styles.inputStyle} allowFontScaling={false}
                            defaultValue={infoUser.Correo}
                            selectionColor={'purple'}
                            inputContainerStyle={{ borderBottomWidth: 0, bottom: -5, paddingLeft: '3%', }}
                            onChange={(e) => onchange(e, "correo")}
                        />
                        <Divider orientation='horizontal' width={3} color={'#F2F3F4'} />

                        <Text style={styles.textStyle} allowFontScaling={false}>Número de teléfono</Text>
                        <Input
                            disabled
                            // rightIcon={{
                            //     name: 'chevron-right',
                            //     color: 'gray',
                            // }}
                            style={styles.inputStyle} allowFontScaling={false}
                            defaultValue={infoUser.Telefono}
                            selectionColor={'purple'}
                            inputContainerStyle={{ borderBottomWidth: 0, bottom: -5, paddingLeft: '3%', }}
                            onChange={(e) => onchange(e, "telefono")}
                        />
                        <Divider orientation='horizontal' width={3} color={'#F2F3F4'} />

                        <View style={{ paddingLeft: '10%', paddingTop: '15%', paddingBottom: '15%', }}>
                            <Button
                                allowFontScaling={false}
                                containerStyle={styles.containerIngresar}
                                buttonStyle={styles.btnIngresar}
                                title="Guardar Cambios"
                                titleStyle={{ fontSize: 22, letterSpacing: -0.5750000000000001, fontFamily: 'Montserrat-Medium' }}
                                onPress={() => actualizarInfoUser()}
                            />
                            <Button
                                title="Cerrar Sesión"
                                titleStyle={{ fontSize: 22, letterSpacing: -0.5750000000000001, fontFamily: 'Montserrat-Medium' }}
                                onPress={() => cerrarSesion()}
                                containerStyle={[styles.containerIngresar, {marginTop: 20}]}
                                buttonStyle={[styles.btnIngresar, {backgroundColor: 'red'}]}
                            />
                        </View>
                    </View>
                </View>
            </ScrollView>
        </View>
    )
}

const styles = StyleSheet.create({
    estilo1: {
        backgroundColor: '#30a1fa',
        height: 'auto',
        alignItems: 'center',
        justifyContent: 'center',
        flexDirection: 'column',
        width: '100%'
    },
    estilo2:{
      height: Platform.OS == 'ios' ? Constants.statusBarHeight : 0,
      width: '100%'
    },
    estilo3:{
      width: '100%',
      height: 80,
      alignItems: 'center',
      justifyContent: 'center',
      flexDirection: 'row',
      paddingHorizontal: 16
    },
    letra: {
        color: '#fff',
        fontSize: 20,
        fontFamily: 'Montserrat-Medium',
    },
    container: {
        flex: 1,
        backgroundColor: "white",
    },
    title: {
        paddingTop: '10%',
        fontSize: 30,
        color: '#000',
    },
    image: {
        height: 80,
        width: 80,
        borderRadius: 80,
    },
    cameraStyle: {
        backgroundColor: 'purple',
        height: 30,
        width: 30,
        borderRadius: 30,
        borderColor: 'white',
        borderWidth: 2.5,
        alignItems: 'center',
        justifyContent: 'center',
    },
    containerIngresar: {
        width: "89%",
        height: 50,
        fontSize: 23,
    },
    textStyle: {
        color: 'gray',
        fontSize: 18,
        paddingTop: '3%',
        paddingLeft: '5%',
        fontFamily: 'Montserrat-Regular',
        bottom: -15
    },
    inputStyle: {
        color: '#000',
        fontSize: 18,
        marginTop: '-2%',
        fontFamily: 'Montserrat-Regular',
        bottom: -10
        
        

        
    },
    btnIngresar: {
        borderRadius: 6,
        backgroundColor: "#3498DB",
        fontSize: 23,
    },
})