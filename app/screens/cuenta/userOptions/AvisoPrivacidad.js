import { SafeAreaView, StyleSheet, Text, View, ScrollView, StatusBar, Platform } from 'react-native';
import React from 'react';
import CloseMenu from '../../../components/CloseMenu';
import { useFonts } from 'expo-font';
import Constants from 'expo-constants';
import { useFocusEffect } from '@react-navigation/native';

export default function AvisoPrivacidad() {

    useFocusEffect(
        React.useCallback(()=>{
            if(Platform.OS == 'ios'){
                StatusBar.setBarStyle('light-content');
            }
        })
    );

    const [loaded] = useFonts({
        "Montserrat-Bold": require("../../../../assets/fuentes/Montserrat-Bold.ttf"),
        "Montserrat-Italic": require("../../../../assets/fuentes/Montserrat-Italic.ttf"),
        "Montserrat-Light": require("../../../../assets/fuentes/Montserrat-Light.ttf"),
        "Montserrat-Medium": require("../../../../assets/fuentes/Montserrat-Medium.ttf"),
        "Montserrat-Regular": require("../../../../assets/fuentes/Montserrat-Regular.ttf"),
        "Montserrat-SemiBold": require("../../../../assets/fuentes/Montserrat-SemiBold.ttf"),
    });
    if (!loaded) {
        return null;
    }

    return (
        <View style={{ flex: 1}}>
        {/* <StatusBar backgroundColor={'#30a1fa'} barStyle={'light-content'}/> */}
            
            <View style={styles.estilo1}>
              <View style={styles.estilo2}></View>
              <View style={styles.estilo3}>
                <CloseMenu />
                <Text style={styles.letra} allowFontScaling={false}>Aviso de Privacidad</Text>
              </View>
            </View>

            <ScrollView style={{ width: '100%', height: '100%', zIndex: 1000 }}>
            <View style={styles.container}>
                    <View style={{ paddingTop: '3%', paddingBottom: '3%', paddingLeft: '5%', }}></View>

            <View style={{flexDirection: 'row', paddingBottom: '6%'}}>
                <Text style={styles.text} allowFontScaling={false}>
                Bienvenido a la app y servicios digitales de compras en línea de Huauchi Tour,
                            este sitio/app es propiedad de Huauchi Tour de la Sierra S.A. de C.V. con RFC
                            ESI020716BLA, ésta última con domicilio fiscal en: Hidalgo 3, Colonia Centro,
                            Huauchinango, Puebla, C.P. 73170.
                            {'\n'}{'\n'}
                            Por favor, revisa los siguientes términos y condiciones que rigen el uso del
                            sitio web y la app. Al favorecernos con su aceptación, significa que has leído,
                            entendido y aceptas en su totalidad los términos y condiciones del mismo y
                            acordado que los presentes términos y condiciones son legalmente vinculantes
                            para el “Usuario”.
                            {'\n'}{'\n'}
                            El Usuario se sujetará a los presentes términos y condiciones de Uso de  la app
                            de Huauchi Tour. Antes de utilizar Huauchi Tour, por favor lea los presentes Términos y
                            Condiciones con atención (especialmente las partes resaltadas en negritas).
                            Tenga en cuenta que el Propietario no podrá acceder, ni tener pleno acceso a los
                            Servicios hasta que no haya proporcionado todos los documentos e información que
                            se requieren, aprobados por Huauchi Tour y se obligue a sujetarse a los Términos y
                            Condiciones.
                            {'\n'}
                            Los presentes Términos y Condiciones son aplicables para todos los usuarios o
                            repartidores de Huauchi Tour y están sujetos a modificación en cualquier momento
                            como resultado de ajustes a la política comercial. El Usuario debe visitar
                            frecuentemente la aplicación y sitio web de Huauchi Tour para mantenerse al tanto
                            de los términos vigentes. Las notificaciones, términos u otros requerimientos
                            expresamente especificados o integrados en la página web y/o en la aplicación
                            en Huauchi Tour, formarán parte integral de los presentes Términos y Condiciones,
                            ciertas partes de los términos podrán ser sustituidas por avisos legales,
                            términos o requerimientos más actualizados expresamente especificados o
                            integrados en la página web y/o aplicación en Huauchi Tour.
                            Al aceptar los Términos y Condiciones, el Usuario también habrá leído con
                            atención y aceptado dichos términos sustituidos o referidos. Si el Usuario
                            sigue utilizando los servicios de la Compañía, se considerará que el Usuario
                            ha aceptado los Términos y Condiciones actualizados o el Usuario dejará de
                            utilizar Huauchi Tour inmediatamente.
                            {'\n'} {'\n'}
                            Los presentes Términos y Condiciones expresamente dejan sin efecto cualquier
                            acuerdo o arreglo previo que la Compañía haya celebrado con el Usuario para
                            efectos de utilizar Huauchi Tour y los Servicios. Huauchi Tour podrá dar por
                            terminados los presentes Términos y Condiciones o cualquier Servicio con
                            respecto al Usuario inmediatamente, o en general, dejar de ofrecer o negar
                            el acceso a los Servicios o a cualquier parte de los mismos, si Huauchi Tour
                            considera que el Usuario ha incurrido en cualquier incumplimiento.
                            {'\n'} {'\n'}
                            Huauchi Tour se compromete a no transferir a un tercero sus datos personales,
                            asimismo, para garantizar la protección y confidencialidad de los
                            Datos Personales ha establecido mecanismos de seguridad administrativos,
                            técnicos o físicos. Tampoco recabará datos personales sensibles para poder
                            llevar a cabo las finalidades que se mencionan en los presentes
                            Términos y Condiciones.
                            {'\n'} {'\n'}
                            Asimismo, Huauchi Tour se reserva el derecho de negarse a prestar los
                            Servicios al Usuario o negar al Usuario el uso de páginas web, servicios o
                            aplicaciones de Huauchi Tour sin causa alguna.
                            {'\n'}{'\n'}{'\n'}
                            <Text style={styles.title}>
                                - REGISTRO A LA APLICACIÓN COMO USUARIOS
                                {'\n'}{'\n'}
                            </Text>
                            ⚫︎ Para utilizar el servicio en línea de pedidos, el Usuario debe descargar
                            la app de Huauchi Tour, instalarla en su dispositivo móvil y completar
                            exitosamente los procesos de registro.
                            {'\n'}
                            ⚫︎ El Usuario deberá reconocer que los datos proporcionados en el registro
                            son verdaderos, para en dado caso de olvido se pueda hacer una recuperación
                            de los datos.
                            {'\n'}{'\n'}
                            <Text style={styles.title}>
                                - USO DE LA APP
                                {'\n'}{'\n'}
                            </Text>
                            Los contenidos y servicios que ofrecemos están dirigidos a un público mayor
                            de 18 años. Por lo que queda bajo responsabilidad de los padres o tutores,
                            supervisar la conducta de los menores de edad que ingresen a los sitios.
                            Huauchi Tour y filiales podrá cancelar o restringir tu cuenta en dado caso que
                            detecte algún uso indebido del sitio y de los servicios que se ofrezcan en el
                            mismo, entendiendo eso como uso indebido. Que en este caso sería como:
                            {'\n'}{'\n'}
                            ⚫︎ Utilizar los servicios para revender productos y cualquier otro uso comercial
                            de la app y sitios web de los contenidos.
                            {'\n'}
                            ⚫︎ La utilización de mecanismos o herramientas automatizadas o tecnología
                            similar cuya finalidad sea realizar la extracción, obtención o recopilación,
                            directa o indirecta, de cualquier información contenida en el sitio.
                            {'\n'}
                            ⚫︎ Cualquier intento de modificación, adaptación, traducción, o conversión de
                            los formatos o programas de cómputo de los Sitios web o de los contenidos de
                            los mismos.
                            {'\n'}
                            ⚫︎ Utilizar los códigos HTML a disposición de un tercero.
                            {'\n'}
                            ⚫︎ Copiar, imitar, replicar para su uso en servidores espejo, reproducir,
                            distribuir, publicar, descargar, mostrar o transmitir cualquier Contenido del
                            Sitio (incluyendo marcas registradas) en cualquier forma o por cualquier medio;
                            esta restricción incluye, pero no se limita a los siguientes medios:
                            medios electrónicos, mecánicos, medios de fotocopiado, grabación o
                            cualquier otro medio.
                            {'\n'}
                            ⚫︎ Acceder a datos no destinados al usuario o iniciar sesión en un servidor
                            o cuenta en la que el usuario no esté autorizado su acceso.
                            {'\n'}
                            ⚫︎ Intentar interferir con el servicio a cualquier usuario, huésped o red,
                            incluyendo, sin limitación, a través del envío de virus al Sitio,
                            sobrecarga, inundación, spam, bombardeo de correo o fallas.
                            {'\n'}
                            ⚫︎ Enviar correo no solicitado, incluyendo promociones y/o publicidad de
                            productos o servicios.
                            {'\n'}
                            ⚫︎ Intento o realización de actividades fraudulentas entre las que se encuentran
                            sin limitar, la falsificación de identidades o formas de pago.
                            {'\n'}{'\n'}
                            Toda la información de registro y facturación proporcionada deberá ser
                            verdadera y exacta. Proporcionar cualquier información falsa o inexacta
                            constituye el incumplimiento de estos TÉRMINOS Y CONDICIONES.
                            Al confirmar tu compra y finalizar el proceso de pago, estás de acuerdo en
                            aceptar y pagar por los artículos solicitados, así como en los datos de
                            facturación proporcionados a Huauchi Tour y su sitio web.
                            {'\n'}
                            Las violaciones al sistema o red de seguridad pueden dar lugar a
                            responsabilidad civil o penal, Huauchi Tour S.A de C.V investigará las
                            ocurrencias que puedan involucrar tales violaciones, así como cooperar
                            con las autoridades en la persecución de usuarios que infrinjan tales
                            violaciones. Haciendo uso de estos sitios, te obligas a no utilizar
                            ningún dispositivo, o software para interferir o intentar interferir
                            con el uso correcto de estos sitios o cualquier actividad llevada a cabo
                            en los mismos.
                            {'\n'}{'\n'}
                            <Text style={styles.title}>
                                - INFORMACIÓN SOBRE LOS PRODUCTOS
                                {'\n'}{'\n'}
                            </Text>
                            La información de los productos anunciados en el sitio de Huauchi Tour es
                            exacta y apegada a las características de los mismos.
                            {'\n'}
                            Exhibimos con precisión los colores que aparecen de nuestros productos en
                            los sitios. Sin embargo, los colores actuales que ves dependerán de tu
                            pantalla, no podemos garantizar que ninguno de los colores mostrados en tu
                            pantalla sea exacto, te recordamos que las imágenes o fotografías que nos
                            proporcionan los proveedores, así como las de los “sellers” autorizados,
                            son con fines ilustrativos y te son expuestas de modo orientativo.
                            Te recomendamos que siempre verifiques las condiciones de compra antes de
                            realizar tu pedido, a fin de cerciorarte de los términos, condiciones y
                            restricciones que pudieren aplicar a cada producto.
                            {'\n'}
                            Todos los precios de los productos incluyen el IVA y demás impuestos
                            que pudieran corresponderles. Lo anterior, independientemente de los
                            gastos de envío que pudieran o no generarse. Estos montos se te indicarán
                            antes de levantar tu pedido, para que estés en posibilidad de aceptarlos.
                            {'\n'}
                            El formato en que se muestran los precios dentro del portal de Huauchi Tour
                            puede verse modificado por las versiones de navegador, sistema operativo o
                            dispositivos para navegación de internet, por lo cual puede existir variaciones
                            en cuanto al uso de diversos dispositivos.
                            {'\n'}
                            De presentarse alguna inconsistencia o fallas al momento de publicar los
                            precios en el portal, se te informará de dicha situación, para que confirmes
                            si aún es tu deseo la adquisición del producto, o bien cancelar el pedido y
                            devolver el importe pagado. En caso de que no hayas finalizado tu compra,
                            se te notificará el cambio en precio y disponibilidad al ingresar al carrito
                            de compra.
                            {'\n'}{'\n'}
                            <Text style={styles.title}>
                                - GARANTIA
                                {'\n'}{'\n'}
                            </Text>
                            Huauchi Tour busca que los artículos que se ofrecen estén respaldados con garantía
                            del fabricante, la cual siempre se apega a las disposiciones y términos
                            establecidos en Ley Federal de Protección al Consumidor.
                            La garantía se entregará con tu producto en donde se establecerán las
                            condiciones y mecanismos para hacerlas efectivas.
                            {'\n'}
                            El cumplimiento de las garantías de los productos, serán brindada
                            directamente por los “sellers” autorizados, la vigencia de dichas garantías
                            se refiere en la descripción de cada producto siendo emitida y validada
                            por los “sellers” de cada producto, por lo que no asume responsabilidad
                            alguna en torno a dichas garantías y el cliente se obliga a contactar
                            directamente al seller para hacer efectiva dicha garantía, no obstante,
                            realizará las acciones necesarias para proporcionar a los clientes que
                            así lo soliciten los datos de contacto para validar las respectivas
                            garantías, si fuera necesario.
                            {'\n'}
                            Es responsabilidad del cliente revisar la mercancía al momento de la
                            recepción. Si el empaque o producto se encuentra rayado, con golpes,
                            sucio, mojado o con otra forma de daño.
                            {'\n'}
                            Sugerimos:
                            {'\n'}
                            ⚫︎ No lo recibas y repórtelo al área de atención a clientes
                            {'\n'}
                            ⚫︎ Validaremos envío de sustituto o aplicar devolución.
                            {'\n'}
                            ⚫︎ Al recibir el artículo deberá firmar de recibido y de conformidad.
                            {'\n'}{'\n'}
                            <Text style={styles.title}>
                                - LÍMITES DE CANTIDAD Y DISPONIBILIDAD
                                {'\n'}{'\n'}
                            </Text>
                            Te informamos que, con la finalidad de beneficiar a una mayor cantidad
                            de clientes, la venta de algunos artículos de alta demanda podrá ser
                            limitada a determinadas piezas, esta situación te será comunicada antes
                            de realizar tu pedido y concretar tu compra. Igualmente el precio mínimo
                            para poder hacer un pedido es de 150$ para poder llevarse a domicilio.
                            {'\n'}
                            Los artículos están sujetos a disponibilidad, del cual el Cliente deberá
                            de verificar antes de procesar el pago. Lo anterior, es con la intención
                            de validar la existencia de los artículos y en caso de no estar disponible,
                            el sistema mostrará la siguiente leyenda “Sin productos disponibles para
                            tu dirección." El límite para poder hacer la entrega a domicilio es de
                            4km en la que se puede entregar el pedido.
                            {'\n'}{'\n'}
                            <Text style={styles.title}>
                                -ACEPTACIÓN DE PEDIDO
                                {'\n'}{'\n'}
                            </Text>
                            Una vez que realices tu pedido se te enviará notificación por medio de
                            la app y podrás dar seguimiento a dicho pedido, lo anterior no significa
                            que el pago ha sido procesado o autorizado por el banco o medios de pago
                            que elegiste, ni que el pedido ha sido confirmado.
                            Si el pago es autorizado o autenticado te enviaremos otro correo
                            electrónico con la confirmación del pedido y los detalles de la compra
                            o en su defecto se realizará el pago a contra-entrega del pedido.
                            {'\n'}{'\n'}
                            <Text style={styles.title}>
                                -DEVOLUCIONES
                                {'\n'}{'\n'}
                            </Text>
                            Si no estás satisfecho con el producto, puedes solicitar un devolución
                            o comprar otro producto con el mismo costo (no se aceptan productos ya
                            abiertos o con signo de haber intentado hacerlo, bebidas alcohólicas,
                            alimentos enlatados).
                            {'\n'}{'\n'}
                            <Text style={styles.title}>
                                -CANCELACIONES
                                {'\n'}{'\n'}
                            </Text>
                            Tu pedido comienza a procesarse inmediatamente después del clic en Comprar.
                            Una vez que el pedido sea confirmado por nuestro sistema, no será posible
                            cancelarse y deberá tramitarse como una devolución.
                            Para realizar una devolución de tu pedido solicítala en el establecimiento,
                            deberás tener a la mano tu número de pedido y en caso de una cancelación
                            parcial, el detalle de los artículos a cancelar.
                            {'\n'}{'\n'}
                            <Text style={styles.title}>
                                -DISPOSICIONES
                                {'\n'}{'\n'}
                            </Text>
                            ⚫︎ Si se declara la nulidad de ciertos términos de los ya presentes Términos
                            y Condiciones, pero los demás términos pueden seguir siendo válidos y su
                            exigibilidad no se ve afectada, Huauchi Tour determinará si continuará
                            cumpliendo o no con tales otros términos.
                            {'\n'}
                            ⚫︎ Huauchi Tour podrá entregar una notificación publicando una notificación
                            general en su sitio web y/o en Huauchi Tour o enviando un correo electrónico
                            o mensaje de texto a la dirección de correo electrónico o número de teléfono
                            móvil registrado en la información de la cuenta del Usuario.
                            Las notificaciones, que podrán publicarse de tiempo en tiempo, constituirán
                            parte de los presentes Términos y Condiciones.
                            {'\n'}
                            ⚫︎ El Usuario no cederá ninguno de los derechos conforme a los presentes
                            Términos y Condiciones sin el previo consentimiento por escrito de
                            Huauchi Tour.
                            {'\n'}{'\n'}
                            <Text style={styles.title}>
                                -OTROS TÉRMINOS
                                {'\n'}{'\n'}
                            </Text>
                            Los Términos y Condiciones hacen referencia a los siguientes términos
                            adicionales, los cuales también serán aplicables al uso de la app,
                            contenido, productos, Servicios y aplicaciones de la Compañía por el
                            Usuario, mismos que, al utilizarlos, el Usuario se obliga a cumplir:
                            {'\n'}
                            ⚫︎ Aviso de Privacidad de Huauchi Tour establece los términos conforme a
                            los cuales los datos personales y otra información recopilada o
                            proporcionada por el Usuario deberá ser tratada.
                            {'\n'}{'\n'}
                            <Text style={styles.title}>
                                - Ley Aplicable
                                {'\n'}{'\n'}
                            </Text>
                            Los presentes Términos y Condiciones se regirán por las leyes aplicables
                            en México. Cualquier conflicto, reclamación o controversia derivada de o
                            relacionada con el incumplimiento, terminación, cumplimiento,
                            interpretación o validez de los presentes Términos y Condiciones o el uso
                            de nuestro sitio web o Huauchi Tour, se someterá a la jurisdicción de un
                            tribunal competente de la Ciudad de Huauchinango, México y las partes en
                            este acto expresa e irrevocablemente renuncian a cualquier otra
                            jurisdicción que pueda corresponderles en virtud de sus domicilios
                            respectivos, presentes o futuros.
                            {'\n'}{'\n'}
                            <Text style={styles.title}>
                                - Subsistencia
                                {'\n'}{'\n'}
                            </Text>
                            Aún cuando los presentes Términos y Condiciones den por terminados o
                            anulen, las disposiciones relacionadas con la responsabilidad por
                            incumplimiento, propiedad industrial, obligación de confidencialidad del
                            Pasajero, ley aplicable y jurisdicción subsistirán.     
                </Text>
                
            </View>
            </View>
            </ScrollView>
        </View>
      
    )
}

const styles = StyleSheet.create({
    estilo1: {
        backgroundColor: '#30a1fa',
        height: 'auto',
        alignItems: 'center',
        justifyContent: 'center',
        flexDirection: 'column',
        width: '100%'
    },
    estilo2:{
      height: Platform.OS == 'ios' ? Constants.statusBarHeight : 0,
      width: '100%'
    },
    estilo3:{
      width: '100%',
      height: 80,
      alignItems: 'center',
      justifyContent: 'center',
      flexDirection: 'row',
      paddingHorizontal: 16
    },
    letra: {
        color: '#fff',
        fontSize: 20,
        fontFamily: 'Montserrat-Medium',
    },

    container: {
        flex: 1,
        backgroundColor: "white",
    },

    text: {
        fontSize: 18,
        textAlign: 'justify',
        lineHeight: 25,
        paddingTop: '5%',
        paddingLeft: '7%',
        paddingRight: '7%',
    },



})