import React, { useState, useCallback } from 'react';
import { ActivityIndicator, Linking, Modal, Pressable, SafeAreaView, ScrollView, StyleSheet, Text, View, StatusBar, Platform} from 'react-native';
import { Image, } from 'react-native-elements';
import { Ionicons } from '@expo/vector-icons';
import { useNavigation, useFocusEffect } from '@react-navigation/native';
import MapView, { Marker } from 'react-native-maps';
import { useFonts } from 'expo-font';
import Constants from 'expo-constants';

import Api from '../../../utils/Api';
import { url } from '../../../utils/config';

const Item = ({ descripcion }) => (
  <View>
    <Text style={styles.text} allowFontScaling={false}>
      {descripcion}
    </Text>
  </View>
);

export default function EstablishmentInformation({ route }) {
  const { id } = route.params;

  const navegacion = useNavigation();
  const [detalle, setDetalle] = useState([]);
  const [show, setShow] = useState(false);
  const [error, setError] = useState(null);

  useFocusEffect(
    useCallback(() => {
      if(Platform.OS == 'ios'){
        StatusBar.setBarStyle('dark-content');
      }
      
      (async () => {

        let uri = `${url}establishment/detailEstablishment/${id}`;
        // console.log(uri,"aqui hay una ruta");
        let api = new Api(uri, "GET");
        api.call().then(res => {
          if (res.response) {
            setDetalle(res.result);
            console.log(res.result);
          } else {
            setError(res.errors);
            console.log(res.errors);
          }
        });
      })()
    }, [])
  )

  const [loaded] = useFonts({
    "Montserrat-Bold": require("../../../../assets/fuentes/Montserrat-Bold.ttf"),
    "Montserrat-Italic": require("../../../../assets/fuentes/Montserrat-Italic.ttf"),
    "Montserrat-Light": require("../../../../assets/fuentes/Montserrat-Light.ttf"),
    "Montserrat-Medium": require("../../../../assets/fuentes/Montserrat-Medium.ttf"),
    "Montserrat-Regular": require("../../../../assets/fuentes/Montserrat-Regular.ttf"),
    "Montserrat-SemiBold": require("../../../../assets/fuentes/Montserrat-SemiBold.ttf"),
  });

  if (!loaded) {
    return null;
  }

  const ShowMap = () => {
    show == false ? (
      setShow(true)
    ) : (
      setShow(false)
    )
  }

  const mapLatitud = parseFloat(detalle.latitud);
  console.log(mapLatitud);
  const mapLongitud = parseFloat(detalle.longitud);
  console.log(mapLongitud);

  const colors = [
    '#DC0072',
    '#B700DC',
    '#1486D8',
    '#35C3D9',
    '#92CE2E',
    '#FBDE00'
  ];


  return (
    <View style={{flex: 1, paddingTop: Platform.OS == 'ios' ? - Constants.statusBarHeight : 0, backgroundColor: '#fff'}} overScrollMode='never'>
      <ScrollView style={styles.scrollContainer}>
        <View style={{ backgroundColor: "#000" }}>
          <Image
            source={detalle.urlImg ? { uri: detalle.urlImg } : require('../../../../assets/notUrl.png')}
            style={{ width: '100%', height: 315, }}
            resizeMode="cover"
            PlaceholderContent={<ActivityIndicator size="large" color="black" />}>
            <Ionicons
              name="arrow-back-circle"
              size={43}
              style={{ color: 'white', marginStart: '3%', marginTop: Platform.OS == 'ios' ? '15%': "5%"}}
              onPress={() => navegacion.goBack()}
            />
          </Image>
        </View>

        <View style={styles.firstDivision}>
          <Image
            source={detalle.urlImgPerfil ? { uri: detalle.urlImgPerfil } : require('../../../../assets/notUrl.png')}
            style={styles.logo}
            resizeMode="cover"
            PlaceholderContent={<ActivityIndicator size="large" color="black" />}
          />
          <View style={{ paddingStart: '5%', paddingLeft: 15, width: '80%'}}>
            <Text style={{ fontFamily: 'Montserrat-Bold', fontSize: 18, }} allowFontScaling={false}>
              {detalle.nombre}
            </Text>
            <Text style={{ fontSize: 15, fontFamily: 'Montserrat-Regular', width: "95%" }} allowFontScaling={false}>
              {detalle.descripcion}
            </Text>
            <View>
              {
                detalle.calificacion == 0 ?
                  <Text style={{ color: 'black', fontSize: 16, lineHeight: 50, fontFamily: 'Montserrat-Regular' }} allowFontScaling={false}>
                    <Ionicons name='star' size={19} color={'#FED554'} /> 5
                  </Text>
                  : (
                    <Text style={{ color: 'black', fontSize: 16, lineHeight: 40, fontFamily: 'Montserrat-Regular' }} allowFontScaling={false}>
                      <Ionicons name='star' size={19} color={'#FED554'} /> {detalle.calificacion}
                    </Text>
                  )
              }
            </View>
          </View>
        </View>

        <View style={{ height: 'auto', paddingVertical: 20, width: '100%', paddingHorizontal: "6%"}}>
          <Text style={styles.informacion} allowFontScaling={false}>Información</Text>
          <Text style={{ fontSize: 14, textAlign: 'justify', paddingEnd: '-1%', fontFamily: 'Montserrat-Regular'}} allowFontScaling={false}>
            {detalle.info}
          </Text>
        </View>

        <View>
          {
            detalle.direccion ?
              <View style={{ paddingStart: '5%', paddingTop: '5%', paddingEnd: 38, flexDirection: 'row', }}>
                <Ionicons name="location-outline" size={23}/>
                <Pressable
                  style={{ backgroundColor: "#FFF", }}
                  onPress={() => setShow(!show)}
                >
                  <View style={{ flexDirection: 'row',}} >
                    <Text style={styles.textDirection } allowFontScaling={false}>
                      {detalle.direccion} {'\n'}
                    </Text>

                    <View >
                      {
                        show == false ? (
                          <Ionicons
                            name="caret-down-outline"
                            size={22}
                            style={{ color: '#383B36', }}
                            onPress={() => ShowMap()}
                          />) : (
                          <Ionicons
                            name="caret-up-outline"
                            size={22}
                            style={{ color: '#383B36', }}
                            onPress={() => ShowMap()}
                          />
                        )
                      }
                    </View>
                  </View>
                </Pressable>
              </View>
              : (
                <View style={{ paddingStart: '6%', paddingTop: '5%', paddingEnd: 38, flexDirection: 'row', }}>
                  <Ionicons name="location-outline" size={23} />
                </View>
              )
          }
        </View>

        <View >
          {
            show == true ? 
              <View style={styles.modalContainer}>
                <View style={styles.modalView}>
                     <MapView loadingEnabled={true} style={{ width: '100%', height: 200, }} region={{ latitude: mapLatitud || 20.17503, longitude: mapLongitud || -98.05180, latitudeDelta: 0, longitudeDelta: 0.01, }}>
                     <Marker
                       coordinate={{
                         latitude: mapLatitud || 20.17503,
                         longitude: mapLongitud || -98.05180,
                       }}
                         title={detalle.nombre}
                         description={detalle.direccion}
                         image={require('../../../../assets/sombra.png')}
                     />
                   </MapView>
                </View>
              </View>
              : (
                (null)
              )
          }
        </View>

        <View>
          {
            detalle.telefono ?
              <View style={styles.phone}>
                <Ionicons name="call" size={20} allowFontScaling={false}
                  onPress={() => { Linking.openURL(`tel:${detalle.telefono}`); }}> </Ionicons>
                <Text style={{ fontSize: 16, fontFamily: 'Montserrat-Regular' }} allowFontScaling={false}
                  onPress={() => { Linking.openURL(`tel:${detalle.telefono}`); }}> {detalle.telefono}</Text>
              </View>
              : (
                <View style={styles.phone}>
                  <Ionicons name="call" size={23} allowFontScaling={false}></Ionicons>
                </View>
              )
          }
        </View>

        <View style={styles.promotion}>
          <Text style={styles.promotionsText} allowFontScaling={false}>Promociones</Text>
          {
            detalle.promos == null ? (
              <View style={{ width: '100%', height: '100%', justifyContent: 'center' }} >
                <ActivityIndicator size="large" color="black" />
              </View>
            ) : (
              detalle.promos.map((item, i) => (
                i < 5 ? (
                  <View style={{ flexDirection: 'row' }}>
                    <View style={{ width: 10, height: 10, borderRadius: 13, backgroundColor: colors[i % colors.length], marginTop: '2.5%' }}
                      PlaceholderContent={<ActivityIndicator size="large" color="black" />} >
                    </View>
                    <View>
                      <Item key={i}
                        descripcion={item.descripcion}
                      />
                    </View>
                  </View>
                ) : null
              ))
            )
          }

        </View>
        <View style={styles.endContent}>
          <Text style={styles.endText} allowFontScaling={false}>
            *Aplica sólo para los clientes que presenten el Huauchi Pass con su membresía mensual vigente.
          </Text>
        </View>
      </ScrollView>
    </View>
  )
}

const styles = StyleSheet.create({
  scrollContainer: {
    backgroundColor: '#fff',
    height: '100%',
    width: '100%',
  },
  firstDivision: {
    paddingTop: '5%',
    alignSelf: 'center',
    width: '90%',
    flexDirection: 'row',
    paddingBottom: '3%',
    borderBottomColor: '#E3E0DE',
    borderBottomWidth: 1
  },
  logo: {
    width: 80,
    height: 80,
    borderRadius: 40,
  },
  informacion: {
    color: 'black',
    fontSize: 20,
    paddingBottom: '5%',
    fontFamily: 'Montserrat-Bold'
  },
  textDirection: {
    fontSize: 16,
    paddingStart: '3%',
    textAlign: 'center',
    fontFamily: 'Montserrat-Regular',
  },
  modalContainer: {
    flex: 2,
    height: '100%',
    width: '100%',
    paddingTop: '6%',
  },
  modalView: {
    elevation: 7,
  },
  phone: {
    alignSelf: 'center',
    width: '90%',
    paddingTop: '5%',
    paddingEnd: '13%',
    flexDirection: 'row',
    paddingBottom: '8%',
    borderBottomColor: '#E3E0DE',
    borderBottomWidth: 1
  },
  promotion: {
    alignSelf: 'center',
    width: '90%',
    paddingTop: '7%',
    paddingBottom: '5%',
    borderBottomColor: '#E3E0DE',
    borderBottomWidth: 1
  },
  promotionsText: {
    color: 'black',
    fontSize: 20,
    paddingBottom: '7%',
    fontFamily: 'Montserrat-Bold',
  },
  text: {
    fontSize: 18,
    textAlign: 'justify',
    paddingStart: '3%',
    paddingBottom: 18,
    paddingEnd: '13%',
    fontFamily: 'Montserrat-Regular',
  },
  endContent: {
    borderBottomColor: '#E3E0DE',
    height: 150,
    width: '100%',
    justifyContent: 'center'
  },
  endText: {
    fontSize: 14,
    textAlign: 'justify',
    color: '#6E7274',
    fontFamily: 'Montserrat-Regular',
    width: '90%',
    alignSelf: 'center'
  },
  loaderStyles: {
    width: "100%",
    height: "100%",
    alignItems: "center",
    justifyContent: "center",
  },
  nullStyles: {
    width: "100%",
    height: "100%",
    alignItems: "center",
    justifyContent: "center",
  },
})