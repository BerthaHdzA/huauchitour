import React from 'react';
import { SafeAreaView, StyleSheet, Text, View,ImageBackground, StatusBar, Platform } from 'react-native';
import CloseMenu from '../../../components/CloseMenu';
import { useFonts } from 'expo-font';
import { Icon } from 'react-native-elements'
import Constants from 'expo-constants';
import { useFocusEffect } from '@react-navigation/native';

export default function Contactanos() {

    useFocusEffect(
        React.useCallback(()=>{
            if(Platform.OS == 'ios'){
                StatusBar.setBarStyle('light-content');
            }
        })
    );

    const [loaded]=useFonts({
        "Montserrat-Bold": require("../../../../assets/fuentes/Montserrat-Bold.ttf"),
        "Montserrat-Italic": require("../../../../assets/fuentes/Montserrat-Italic.ttf"),
        "Montserrat-Light": require("../../../../assets/fuentes/Montserrat-Light.ttf"),
        "Montserrat-Medium": require("../../../../assets/fuentes/Montserrat-Medium.ttf"),
        "Montserrat-Regular": require("../../../../assets/fuentes/Montserrat-Regular.ttf"),
        "Montserrat-SemiBold": require("../../../../assets/fuentes/Montserrat-SemiBold.ttf"),
      });
    if (!loaded) {
        return null;
    }

    return (
        <View style={{ flex: 1}}>
        {/* <StatusBar backgroundColor={'#30a1fa'} barStyle={'light-content'}/> */}
            <View style={styles.estilo1}>
              <View style={styles.estilo2}></View>
              <View style={styles.estilo3}>
                <CloseMenu />
                <Text style={styles.letra} allowFontScaling={false}>Contáctanos</Text>
              </View>
            </View>


            <View style={styles.container}>
               
                <ImageBackground source={require("../../../../assets/fondo_contactos.png")} resizeMode="cover" style={styles.image}>      
                  <View style={{ flexDirection: 'column', bottom:10, alignItems: 'center', justifyContent: 'center',marginTop:"-80%"}}>
                      <Icon name='email'  size={45} />
                      <Text style={{ fontWeight: 'bold', fontSize: 22, marginTop:"2%" }}> hola@huauchitour.com</Text>
                  </View>

                  <View style={{ flexDirection: 'column', bottom:10, alignItems: 'center', justifyContent: 'center',marginTop:"5%"}}>
                    <Icon name='facebook' size={45} />
                    <Text style={{ fontWeight: 'bold', fontSize: 22, marginTop:"2%" }}> Huauchi Tour</Text>
                  </View>

                  <View style={{ flexDirection: 'column', bottom:10, alignItems: 'center', justifyContent: 'center',marginTop:"5%"}}>
                      <Icon name='image' size={45} />
                      <Text style={{ fontWeight: 'bold', fontSize: 22,marginTop:"2%" }}> @huauchitour</Text>
                  </View>

                  <View style={{ flexDirection: 'column', bottom:10, alignItems: 'center', justifyContent: 'center',marginTop:"5%"}}>
                      <Icon name='language' size={45} />
                      <Text style={{ fontWeight: 'bold', fontSize: 22,marginTop:"2%" }}> huauchitour.com</Text>
                  </View>
               </ImageBackground>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    estilo1: {
      backgroundColor: '#30a1fa',
      height: 'auto',
      alignItems: 'center',
      justifyContent: 'center',
      flexDirection: 'column',
      width: '100%'
    },
    estilo2:{
      height: Platform.OS == 'ios' ? Constants.statusBarHeight : 0,
      width: '100%'
    },
    estilo3:{
      width: '100%',
      height: 80,
      alignItems: 'center',
      justifyContent: 'center',
      flexDirection: 'row',
      paddingHorizontal: 16
    },
    letra: {
      color: '#fff',
      fontSize: 20,
      fontFamily: 'Montserrat-Medium',
    },
    container: {
      flex: 1,
    },
    image: {
      flex: 1,
      justifyContent: "center",
      marginTop: "50%"
    },
    text: {
      color: "white",
      fontSize: 42,
      lineHeight: 84,
      fontWeight: "bold",
      textAlign: "center",
      backgroundColor: "#000000c0"
    }
})