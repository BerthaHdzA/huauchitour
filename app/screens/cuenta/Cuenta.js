import React,{ useState, useCallback, useRef }from 'react'
import { SafeAreaView, Text, View, StyleSheet, Image, Dimensions,  ScrollView, Alert,TouchableOpacity, StatusBar, Platform } from 'react-native'
import { Button, ListItem,Input, Icon } from 'react-native-elements';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import { useNavigation, useFocusEffect } from '@react-navigation/native';
import { getValueFor,saveValue } from "../../utils/localStorage.js";
import { Ionicons } from '@expo/vector-icons';
import { useFonts } from 'expo-font';

import Modal from "react-native-modal";

import { deleteValue } from '../../utils/localStorage';
import {AuthContext} from '../../utils/context';

import Loader from '../../components/Loader';
import Api from '../../utils/Api.js';
import {url} from '../../utils/config.js';
import Constants from 'expo-constants';

const Cuenta = (props) => {
  const [visible, setVisible] = React.useState(false);
  const navegacion = useNavigation();
  
  //Eliminar Cuenta
  const[moment2, setMoment2] = useState("validation");
  const[verPassword, setVerPassword] = useState(false);
  const [data, setData] = React.useState({
      user:"",
      password:""
  });
  const { setLogin } = props;
  
  const message = useRef("");

  const gurdarValor = (name,value) => {
      setData({...data,[name]:value})
  }
  
  useFocusEffect(
    useCallback(()=>{
      if(Platform.OS == 'ios'){
        StatusBar.setBarStyle('light-content');
      }
    })
  );

  //Confirmación para poder eliminar cuenta.
  const confirmarContra = async()=>{
    let uri = "";
    let api = null;
    
    let id = await getValueFor('id');

    uri = `${url}auth/checkPassword/${id}/${data.password}`;
    api = new Api(uri,'GET');
    api.call().then(res => {
      // console.log(uri);
      if(res.response){
          Alert.alert(
            "Eliminar cuenta.",
            "¿Estás seguro de querer eliminar tu cuenta?. Esta es una acción permanente, así que tómatelo con calma",
            [
              {
                text:"Cancelar",
                style: "cancel"
              },
              { 
                text: "Aceptar", 
                onPress: async () => {
                  
                  uri = `${url}auth/removalRequest/${id}`;
                  api = new Api(uri,'POST');
                  await api.call().then(res => {
                    if(res.response){
                      message.current = res.message;
                      setMoment2('message');
                    }else{
                      alert(res.message);
                    }
                  })
                } 
              }
            ],
            {
                cancelable:false
            }
          )
        }else{
          if (data.password == "" || data.password == null) {
              alert("Contraseña incorrecta");
          } else {
              alert(res.message);
          }
      }
    })
  }

  //funcion para abrir y cerrar el popup
  const abrirCerrar = () => {
    if(visible){
      setVisible(false);
    }else{
      setVisible(true)
    }
  }

  const listPerfil = [
    {
      title: 'Perfil',
      onPress: () => navegacion.navigate('Perfil')
    },
    {
      title: 'Suscripción Huauchi Pass',
      onPress: () => navegacion.navigate('membershipPass')
    },
    {
      title: 'Centros autorizados',
      onPress: () => navegacion.navigate('centrosAutorizados')
    },
    {
      title: 'Eliminar cuenta',
      onPress: () => abrirCerrar()
    }
  ]


  const listAyuda = [
    {
      title: 'Términos y Condiciones',
      onPress: () => navegacion.navigate('terminosCondiciones')
    },
    {
      title: 'Aviso de privacidad',
      onPress: () => navegacion.navigate('avisoPrivacidad')
    },
    {
      title: 'Contáctanos',
      onPress: () => navegacion.navigate('contactanos')
    }
  ];

  const [loaded] = useFonts({
    "Montserrat-Bold": require("../../../assets/fuentes/Montserrat-Bold.ttf"),
    "Montserrat-Italic": require("../../../assets/fuentes/Montserrat-Italic.ttf"),
    "Montserrat-Light": require("../../../assets/fuentes/Montserrat-Light.ttf"),
    "Montserrat-Medium": require("../../../assets/fuentes/Montserrat-Medium.ttf"),
    "Montserrat-Regular": require("../../../assets/fuentes/Montserrat-Regular.ttf"),
    "Montserrat-SemiBold": require("../../../assets/fuentes/Montserrat-SemiBold.ttf"),
  });


  if (!loaded) {
    return null;
  }


  return (
    <View style={{ flex: 1, backgroundColor: "white"}}>
      <View style={styles.estilo1}>
        <View style={styles.estilo2}></View>
        <View style={styles.estilo3}>
          <Text style={styles.letra} allowFontScaling={false}>Mi cuenta</Text>
          <View style={{ display: 'flex', alignItems: 'center', position: 'absolute', alignSelf:'center', margin:'20%'}}>
            <View style={styles.contenedor}>
              <Image
                source={require('../../../assets/htlogo.png')}
                style={{ width: "80%", height: "80%", resizeMode: 'cover' }}
              />
            </View>
          </View>
        </View>
      </View>
      <ScrollView style={{marginTop: 70, height: "81%"}} overScrollMode={'never'} keyboardShouldPersistTaps='handled'>
        <View>
          <Text style={{ color: "#a4a4a4", fontSize: 18, paddingLeft: 13, fontFamily: 'Montserrat-Medium' }} allowFontScaling={false}>Mi cuenta</Text>
          {
            listPerfil.map((item, i) => (
              <ListItem key={i} onPress={item.onPress}>
                <ListItem.Content>
                  <ListItem.Title style={styles.titleItem} allowFontScaling={false}>{item.title}</ListItem.Title>
                </ListItem.Content>
                <ListItem.Chevron iconStyle={{ fontSize: 25 }} />
              </ListItem>

            ))
          }
        </View>
        
        <Modal
          isVisible={visible}
          animationIn={"slideInRight"}
          animationInTiming={700}
          animationOut={"slideOutRight"}
          onBackdropPress={abrirCerrar}
          backdropTransitionInTiming={1}
          deviceHeight={Dimensions.get("window").height}
          deviceWidth={Dimensions.get("window").width}
          backdropOpacity={.5}
          statusBarTranslucent={true}
        >
          <View style={styles.eliminarCuentaC}>

            <ScrollView style={styles.scroll} overScrollMode="never" keyboardShouldPersisTaps={'handled'} contentContainerStyle={{height: '100%'}}>
              {
                moment2 == "validation" ?
                <View style={styles.datosC}>
                
                  <Text style={{fontSize:30, fontWeight:'bold', textAlign:'center', marginBottom:10, marginTop: "10%"}} allowFontScaling={false}> Eliminar cuenta</Text>
              
                  <Text style={{fontWeight:'bold',fontSize:18, marginBottom:10, textAlign:'center'}} allowFontScaling={false}>Por favor, introduce tu contraseña.</Text>
              
                  <Text style={{fontSize:12, color:'grey', margin:20, marginBottom:20, textAlign:'center'}} allowFontScaling={false}>Recuerda que una vez terminado el proceso tu cuenta sera eliminada de manera permanente. </Text>
              
                  <Input 
                      placeholder="Contraseña"
                      onChangeText={(value)=>gurdarValor('password', value)}
                      inputContainerStyle = {{borderBottomWidth:0, paddingBottom:0}}
                      style = {styles.inputStyle}
                      containerStyle = {styles.inputContainer}
                      password={true}
                      secureTextEntry={verPassword ? false : true}
                      rightIcon={
                          <Icon
                              type="font-awesome" 
                              name = {verPassword ? "eye-slash" : "eye"}
                              size={20}
                              iconStyle = {{color:"gray"}}
                              onPress={()=>setVerPassword(!verPassword)}
                          />
                      }
                  />
                  <Button
                      title="Eliminar"
                      titleStyle = {{fontSize:20}}
                      buttonStyle={styles.btnSiguiente}
                      containerStyle = {styles.btnContainer}
                      onPress={() => {
                        if(data.password==null || data.password==""){
                          alert('Contraseña incorrecta');
                        }else{
                          confirmarContra();
                        }
                      }}
                  />   
                  <Button
                      title="Cancelar"
                      titleStyle = {{fontSize:20}}
                      buttonStyle={styles.btnCancelar}
                      containerStyle = {styles.btnContainer2}
                      onPress={ () => {abrirCerrar();setVerPassword(false)}}
                  />   
                </View>
                    :
                    <View style={styles.datosC}>
                        <Text style={{fontSize:20,  textAlign:'justify', marginBottom:10, marginTop: "15%"}} allowFontScaling={false}>{message.current}</Text>

                        <Button
                            title="Entendido"
                            titleStyle = {{fontSize:20}}
                            buttonStyle={styles.btnAceptar}
                            containerStyle = {styles.btnContainer3}
                            onPress={() => {
                                abrirCerrar();
                                cerrarSesion();
                            }}
                        />
                </View>
              }
            </ScrollView>
          </View>
        </Modal>

        <Text style={{ color: "grey", paddingLeft: 13, fontSize: 18, paddingTop: '15%', fontFamily: 'Montserrat-Medium' }} allowFontScaling={false}>Ayuda</Text>
        <View >
          {
            listAyuda.map((item, i) => (
              <ListItem key={i} onPress={item.onPress} >
                <ListItem.Content>
                  <ListItem.Title style={styles.titleItem} allowFontScaling={false} >{item.title}</ListItem.Title>
                </ListItem.Content>
                <ListItem.Chevron iconStyle={{ fontSize: 25, }} />
              </ListItem>
            ))
          }
        </View>
      </ScrollView>
    </View>
  )
}
export default Cuenta

const styles = StyleSheet.create({
  estilo1: {
    backgroundColor: '#30a1fa',
    height: 'auto',
  },
  estilo2:{
    height: Platform.OS == 'ios' ? Constants.statusBarHeight : 0,
    width: '100%'
  },
  estilo3:{
    width: '100%',
    height: 150
  },
  letra: {
    color: '#fff',
    paddingTop: 45,
    fontSize: 23,
    textAlign: 'justify',
    fontFamily: 'Montserrat-Bold',
    alignSelf: 'center'
  },
  contenedor: {
    width: 110,
    height: 110,
    marginTop: 10,
    borderRadius: 150,
    borderWidth: 3,
    borderColor: '#E0E0E0',
    backgroundColor: '#fff',
    justifyContent: 'center',
    alignItems: 'center',
    overflow: 'hidden'
  },
  titleItem: {
    fontSize: 20,
    paddingLeft: '3%',
    fontFamily: 'Montserrat-Regular',
  },
  eliminarCuentaC:{
    height: "110%",
    width: "111%",
    alignSelf: "center",
    backgroundColor: "#fff",
    flexDirection: "column",
    alignItems: "center",
    justifyContent:'center',
    position:'relative',
    paddingTop: Platform.OS == "ios" ? Constants.statusBarHeight : 0,
    top: Platform.OS == "ios" ? 75 : 0,
  },
  scroll: {
      width: "100%",
      padding: "5%",
    },
  datosC:{
      width: "100%",
      height: Dimensions.get("window").height,
      flexDirection: "column",
      alignItems:'center'
    },
  inputContainer : {
      width : "90%",
      height: 50,
      borderWidth: 1,
      borderColor: "#fff",
      borderRadius: 6,
      backgroundColor: "#F0F0F0",
      marginBottom:10
    },
  inputStyle:{
      // textAlign:"center",
      height:50,
      justifyContent :'center',
      fontSize:18,
      textAlignVertical:'center'
    },
  btnContainer:{
      width:"90%",
      height:50,
      fontSize : 25,
      marginTop:10,
    },
  btnSiguiente:{
      borderRadius: 6,
      backgroundColor: "red",
      fontSize : 25,
      height:50,
    },
  btnContainer2:{
      width:"90%",
      height:50,
      fontSize : 25,
      marginTop:10,
      marginBottom:10
    },
  btnCancelar:{
      borderRadius: 6,
      backgroundColor: "#3498DB",
      fontSize : 25,
      height:50
    },
  btnContainer3:{
      width:"90%",
      height:50,
      fontSize : 25,
      marginTop:20,
      marginBottom:10
    },
  btnAceptar:{
      borderRadius: 6,
      backgroundColor: "black",
      fontSize : 25,
      height:50
    },

})