import React from 'react'
import { View, } from 'react-native'
import { LinearProgress } from 'react-native-elements';

const LoaderLineal = () => {
    return (
        <View style={{ alignItems: 'center', justifyContent: 'center' }}>
            <LinearProgress style={{ width: 250, height: 7 }} color="#3D5CA4" />
        </View>
    )
}

export default LoaderLineal
