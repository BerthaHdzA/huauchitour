import React from 'react';
import { View } from 'react-native';
import { Ionicons } from '@expo/vector-icons';
import { useNavigation } from '@react-navigation/native';


export default function CloseMenu() {
    const navegacion = useNavigation();

    return (
        <View style={{position: 'absolute', left: 10}}>
            <Ionicons
                allowFontScaling={false}
                name="arrow-back"
                size={38.5}
                style={{ color: '#fff', paddingRight: '3%'}}
                onPress={() => navegacion.goBack()}
            />
        </View>
    )
}