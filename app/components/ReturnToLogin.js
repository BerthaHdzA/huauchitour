import React from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import { Ionicons } from '@expo/vector-icons';
import { useNavigation } from '@react-navigation/native';


export default function ReturnToLogin() {
    const navegacion = useNavigation();

    return (
        <View style={{ backgroundColor: '#fff', width: '100%', height: 70, }}>
            <TouchableOpacity onPress={() => navegacion.navigate('login')}>
                <View style={{ flexDirection: 'row', alignItems:'center', height: '100%' }}>
                    <Ionicons
                        allowFontScaling={false}
                        name="arrow-back-outline"
                        size={30}
                        style={{ color: '#555454', marginStart: '3%', paddingRight: '5%' }} />
                    <Text allowFontScaling={false} style={{ fontSize: 20, color: '#555454', fontWeight: 'bold' }}>
                        Atrás
                    </Text>
                </View>
            </TouchableOpacity>
        </View>
    )
}