import React from 'react';
import { View, Text, TouchableOpacity, Alert } from 'react-native';
import { Ionicons } from '@expo/vector-icons';
import { useNavigation } from '@react-navigation/native';
import { deleteValue } from '../utils/localStorage';

export default function ReturnIngCorreo() {
    const navegacion = useNavigation();

    const back = () => {
        Alert.alert(
            "Atención",
            "Si regresas tendrás que iniciar nuevamente el proceso para crear una cuenta nueva.",
            [
                {
                    text: "Regresar",
                    onPress: async () => {
                        await deleteValue("soli2");
                        await deleteValue("fechaSoli2");
                        navegacion.navigate('ingresaCorreo');
                    }
                },
                {
                    text: "Cancelar"
                }
            ]
        );
    }

    return (
        <View style={{ backgroundColor: '#fff', width: '100%', height: 70, }}>
            <TouchableOpacity onPress={() => back()}>
                <View style={{ flexDirection: 'row', alignItems:'center', height: '100%'}}>
                    <Ionicons
                        allowFontScaling={false}
                        name="arrow-back-outline"
                        size={30}
                        style={{ color: '#555454', marginStart: '3%', paddingRight: '5%' }} />
                    <Text allowFontScaling={false} style={{ fontSize: 20, color: '#555454', fontWeight: 'bold' }}>
                        Atrás
                    </Text>
                </View>
            </TouchableOpacity>
        </View>
    )
}