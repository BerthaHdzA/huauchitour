import React from "react";
import { View, Text, TouchableOpacity } from "react-native";
import { Ionicons } from "@expo/vector-icons";
import { useNavigation } from "@react-navigation/native";

export default function ReturnInicio() {
  const navegacion = useNavigation();

  return (
    <View style={{ backgroundColor: "#fff", width: "100%", height: 70 }}>
      <TouchableOpacity onPress={() => navegacion.navigate("inicio")}>
        <View style={{ flexDirection: "row" }}>
          <Ionicons
            allowFontScaling={false}
            name="arrow-back-outline"
            size={30}
            style={{ color: "#555454", marginStart: "3%", paddingTop: "10%" }}
          />
          <Text
            allowFontScaling={false}
            style={{
              fontSize: 20,
              paddingTop: "10%",
              color: "#555454",
              fontWeight: "bold",
            }}
          >
            Atrás
          </Text>
        </View>
      </TouchableOpacity>
    </View>
  );
}
