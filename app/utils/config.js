let url = false;
let refUrlPag = false;
let conekta = false;
let configFireBase = false;

// const modoServidor = 'dev';
const modoServidor = 'prod';
if (modoServidor === 'dev') {
    url = `https://huauchitour.com/dev/backend/public/`;
    // url = "http://192.168.1.252/gamma/backend/public/";
} else if (modoServidor === 'prod') {
    url = `https://huauchitour.com/backend/public/`;
}

export {
    url,
    refUrlPag,
    conekta,
    configFireBase
}