import { getValueFor } from "./localStorage";
import Api from "./Api";
import { url } from "./config";

export async function VerifyQR () { 
    let api = '';
    let uri = '';

    let id = await getValueFor('id');
    let token = await getValueFor('token');

    let result;

    uri = `${url}membership/buscarQr/${id}`;
    api = new Api(uri, "GET", null, token);
    await api.call().then(res => {
        if (res.response) {
            result = true;
        } else {
            result = false;
        }
    })

    return result;
}