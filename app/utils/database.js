import { initializeApp } from "firebase/app";
import { getDatabase } from "firebase/database"

const firebaseConfig = {
  apiKey: "AIzaSyA4qUwbjReJA1VMzxiK5Vy4tS2mMJktHCg",
  authDomain: "huauchitour-93841.firebaseapp.com",
  projectId: "huauchitour-93841",
  storageBucket: "huauchitour-93841.appspot.com",
  messagingSenderId: "622427791918",
  appId: "1:622427791918:web:5de1a2d1dd7e8a7a93b5bf",
  databaseURL: "https://huauchitour-93841-default-rtdb.firebaseio.com/"
};

// Initialize Firebase
initializeApp(firebaseConfig);

//obteniendo direccion de la base de datos
export const db = getDatabase();